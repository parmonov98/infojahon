<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => 'parmonov98',
            'email' => 'parmonov98@yandex.ru',
            'email_verified_at' => now(),
            'password' => '$2y$10$N7bIyvK.xBFGKrU5mibvXu99Xzu59HyDUjRMAT3OS48pG9A80EhKG', // d@DyBF9h6vStm@M
            'remember_token' => Str::random(10),
        ];
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTheoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->string('link');
            $table->text('content');
            $table->unsignedBigInteger('count_views')->default(0);
            $table->string('image');
            $table->string('image_portrait')->nullable();
            $table->enum('status', ['published', 'unpublished'])->default('published');
            $table->enum('type', ['paid', 'free'])->default('free');
            $table->enum('for_beginners', ['yes', 'no'])->default('no');
            $table->timestamps();

            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theories');
    }
}

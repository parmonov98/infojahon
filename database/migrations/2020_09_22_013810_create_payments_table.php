<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('purchase_id')->default(0);
            $table->unsignedBigInteger('order_id')->default(0);
            $table->enum('type', ['account', 'purchase', 'uzcard', 'affiliate', 'click', 'payme', 'freekassa'])->default('account');
            $table->float('sum');
            $table->string('hint');
            $table->float('rate');
            $table->enum('status', ['processing', 'confirmed', 'paid', 'unpaid']); // processing is be ing paid, paid, and expired to pay.
            // $table->enum('user_confirmation', ['no', 'yes'])->default('no'); // processing is be ing paid, paid, and expired to pay.
            // $table->string('hint');
            $table->timestamps();

            $table->index('user_id');
            $table->index('purchase_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['piece', 'term'])->default('piece');
            $table->unsignedInteger('quantity')->default(0);
            $table->unsignedInteger('term')->default(0); // term is period of tariff
            $table->integer('discount')->nullable(); // perfect discount for the tariff
            $table->integer('price'); // perfect discount for the tariff
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
    }
}

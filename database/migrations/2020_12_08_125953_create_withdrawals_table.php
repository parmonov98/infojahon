<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sum');
            $table->unsignedBigInteger('user_id');
            // $table->unsignedBigInteger('account_id');
            $table->unsignedBigInteger('rate');
            $table->enum('wallet', ['qiwi', 'yandex_money'])->default('qiwi');
            $table->enum('status', ['created', 'paid', 'cancelled'])->default('created');
            $table->timestamps();
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawals');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username')->nullable(); // auth using TG and setting tg_id
            $table->unsignedBigInteger('tg_id')->nullable(); // auth using TG and setting tg_id
            $table->string('email')->nullable();
            $table->enum('role', ['user', 'admin', 'developer'])->default('user');
            $table->enum('type', ['signup', 'telegram'])->default('signup');
            $table->string('password');
            $table->string('referer')->nullable();
            $table->string('hash')->nullable(); // auth using TG and setting hash coming from TG seamless login redirect
            $table->string('token'); // confirmation token using email signup
            // $table->text('profile_photo_path')->nullable();
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

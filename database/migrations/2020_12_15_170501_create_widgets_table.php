<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['sidebar', 'content'])->default('sidebar');
            $table->enum('page', ['home', 'theories', 'forecast', 'tariffs', 'stats'])->default('home');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->text('content');
            $table->string('button');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widgets');
    }
}

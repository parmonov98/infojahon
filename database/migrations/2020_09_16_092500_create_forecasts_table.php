<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forecasts', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('title')->unique();
            $table->string('link')->unique();
            $table->string('sport_type'); // sport type
            $table->string('chempionat');
            $table->string('forecast_hint');
            $table->string('first_team')->nullable();
            $table->string('second_team')->nullable();
            $table->integer('bet_sum')->default(10000);
            $table->enum('bet_type', ['ordinar', 'expres'])->default('ordinar'); // forecast type
            $table->string('forecast_value'); // forecast_value type
            $table->string('partner_url')->nullable(); // forecast_value type
            $table->float('kf');
            $table->text('explanation');
            $table->string('image');
            $table->string('image_portrait')->nullable();
            $table->enum('status', ['created', 'published', 'unpublished'])->default('created');
            $table->enum('type', ['paid', 'free']); // paid or free
            $table->enum('is_ended', ['yes', 'no'])->default('no'); // ended match
            $table->enum('bet_result', ["win", "lose", "tie"])->default("win"); // bet result is g'alaba
            $table->dateTime('begin');
            $table->dateTime('end');
            $table->dateTime('publishing_time');
            $table->timestamps();
            $table->index('user_id');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forecasts');
    }
}

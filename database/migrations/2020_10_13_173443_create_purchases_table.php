<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('tariff_id');
            $table->unsignedInteger('payment_id');
            $table->enum('payment_method', ['account','freekassa'])->default('freekassa');
            $table->dateTime('expire_date')->nullable();
            $table->unsignedInteger('quantity')->default(0);
            $table->timestamps();
            $table->index('user_id');
            $table->index('tariff_id');
            $table->index('payment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}

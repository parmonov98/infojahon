<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'username' => 'parmonov98',
            'email' => 'parmonov98@yandex.ru',
            'password' => Hash::make('d@DyBF9h6vStm@M'),
            'email_verified_at' => now(),
            'type' => 'admin',
            'created_at' => now(),
            'updated_at' => now(),
            'remember_token' => Str::random(10)
        ]);
        DB::table('users')->insert([
            'username' => 'ilgiz',
            'email' => 'ilgiz@iskandarov.uz',
            'password' => Hash::make('12345678'),
            'email_verified_at' => now(),
            'type' => 'user',
            'created_at' => now(),
            'updated_at' => now(),
            'remember_token' => Str::random(10)
        ]);
        DB::table('profiles')->insert([
            'user_id' => 1,
            'first_name' => 'Murod',
            'last_name' => 'Parmonov',
            'image' => null,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('profiles')->insert([
            'user_id' => 2,
            'first_name' => 'Ilgiz',
            'last_name' => 'iskandarov',
            'image' => null,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('accounts')->insert([
            'user_id' => 1,
            'balance' => 50000,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('accounts')->insert([
            'user_id' => 2,
            'balance' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('forecasts')->insert([
            'user_id' => 1,
            'title' => "Chelsea bilan Milan o'rtasida bo'ladigan matchga prognoz",
            'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz',
            'sport_type' => 'Futbol',
            'chempionat' => 'UFC',
            'begin' => '2020-10-02 23:09:00',
            'end' => '2020-10-02 23:09:00',
            'forecast_hint' => 'Milan yutqazadi',
            'first_team' => 'Chelsea',
            'second_team' => 'Milan',
            'forecast_value' => 'TM(2.3)',
            'kf' => 2.00,
            'explanation' => "<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>",
            'status' => 'published',
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'type' => 'free',
            'is_ended' => 'no',
            'bet_type' => 'ordinar',
            'publishing_time' => '2020-09-30 13:09:00',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('forecasts')->insert([
            'user_id' => 1,
            'title' => "Chelsea bilan Milan o'rtasida bo'ladigan matchga prognoz 2",
            'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz-2',
            'sport_type' => 'Futbol',
            'chempionat' => 'UFC',
            'begin' => '2020-09-18 23:09:00',
            'end' => '2020-09-18 23:09:00',
            'forecast_hint' => 'Milan yutqazadi',
            'first_team' => 'Chelsea',
            'second_team' => 'Milan',
            'forecast_value' => 'TM(2.3)',
            'bet_type' => 'ordinar',
            'kf' => 2.00,
            'explanation' => "<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>",
            'status' => 'created',
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'type' => 'free',
            'is_ended' => 'no',

            'publishing_time' => '2020-09-30 13:09:00',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('forecasts')->insert([
            'user_id' => 1,
            'title' => "Chelsea bilan Milan o'rtasida bo'ladigan matchga prognoz 3",
            'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz-3',
            'sport_type' => 'Futbol',
            'chempionat' => 'UFC',
            'begin' => '2020-09-18 23:09:00',
            'end' => '2020-09-18 23:09:00',
            'forecast_hint' => 'Milan yutqazadi',
            'first_team' => 'Chelsea',
            'second_team' => 'Milan',
            'forecast_value' => 'TM(2.3)',
            'bet_type' => 'ordinar',
            'kf' => 2.00,
            'explanation' => "<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>",
            'status' => 'created',
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'type' => 'free',
            'is_ended' => 'no',
            'publishing_time' => '2020-09-30 13:09:00',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('forecasts')->insert([
            'user_id' => 1,
            'title' => "Chelsea bilan Milan o'rtasida bo'ladigan matchga prognoz4",
            'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz-4',
            'sport_type' => 'Futbol',
            'chempionat' => 'UFC',
            'begin' => '2020-10-1 13:09:00',
            'end' => '2020-10-1 13:09:00',
            'forecast_hint' => 'Milan yutqazadi',
            'first_team' => 'Chelsea',
            'second_team' => 'Milan',
            'bet_type' => 'ordinar',
            'forecast_value' => 'TM(2.3)',
            'kf' => 2.00,
            'explanation' => "<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>",
            'status' => 'published',
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'type' => 'paid',
            'is_ended' => 'no',
            'publishing_time' => '2020-09-30 13:09:00',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('forecasts')->insert([
            'user_id' => 1,
            'title' => "Chelsea bilan Milan o'rtasida bo'ladigan matchga prognoz 5",
            'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz-5',
            'sport_type' => 'Futbol',
            'chempionat' => 'UFC',
            'begin' => '2020-09-18 23:09:00',
            'end' => '2020-09-18 23:09:00',
            'forecast_hint' => 'Milan yutqazadi',
            'first_team' => 'Chelsea',
            'second_team' => 'Milan',
            'bet_type' => 'expres',
            'forecast_value' => 'TM(2.3)',
            'kf' => 2.00,
            'explanation' => "<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>",
            'status' => 'published',
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'type' => 'paid',
            'is_ended' => 'no',

            'publishing_time' => '2020-09-30 13:09:00',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('forecasts')->insert([
            'user_id' => 1,
            'title' => "Chelsea bilan Milan o'rtasida bo'ladigan matchga prognoz 6",
            'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz-6',
            'sport_type' => 'Futbol',
            'chempionat' => 'UFC',
            'begin' => '2020-09-18 23:09:00',
            'end' => '2020-09-18 23:09:00',
            'forecast_hint' => 'Milan yutqazadi',
            'first_team' => 'Chelsea',
            'second_team' => 'Milan',
            'bet_type' => 'expres',
            'forecast_value' => 'TM(2.3)',
            'kf' => 2.00,
            'explanation' => "<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>",
            'status' => 'published',
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'type' => 'paid',

            'is_ended' => 'no',
            'publishing_time' => '2020-09-30 13:09:00',
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        DB::table('theories')->insert([
            'user_id' => 1,
            'title' => "Почему долгосрочные ставки выгодны?",
            'link' => 'pochemu-dolgosrochnye-stavki-vygodny-',
            'content' => "<p>В линиях букмекерских контор можно найти большое разнообразие вариантов ставок. Помимо обычных событий, например, футбольных или баскетбольных матчей, игроки могут делать ставки и на долгосрочные события. К таковым относятся прогнозы победителя турнира или чемпионата, выход команды в плей-офф и другие события, результат которых будет известен через длительное время. При этом ставки можно сделать за несколько дней, за месяц или за полгода до начала турнира.</p>",
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        DB::table('theories')->insert([
            'user_id' => 1,
            'title' => "Почему долгосрочные ставки выгодны? 2",
            'link' => 'pochemu-dolgosrochnye-stavki-vygodny-2',
            'content' => "<p>В линиях букмекерских контор можно найти большое разнообразие вариантов ставок. Помимо обычных событий, например, футбольных или баскетбольных матчей, игроки могут делать ставки и на долгосрочные события. К таковым относятся прогнозы победителя турнира или чемпионата, выход команды в плей-офф и другие события, результат которых будет известен через длительное время. При этом ставки можно сделать за несколько дней, за месяц или за полгода до начала турнира.</p>",
            'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('tariffs')->insert([
            'type' => "piece",
            'quantity' => '1',
            'term' => "0",
            'discount' => '0',
            'price' => '2000',
            'created_at' => now(),
            'updated_at' => now(),
        ]);



        // User::factory(10)->create();
        // return [
        //     'username' => 'parmonov98',
        //     'email' => 'parmonov98@yandex.ru',
        //     'email_verified_at' => now(),
        //     'password' => '$2y$10$N7bIyvK.xBFGKrU5mibvXu99Xzu59HyDUjRMAT3OS48pG9A80EhKG', // d@DyBF9h6vStm@M
        //     'remember_token' => Str::random(10),
        // ];
    }
}

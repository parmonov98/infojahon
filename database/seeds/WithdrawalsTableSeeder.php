<?php

use Illuminate\Database\Seeder;

class WithdrawalsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('withdrawals')->delete();
        
        \DB::table('withdrawals')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sum' => 10000,
                'user_id' => 1,
                'rate' => 135,
                'wallet' => 'yandex_money',
                'status' => 'paid',
                'created_at' => '2020-12-24 18:08:26',
                'updated_at' => '2020-12-24 18:08:45',
            ),
        ));
        
        
    }
}
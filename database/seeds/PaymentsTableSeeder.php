<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payments')->delete();
        
        \DB::table('payments')->insert(array (
            0 => 
            array (
                'id' => 7,
                'user_id' => 1,
                'purchase_id' => 9,
                'order_id' => 0,
                'type' => 'account',
                'sum' => 2000.0,
                'hint' => '2000 so\'m admin hisobidan yechib to\'lov qilindi.',
                'rate' => 135.03,
                'status' => 'paid',
                'created_at' => '2020-11-01 17:31:57',
                'updated_at' => '2020-12-24 17:31:57',
            ),
            1 => 
            array (
                'id' => 8,
                'user_id' => 1,
                'purchase_id' => 10,
                'order_id' => 0,
                'type' => 'account',
                'sum' => 2000.0,
                'hint' => '2000 so\'m admin hisobidan yechib to\'lov qilindi.',
                'rate' => 135.03,
                'status' => 'paid',
                'created_at' => '2020-11-10 17:51:38',
                'updated_at' => '2020-12-24 17:51:38',
            ),
            2 => 
            array (
                'id' => 9,
                'user_id' => 1,
                'purchase_id' => 11,
                'order_id' => 0,
                'type' => 'account',
                'sum' => 20000.0,
                'hint' => '20000 so\'m admin hisobidan yechib to\'lov qilindi.',
                'rate' => 135.03,
                'status' => 'paid',
                'created_at' => '2020-12-24 17:53:56',
                'updated_at' => '2020-12-24 17:53:56',
            ),
            3 => 
            array (
                'id' => 10,
                'user_id' => 1,
                'purchase_id' => 12,
                'order_id' => 0,
                'type' => 'account',
                'sum' => 20000.0,
                'hint' => '20000 so\'m admin hisobidan yechib to\'lov qilindi.',
                'rate' => 135.03,
                'status' => 'paid',
                'created_at' => '2020-12-24 17:55:30',
                'updated_at' => '2020-12-24 17:55:30',
            ),
            4 => 
            array (
                'id' => 11,
                'user_id' => 1,
                'purchase_id' => 13,
                'order_id' => 0,
                'type' => 'account',
                'sum' => 2000.0,
                'hint' => '2000 so\'m admin hisobidan yechib to\'lov qilindi.',
                'rate' => 135.03,
                'status' => 'paid',
                'created_at' => '2020-10-02 17:56:26',
                'updated_at' => '2020-12-24 17:56:26',
            ),
        ));
        
        
    }
}
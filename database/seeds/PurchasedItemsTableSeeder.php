<?php

use Illuminate\Database\Seeder;

class PurchasedItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('purchased_items')->delete();
        
        \DB::table('purchased_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'forecast_id' => 12,
                'theory_id' => NULL,
                'type' => 'forecast',
                'created_at' => '2020-12-22 07:35:43',
                'updated_at' => '2020-12-22 07:35:43',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class PurchasesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('purchases')->delete();
        
        \DB::table('purchases')->insert(array (
            0 => 
            array (
                'id' => 9,
                'user_id' => 1,
                'tariff_id' => 1,
                'payment_id' => 7,
                'payment_method' => 'account',
                'expire_date' => NULL,
                'quantity' => 1,
                'created_at' => '2020-11-01 17:31:57',
                'updated_at' => '2020-12-24 17:31:57',
            ),
            1 => 
            array (
                'id' => 10,
                'user_id' => 1,
                'tariff_id' => 1,
                'payment_id' => 8,
                'payment_method' => 'account',
                'expire_date' => NULL,
                'quantity' => 1,
                'created_at' => '2020-11-10 17:51:38',
                'updated_at' => '2020-12-24 17:51:38',
            ),
            2 => 
            array (
                'id' => 11,
                'user_id' => 1,
                'tariff_id' => 2,
                'payment_id' => 9,
                'payment_method' => 'account',
                'expire_date' => NULL,
                'quantity' => 10,
                'created_at' => '2020-12-24 17:53:56',
                'updated_at' => '2020-12-24 17:53:56',
            ),
            3 => 
            array (
                'id' => 12,
                'user_id' => 1,
                'tariff_id' => 2,
                'payment_id' => 10,
                'payment_method' => 'account',
                'expire_date' => NULL,
                'quantity' => 10,
                'created_at' => '2020-12-24 17:55:30',
                'updated_at' => '2020-12-24 17:55:30',
            ),
            4 => 
            array (
                'id' => 13,
                'user_id' => 1,
                'tariff_id' => 1,
                'payment_id' => 11,
                'payment_method' => 'account',
                'expire_date' => NULL,
                'quantity' => 1,
                'created_at' => '2020-10-02 17:56:26',
                'updated_at' => '2020-12-24 17:56:26',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class TariffsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tariffs')->delete();
        
        \DB::table('tariffs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'piece',
                'quantity' => 1,
                'term' => 0,
                'discount' => 0,
                'price' => 2000,
                'created_at' => '2020-11-28 13:23:14',
                'updated_at' => '2020-11-28 13:23:14',
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'piece',
                'quantity' => 10,
                'term' => 0,
                'discount' => NULL,
                'price' => 20000,
                'created_at' => '2020-12-22 05:48:54',
                'updated_at' => '2020-12-22 05:48:54',
            ),
        ));
        
        
    }
}
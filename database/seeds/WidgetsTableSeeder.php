<?php

use Illuminate\Database\Seeder;

class WidgetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('widgets')->delete();
        
        \DB::table('widgets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'content',
                'page' => 'home',
                'status' => 'inactive',
                'content' => '<img src="/assets/img/avatars/8.jpg" class="w-100" alt="">',
                'button' => 'Kanalga a\'zo bo\'lish 1',
                'url' => 'https://youtu.be/tgbNymZ7vqY',
                'created_at' => NULL,
                'updated_at' => '2020-12-23 17:04:57',
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'content',
                'page' => 'home',
                'status' => 'inactive',
                'content' => '<img src="/assets/img/banner.png" class="w-100" alt="">',
                'button' => 'Reklama saytiga o\'tish 2',
                'url' => 'https://google.com',
                'created_at' => NULL,
                'updated_at' => '2020-12-23 17:06:18',
            ),
            2 => 
            array (
                'id' => 3,
                'type' => 'sidebar',
                'page' => 'home',
                'status' => 'inactive',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/8.jpg" class="w-100" alt="">',
                'button' => 'reklamaga bosish',
                'url' => 'https://www.youtube.com/watch?v=HtTzkRvMbvU',
                'created_at' => NULL,
                'updated_at' => '2020-12-23 17:11:35',
            ),
            3 => 
            array (
                'id' => 4,
                'type' => 'sidebar',
                'page' => 'home',
                'status' => 'inactive',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/5.jpg" class="w-100" alt="">',
                'button' => 'sidebar banner 2',
                'url' => 'https://bitly.com/',
                'created_at' => NULL,
                'updated_at' => '2020-12-23 17:12:47',
            ),
            4 => 
            array (
                'id' => 5,
                'type' => 'sidebar',
                'page' => 'stats',
                'status' => 'inactive',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/2.jpg" class="w-100" alt="">',
                'button' => 'button on page stats',
                'url' => 'http://127.0.0.1:8000/stats',
                'created_at' => NULL,
                'updated_at' => '2020-12-23 17:15:13',
            ),
            5 => 
            array (
                'id' => 6,
                'type' => 'content',
                'page' => 'stats',
                'status' => 'inactive',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/8.jpg" class="w-100" alt="">',
                'button' => 'button in content of stats',
                'url' => 'http://127.0.0.1:8000/stats',
                'created_at' => NULL,
                'updated_at' => '2020-12-23 17:14:57',
            ),
            6 => 
            array (
                'id' => 7,
                'type' => 'sidebar',
                'page' => 'tariffs',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/3.jpg" class="w-100" alt="">',
                'button' => 'sidebar button on tariff page',
                'url' => 'http://127.0.0.1:8000/tariffs',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'type' => 'sidebar',
                'page' => 'stats',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/1.jpg" class="w-100" alt="">',
                'button' => 'stats page sidebar btn',
                'url' => 'http://127.0.0.1:8000/stats',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'type' => 'sidebar',
                'page' => 'theories',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/4.jpg" class="w-100" alt="">',
                'button' => 'stats page sidebar btn',
                'url' => 'http://127.0.0.1:8000/theories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'type' => 'sidebar',
                'page' => 'tariffs',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/4.jpg" class="w-100" alt="">',
                'button' => 'stats page sidebar btn',
                'url' => 'http://127.0.0.1:8000/theories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'type' => 'sidebar',
                'page' => 'theories',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/6.jpg" class="w-100" alt="">',
                'button' => 'stats page sidebar btn',
                'url' => 'http://127.0.0.1:8000/theories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'type' => 'sidebar',
                'page' => 'theories',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/6.jpg" class="w-100" alt="">',
                'button' => 'stats page sidebar btn',
                'url' => 'http://127.0.0.1:8000/theories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'type' => 'content',
                'page' => 'forecast',
                'status' => 'active',
                'content' => '<img src="/assets/img/banner.png" class="w-100" alt="">',
                'button' => 'free forecast button',
                'url' => 'http://127.0.0.1:8000/forecasts/free',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'type' => 'sidebar',
                'page' => 'forecast',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/1.jpg" class="w-100" alt="">',
                'button' => 'stats page sidebar btn',
                'url' => 'http://127.0.0.1:8000/theories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'type' => 'sidebar',
                'page' => 'forecast',
                'status' => 'active',
                'content' => '<img src="http://127.0.0.1:8000/assets/img/avatars/7.jpg" class="w-100" alt="">',
                'button' => 'stats page sidebar btn',
                'url' => 'http://127.0.0.1:8000/theories',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
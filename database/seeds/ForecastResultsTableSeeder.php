<?php

use Illuminate\Database\Seeder;

class ForecastResultsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('forecast_results')->delete();
        
        \DB::table('forecast_results')->insert(array (
            0 => 
            array (
                'id' => 1,
                'forecast_id' => 9,
                'result' => 'win',
                'screenshot' => 'uploads/9sIJtH4IRPLbPxKPNsDohDvKxHYf1tkZYU9wiUAy.jpeg',
                'created_at' => '2020-12-08 06:00:49',
                'updated_at' => '2020-12-08 06:00:49',
            ),
            1 => 
            array (
                'id' => 2,
                'forecast_id' => 8,
                'result' => 'win',
                'screenshot' => 'uploads/2VuSRCYMJC6LUa3osvEv17s0fnj8dNw2vvbAldaH.jpeg',
                'created_at' => '2020-12-08 07:19:40',
                'updated_at' => '2020-12-08 07:19:40',
            ),
            2 => 
            array (
                'id' => 3,
                'forecast_id' => 7,
                'result' => 'lose',
                'screenshot' => 'uploads/vhRHTewGNsB0kkQpig3iDku9BcHrCbjhIYnHrlNR.jpeg',
                'created_at' => '2020-12-08 07:32:46',
                'updated_at' => '2020-12-08 07:32:46',
            ),
        ));
        
        
    }
}
<?php

namespace Database\Seeders;

use AccountsTableSeeder;
use ContactsTableSeeder;
use FailedJobsTableSeeder;
use ForecastResultsTableSeeder;
use ForecastsTableSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use MigrationsTableSeeder;
use PasswordResetsTableSeeder;
use PaymentsTableSeeder;
use ProfilesTableSeeder;
use PurchasesTableSeeder;
use TariffsTableSeeder;
use TheoriesTableSeeder;
use UsersTableSeeder;
use PurchasedItemsTableSeeder;
use WidgetsTableSeeder;
use WithdrawalsTableSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(AccountsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(ForecastResultsTableSeeder::class);
        $this->call(ForecastsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(PurchasesTableSeeder::class);
        $this->call(TariffsTableSeeder::class);
        $this->call(TheoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(FailedJobsTableSeeder::class);
        $this->call(MigrationsTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PurchasedItemsTableSeeder::class);

        $this->call(WidgetsTableSeeder::class);
        $this->call(WithdrawalsTableSeeder::class);
    }
}

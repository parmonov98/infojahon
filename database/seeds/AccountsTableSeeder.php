<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('accounts')->delete();
        
        \DB::table('accounts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'balance' => 148000.0,
                'quantity' => 58,
                'expire_date' => NULL,
                'yandex_money' => '406565065065',
                'qiwi' => '-',
                'created_at' => '2020-11-28 13:23:13',
                'updated_at' => '2020-12-24 18:08:26',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'balance' => 200.0,
                'quantity' => 0,
                'expire_date' => NULL,
                'yandex_money' => NULL,
                'qiwi' => NULL,
                'created_at' => '2020-11-28 13:23:13',
                'updated_at' => '2020-12-08 09:06:18',
            ),
            2 => 
            array (
                'id' => 9,
                'user_id' => 9,
                'balance' => 0.0,
                'quantity' => 0,
                'expire_date' => NULL,
                'yandex_money' => NULL,
                'qiwi' => NULL,
                'created_at' => '2020-12-02 06:39:00',
                'updated_at' => '2020-12-02 06:39:00',
            ),
            3 => 
            array (
                'id' => 10,
                'user_id' => 10,
                'balance' => 0.0,
                'quantity' => 0,
                'expire_date' => NULL,
                'yandex_money' => '406565065065',
                'qiwi' => '-',
                'created_at' => '2020-12-24 18:23:11',
                'updated_at' => '2020-12-24 18:23:25',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class TheoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('theories')->delete();
        
        \DB::table('theories')->insert(array (
            0 => 
            array (
                'id' => 2,
                'user_id' => 1,
                'title' => 'Почему долгосрочные ставки выгодны? 2',
                'link' => 'pochemu-dolgosrochnye-stavki-vygodny-2',
                'content' => '<p>В линиях букмекерских контор можно найти большое разнообразие вариантов ставок. Помимо обычных событий, например, футбольных или баскетбольных матчей, игроки могут делать ставки и на долгосрочные события. К таковым относятся прогнозы победителя турнира или чемпионата, выход команды в плей-офф и другие события, результат которых будет известен через длительное время. При этом ставки можно сделать за несколько дней, за месяц или за полгода до начала турнира.</p>',
                'count_views' => 16,
                'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
                'image_portrait' => NULL,
                'status' => 'published',
                'type' => 'free',
                'for_beginners' => 'no',
                'created_at' => '2020-11-28 13:23:14',
                'updated_at' => '2020-12-22 05:41:48',
            ),
            1 => 
            array (
                'id' => 3,
                'user_id' => 1,
                'title' => 'Yangi kelganlar uchun super qo\'llanma !',
                'link' => 'yangi-kelganlar-uchun-super-qo-llanma-',
                'content' => '<p>Menga juda ko&#39;p kishidan savol keladi bu saytda pul ishlasa bo&#39;ladimi u to&#39;laydimi yoqmi , <strong>loxotron emasmi</strong> , qanday kiriladi , <strong>pul kirgizish</strong> va <strong>chiqarish</strong> qanday usullarda olib boriladi shunga o&#39;xshash ko&#39;plab savollar bilan murojat qilishadi !</p>

<p>&nbsp;</p>

<p>Shu qiyinchiliklarni oldini olish maqsadida quyidagi to&#39;liq ma&#39;lumotlar bazasini yaratdim ! You tubeda videodarslari bilan birgalikda albatta !<strong> Qani ketdik </strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>To&#39;liq ma&#39;lumot berilgan videodarslik : <a href="https://t.me/info1xbet_uz" target="_blank"> Videolarni ko&#39;rish</a></p>

<blockquote>Maqola ko&#39;rinishida ham o&#39;qib o&#39;rganishingiz mumkin albatta ! <strong>Pastdaa</strong></blockquote>

<p><strong>1)</strong> Quyidagi ishonchli manzil orqali ro&#39;yhatdan o&#39;tsangiz hech qanday muommo yuz bermaydi ! <a href="https://bit.ly/2RCNQOi" target="_blank">Ro&#39;yhatdan o&#39;tish uchun bosing</a> <strong> --&gt; </strong><a href="https://bit.ly/2RCNQOi" target="_blank">https://bit.ly/2RCNQOi</a></p>

<p><strong>2)</strong> Bosganingizdan so&#39;ng quyidagicha oyna ochiladi ! Rasmga e&#39;tibor bering !</p>

<p><img alt="" src="http://127.0.0.1:8000/uploads/1xbet-signup-form_1608460361.png" style="height:1167px; width:720px" /></p>

<p>3) Bu yerda siz tez oson ro&#39;yhatdan o&#39;tasiz ! Bu bo&#39;limchalarni to&#39;ldirish esa uzizga havola farqi yoq ! <strong>Pastdagi rasmga e&#39;tibor beramiz ! </strong></p>

<p><img alt="" src="http://127.0.0.1:8000/uploads/1xbet-signup-form2_1608460407.png" style="height:1164px; width:720px" /></p>

<p>4) Bo&#39;limchalarni to&#39;ldirib keyin pastdagi <strong>Зарегистироваться</strong>(Ro&#39;yhatdan o&#39;tish)tgmasini bosamiz ! Hamda quyidagi oyna ochiladi : Rasmga e&#39;tibor bering</p>

<p><img alt="" src="http://127.0.0.1:8000/uploads/1xbet-signup-form3_1608460447.png" style="height:755px; width:481px" /></p>

<p>&nbsp;</p>

<p>Telefonimizga endi mobilniy prilojeniyani ustanovka qilamiz ! Uni qayerdan olamiz ?</p>

<p><strong>Quyidagi manzilga bosamiz : </strong>--&gt; <a href="https://bit.ly/2RCNQOi" target="_blank">https://bit.ly/2RCNQOi</a><a href="https://bit.ly/2SL7vhj" target="_blank"> </a> (Agar manzil ishlamay qolsa telegramga kirib mendan <strong>@Jahongir1994 </strong> dan yangi ishonchli manzilni so&#39;rashingiz mumkin ) Quyidagicha oyna ochiladi !</p>

<p><img alt="" src="http://127.0.0.1:8000/uploads/1xbet-signup-form4_1608460485.png" style="height:1249px; width:720px" /></p>

<p>Pastroqqa tushib mobilniy prilojeniya ni bosamiz !</p>

<p>&nbsp;</p>

<p>Bosgandan keyin quyidagicha 2 ta telefonning versiyasi uchun programma ko&#39;rinadi ! Bizga androidniki kerak bo&#39;lsa androidni skachat qilamiz : Hammasi oddiy</p>

<p><img alt="" src="http://127.0.0.1:8000/uploads/1xbet-signup-form5_1608460559.png" style="height:1144px; width:720px" /></p>

<p><strong>Shundan so&#39;ng telefonimizga ustanovka qilinadi ! hamda uning ko&#39;rinishi quyidagicha holatda bo&#39;ladi :</strong></p>

<p><strong><img alt="" src="http://127.0.0.1:8000/uploads/1xbet_1608460629." style="height:1280px; width:606px" /></strong></p>

<p><em>Mana do&#39;stlar oson va oddiy holatda hamda ishonchli manzil orqali ro&#39;yhatdan o&#39;tdik , endi esa biz tepadagi </em><strong><em>login va parolimiz </em></strong><em>orqali o&#39;yinga kiramiz va pul ishlashni birgalikda boshlaymiz ! </em></p>

<p><img alt="" src="http://127.0.0.1:8000/uploads/1xbet-login_1608460664.png" style="height:767px; width:412px" /></p>

<p>&nbsp;</p>

<p>Shundan so&#39;ng <strong>Войти (Kirish</strong> ) tugmasini bosasiz ! Bo&#39;ldi siz o&#39;yindasiz !</p>

<blockquote>Albatta hisobizni kamida <strong>50-100 ming</strong> so&#39;mga to&#39;ldiring<br />
Keyingi tushunchalarni <strong> videodarslar</strong> orqali berib boraman ! <strong>Qani ketdik !</strong></blockquote>

<p>Boshqa savollarizga to&#39;liq javobni telegram botimizdan olasiz !<br />
<strong>Telegram bot :</strong> ---&gt; <a href="https://t.me/infojahon_bot" target="_blank">@Infojahon_bot</a></p>

<p><strong>Bizning yutuqlarimiz marhamat :</strong> ---&gt; <a href="https://telegra.ph/Vip-kanal-narxlari-haqida-01-03">Yutuqlarimiz haqida to&#39;liq</a></p>

<p><br />
To&#39;liq ma&#39;lumotli Saytimiz: taplink.cc/infojahon</p>

<p>Instagram : Infojahon</p>

<p>&nbsp;</p>',
    'count_views' => 43,
    'image' => 'uploads/OqjG2EuS5fPCaKdquWBNIktfK3rmf0E8jwLQySU5.png',
    'image_portrait' => NULL,
    'status' => 'published',
    'type' => 'free',
    'for_beginners' => 'yes',
    'created_at' => '2020-12-20 15:38:48',
    'updated_at' => '2020-12-23 09:27:27',
),
2 => 
array (
    'id' => 4,
    'user_id' => 1,
    'title' => 'gdrgd',
    'link' => 'gdrgd',
    'content' => '<p>test</p>',
    'count_views' => 3,
    'image' => 'uploads/AtLZ3Clt8DlF2yFdp6H1ZXtivjdekDL1dRSJHryI.jpeg',
    'image_portrait' => NULL,
    'status' => 'published',
    'type' => 'paid',
    'for_beginners' => 'no',
    'created_at' => '2020-12-22 05:18:07',
    'updated_at' => '2020-12-22 07:58:42',
),
));
        
        
    }
}
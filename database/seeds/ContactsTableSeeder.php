<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contacts')->delete();
        
        \DB::table('contacts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Murod Parmonov',
                'email' => 'parmonov98@yandex.ru',
                'phone' => '+998942638523',
                'message' => 'telefon',
                'created_at' => '2020-12-24 12:20:14',
                'updated_at' => '2020-12-24 12:20:14',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Murod Parmonov',
                'email' => 'parmonov98@yandex.ru',
                'phone' => '+998942638523',
                'message' => 'egeret',
                'created_at' => '2020-12-24 12:57:18',
                'updated_at' => '2020-12-24 12:57:18',
            ),
        ));
        
        
    }
}
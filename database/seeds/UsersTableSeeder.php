<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin demo',
                'username' => 'admin',
                'tg_id' => 1351354,
                'email' => 'admin@infojahon.ru',
                'role' => 'admin',
                'type' => 'signup',
                'password' => '$2y$10$puNOOpJXR5NrSaUqFW/6Zu7xpfwifT//.6/xTGWz0RkojIxFfOAHy',
                'referer' => NULL,
                'hash' => NULL,
                'token' => 'token',
                'remember_token' => 'To7K6nePlaKB6dP3UQypXO67HF3tvW3Pcj7mzVW6gRki8GhikVrM8V6thBJw',
                'email_verified_at' => '2020-11-13 10:32:23',
                'created_at' => '2020-11-13 10:32:23',
                'updated_at' => '2020-11-13 10:32:23',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'User demo',
                'username' => NULL,
                'tg_id' => 1351354,
                'email' => 'user@infojahon.ru',
                'role' => 'user',
                'type' => 'signup',
                'password' => '$2y$10$AHBs3ZNBhksgbpri32tRqush/wsLG0t54PwbhFdcCR7flKU95ioGC',
                'referer' => NULL,
                'hash' => NULL,
                'token' => 'token',
                'remember_token' => 'rYILsTJKUa',
                'email_verified_at' => '2020-11-13 10:32:23',
                'created_at' => '2020-11-13 10:32:23',
                'updated_at' => '2020-11-13 10:32:23',
            ),
            2 => 
            array (
                'id' => 9,
                'name' => 'g1parmonov',
                'username' => 'g1parmonov',
                'tg_id' => NULL,
                'email' => 'g1parmonov@gmail.com',
                'role' => 'user',
                'type' => 'signup',
                'password' => '$2y$10$iJNMlfkrJy4nlXc57QIeHeNyvkcwSMCKXXAdGJgHRtWYScbwlD4.K',
                'referer' => NULL,
                'hash' => NULL,
                'token' => 'e1TPo2lv6zAo48qeLsvAOwtYlFoCA2g6',
                'remember_token' => NULL,
                'email_verified_at' => '2020-12-02 06:39:57',
                'created_at' => '2020-12-02 06:39:00',
                'updated_at' => '2020-12-02 06:39:57',
            ),
            3 => 
            array (
                'id' => 10,
                'name' => 'salomdunyouz',
                'username' => 'salomdunyouz',
                'tg_id' => NULL,
                'email' => 'salomdunyouz@gmail.com',
                'role' => 'user',
                'type' => 'signup',
                'password' => '$2y$10$fDobc1V1lqy2kRZpyKdQEeOgHwSLwX1CIcTro7PqXiD.OmtEYN8YC',
                'referer' => NULL,
                'hash' => NULL,
                'token' => '3axmI88felXxbFt5MRwpn0PllBjUkpcb',
                'remember_token' => NULL,
                'email_verified_at' => NULL,
                'created_at' => '2020-12-24 18:23:11',
                'updated_at' => '2020-12-24 18:23:11',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class ForecastsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('forecasts')->delete();
        
        \DB::table('forecasts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => '1',
                'title' => 'Chelsea bilan Milan o\'rtasida bo\'ladigan matchga prognoz',
                'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz',
                'sport_type' => 'Futbol',
                'chempionat' => 'UFC',
                'forecast_hint' => 'Milan yutqazadi',
                'first_team' => 'Chelsea',
                'second_team' => 'Milan',
                'bet_sum' => 10000,
                'bet_type' => 'ordinar',
            'forecast_value' => 'TM(2.3)',
                'partner_url' => NULL,
                'kf' => 2.0,
            'explanation' => '<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>',
                'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
                'image_portrait' => NULL,
                'status' => 'published',
                'type' => 'paid',
                'is_ended' => 'yes',
                'bet_result' => 'lose',
                'begin' => '2020-10-02 23:09:00',
                'end' => '2020-12-08 23:09:00',
                'publishing_time' => '2020-09-30 13:09:00',
                'created_at' => '2020-11-28 13:23:13',
                'updated_at' => '2020-12-20 14:34:01',
            ),
            1 => 
            array (
                'id' => 6,
                'user_id' => '1',
                'title' => 'Chelsea bilan Milan o\'rtasida bo\'ladigan matchga prognoz 6',
                'link' => 'chelsea-bilan-milan-o-rtasida-bo-ladigan-matchga-prognoz-6',
                'sport_type' => 'Futbol',
                'chempionat' => 'UFC',
                'forecast_hint' => 'Milan yutqazadi',
                'first_team' => 'Chelsea',
                'second_team' => 'Milan',
                'bet_sum' => 10000,
                'bet_type' => 'expres',
            'forecast_value' => 'TM(2.3)',
                'partner_url' => NULL,
                'kf' => 2.0,
            'explanation' => '<p><em>Chelsea</em>&nbsp;Football Club (CFC) est un club de football professionnel anglais fond&eacute; le 10 mars ... Malgr&eacute; un bon parcours au cours duquel ils &eacute;liminent notamment l&#39;AC&nbsp;<em>Milan</em>, les &quot;blues&quot; sont &eacute;limin&eacute;s en demi-finale face au futur vainqueur, le FC Barcelone. ... Article connexe :&nbsp;<em>Bilan</em>&nbsp;saison par saison du&nbsp;<em>Chelsea</em>&nbsp;Football Club.</p>',
                'image' => 'uploads/V9kdjseLqQlzXJQ2UW553PdNIzFfXksfGpL5jkhR.png',
                'image_portrait' => NULL,
                'status' => 'published',
                'type' => 'paid',
                'is_ended' => 'yes',
                'bet_result' => 'lose',
                'begin' => '2020-11-08 07:55:35',
                'end' => '2020-11-08 08:58:00',
                'publishing_time' => '2020-11-08 07:57:00',
                'created_at' => '2020-11-28 13:23:14',
                'updated_at' => '2020-12-08 08:00:02',
            ),
            2 => 
            array (
                'id' => 7,
                'user_id' => '1',
                'title' => 'Argentina vs Portugaliya',
                'link' => 'argentina-vs-portugaliya',
                'sport_type' => 'Futbol',
                'chempionat' => 'UFC',
                'forecast_hint' => 'Argentina va Portugaliya o\'rtasidagi "jang"',
                'first_team' => 'Argentina',
                'second_team' => 'Portugaliya',
                'bet_sum' => 30000,
                'bet_type' => 'ordinar',
            'forecast_value' => 'TM(3.7)',
                'partner_url' => 'https://1xbet.com/infojahon',
                'kf' => 3.5,
                'explanation' => '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic omnis porro quis esse rerum, in dolorem maiores doloremque obcaecati natus quisquam quod harum odio, voluptate libero facere voluptatum saepe placeat.</p>

<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic omnis porro quis esse rerum, in dolorem maiores doloremque obcaecati natus quisquam quod harum odio, voluptate libero facere voluptatum saepe placeat.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic omnis porro quis esse rerum, in dolorem maiores doloremque obcaecati natus quisquam quod harum odio, voluptate libero facere voluptatum saepe placeat.Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic omnis porro quis esse rerum, in dolorem maiores doloremque obcaecati natus quisquam quod harum odio, voluptate libero facere voluptatum saepe placeat.</p>

<p>&nbsp;</p>

<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic omnis porro quis esse rerum, in dolorem maiores doloremque obcaecati natus quisquam quod harum odio, voluptate libero facere voluptatum saepe placeat.</p>',
                'image' => 'uploads/wMOlNYzTUK0c7or7RxhXo6dzpKUWX5GeNpOv8pZC.jpeg',
                'image_portrait' => NULL,
                'status' => 'published',
                'type' => 'free',
                'is_ended' => 'yes',
                'bet_result' => 'win',
                'begin' => '2020-12-02 07:07:00',
                'end' => '2020-12-08 08:57:00',
                'publishing_time' => '2020-12-08 07:32:00',
                'created_at' => '2020-11-30 07:08:14',
                'updated_at' => '2020-12-08 08:57:02',
            ),
            3 => 
            array (
                'id' => 8,
                'user_id' => '1',
                'title' => 'Rafael Nadal va Roger Federer',
                'link' => 'rafael-nadal-va-roger-federer',
                'sport_type' => 'tennis',
                'chempionat' => 'Wimbledon',
                'forecast_hint' => 'Rafael Nadal ni yutishi kutilmoqda 1xbetda',
                'first_team' => 'Rafael Nadal',
                'second_team' => 'Roger Federer',
                'bet_sum' => 55000,
                'bet_type' => 'ordinar',
            'forecast_value' => 'TM(3.1)',
                'partner_url' => 'https://1xbet.com/infojahon2',
                'kf' => 2.2,
                'explanation' => '<p>lorem2</p>',
                'image' => 'uploads/fQiXCnyCDUGbNNCPI0fleP7II2QxuWm9Rz5un24j.jpeg',
                'image_portrait' => NULL,
                'status' => 'published',
                'type' => 'free',
                'is_ended' => 'yes',
                'bet_result' => 'win',
                'begin' => '2020-11-30 09:57:00',
                'end' => '2020-12-08 07:18:00',
                'publishing_time' => '2020-11-30 08:27:00',
                'created_at' => '2020-11-30 08:59:11',
                'updated_at' => '2020-12-08 07:18:02',
            ),
            4 => 
            array (
                'id' => 9,
                'user_id' => '1',
                'title' => 'ping pong jamoa1 va jamoa2',
                'link' => 'ping-pong-jamoa1-va-jamoa2',
                'sport_type' => 'ping pong',
                'chempionat' => 'ping pong CHEMPIONSHIP',
                'forecast_hint' => 'ping pong jamoa1 va jamoa2 tarif',
                'first_team' => 'jamoa1',
                'second_team' => 'jamoa2',
                'bet_sum' => 25000,
                'bet_type' => 'ordinar',
            'forecast_value' => 'TM(3.9)',
                'partner_url' => 'https://1xbet.com/infojahon3',
                'kf' => 3.4,
                'explanation' => '<p>lorem ping</p>',
                'image' => 'uploads/Y8LM25nS487wF0G4q4o7coTijf8dsS0DalviMCE7.jpeg',
                'image_portrait' => NULL,
                'status' => 'published',
                'type' => 'free',
                'is_ended' => 'yes',
                'bet_result' => 'win',
                'begin' => '2020-11-30 10:06:00',
                'end' => '2020-12-03 05:20:00',
                'publishing_time' => '2020-12-08 05:17:00',
                'created_at' => '2020-11-30 10:06:49',
                'updated_at' => '2020-12-08 05:59:01',
            ),
            5 => 
            array (
                'id' => 10,
                'user_id' => '1',
                'title' => 'bbb',
                'link' => 'bbb',
                'sport_type' => 'Basketbol',
                'chempionat' => 'BUFC',
                'forecast_hint' => '1xbet',
                'first_team' => NULL,
                'second_team' => NULL,
                'bet_sum' => 80000,
                'bet_type' => 'expres',
            'forecast_value' => 'TM(3.1)',
                'partner_url' => 'https://bit.ly/2RCNQOi',
                'kf' => 2.2,
                'explanation' => '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magnam, atque quas eaque quae modi necessitatibus delectus rem et. Pariatur placeat dignissimos nihil dolor! Quaerat, eveniet at! Accusantium, quaerat tenetur?</p>

<p>Neque, corporis non numquam repellat commodi debitis incidunt explicabo reiciendis similique fuga temporibus molestias minus repudiandae fugit libero odit! Nisi ipsa rerum dolorum reiciendis ipsam recusandae ad aliquam dicta similique.</p>

<p>Vel sed, minus perferendis adipisci iure nam veniam deserunt pariatur magnam molestiae minima amet, enim ex quod illo autem rem vitae quia. Recusandae accusantium beatae illo ex eveniet enim ea.</p>

<p>Pariatur quisquam ipsum temporibus saepe nulla odio praesentium culpa cupiditate dolor, est perspiciatis soluta dicta atque animi alias officia, quae natus eligendi excepturi sit nihil exercitationem? Excepturi repudiandae incidunt dignissimos?</p>

<p>Atque itaque eaque saepe assumenda quos? Non dolorem laudantium quo id, nisi repudiandae totam consectetur architecto, earum nobis minus illum quibusdam harum perferendis odit. Voluptatem ipsa quis ratione cumque quidem!</p>

<p>Iure culpa reiciendis modi sit tempora consectetur voluptatum. Ipsa cumque maiores nesciunt dignissimos sunt laudantium delectus, laboriosam veritatis aspernatur modi magni ipsum id ex a officia, maxime inventore dolorem nulla!</p>

<p>A sapiente illum aperiam at id adipisci sint, laboriosam, explicabo tenetur sequi non corporis perspiciatis iste necessitatibus harum quas porro quisquam saepe veritatis officiis sunt dolorum praesentium. Tempora, quisquam placeat!</p>

<p>Totam, eaque earum nisi iure mollitia provident corrupti sequi temporibus nesciunt incidunt expedita molestias? Sint, laudantium. Nisi ea, illum aliquam a sunt nulla officia hic iusto adipisci incidunt dolores culpa.</p>

<p>Sint mollitia voluptates molestiae veritatis incidunt, harum esse, a odio magnam ipsa at fuga non dignissimos explicabo ipsam repellat, aut accusamus. Amet molestiae molestias sed aliquid sunt maxime accusantium rem.</p>

<p>Dolorem, distinctio. Rem repellendus aperiam, quaerat sint tempore voluptatem ex voluptate iste expedita fugiat dicta asperiores in sequi sed, quos officiis dolore ipsa totam. Doloribus laudantium unde alias libero molestias.</p>

<p>Sunt reiciendis repellendus repellat! Libero sunt, expedita adipisci voluptate minus eaque voluptatum? Mollitia alias quod atque accusantium, ipsa animi officiis aliquam accusamus sequi? Recusandae, eveniet at error voluptatibus esse animi!</p>

<p>Officiis cum quae totam quasi nam amet, quo fuga aut, laboriosam impedit, possimus sint minus nihil enim. Dolorum aperiam corrupti, odio fuga necessitatibus, natus enim esse iure minima voluptatibus alias?</p>

<p>Laborum maiores ex excepturi veritatis possimus impedit error eaque accusamus id, illum dicta architecto labore officiis? Dolorem quam facilis hic rem, reprehenderit reiciendis modi inventore tempora quasi, voluptatum praesentium id?</p>

<p>Laborum, officia illum. Quod repellendus asperiores commodi, et dicta hic cumque, suscipit aliquam aperiam assumenda perspiciatis fugit iure, minus at voluptatibus nam debitis quos ullam officia obcaecati autem? Consequatur, animi?</p>

<p>Aliquid, voluptatem qui. Aut sit autem blanditiis accusantium maxime perferendis numquam quisquam ullam voluptatibus a iste, molestiae quam excepturi reiciendis dolores, veritatis non obcaecati enim eaque. Reprehenderit eius voluptate mollitia.</p>',
                'image' => 'uploads/clhPIWop5tozhp6OQlGg5cC6d3XDsnCv1il9Y3Z1.jpeg',
                'image_portrait' => NULL,
                'status' => 'published',
                'type' => 'free',
                'is_ended' => 'yes',
                'bet_result' => 'win',
                'begin' => '2020-12-22 06:20:00',
                'end' => '2020-12-22 07:10:00',
                'publishing_time' => '2020-12-22 06:10:00',
                'created_at' => '2020-12-22 06:08:29',
                'updated_at' => '2020-12-22 07:10:01',
            ),
            6 => 
            array (
                'id' => 11,
                'user_id' => '1',
                'title' => 'gdrgd',
                'link' => 'gdrgd',
                'sport_type' => 'Futbol3',
                'chempionat' => 'UFC',
                'forecast_hint' => '1xbet',
                'first_team' => NULL,
                'second_team' => NULL,
                'bet_sum' => 50000,
                'bet_type' => 'expres',
            'forecast_value' => 'TM(3.9)',
                'partner_url' => 'https://bit.ly/2RCNQOi',
                'kf' => 3.5,
                'explanation' => '<p>tstsetse</p>',
                'image' => 'uploads/ujXVAlfxjsopeiLkSmaCYtWYBn9VeJIxBBSwwN4b.png',
                'image_portrait' => 'uploads/portrait_ujXVAlfxjsopeiLkSmaCYtWYBn9VeJIxBBSwwN4b.png',
                'status' => 'published',
                'type' => 'free',
                'is_ended' => 'yes',
                'bet_result' => 'win',
                'begin' => '2020-12-22 06:30:00',
                'end' => '2020-12-22 07:30:00',
                'publishing_time' => '2020-12-22 06:28:00',
                'created_at' => '2020-12-22 06:36:17',
                'updated_at' => '2020-12-22 07:30:01',
            ),
            7 => 
            array (
                'id' => 12,
                'user_id' => '1',
                'title' => 'ttttttttttttt',
                'link' => 'ttttttttttttt',
                'sport_type' => 'Futbol2',
                'chempionat' => 'China Open',
                'forecast_hint' => 'ttttttttttttt',
                'first_team' => NULL,
                'second_team' => NULL,
                'bet_sum' => 25000,
                'bet_type' => 'expres',
                'forecast_value' => 'SDNFJSNDt',
                'partner_url' => 'https://bit.ly/2RCNQOi',
                'kf' => 3.3,
                'explanation' => '<p>&nbsp;</p>

<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit quibusdam perferendis alias cumque, est dolorum, officiis unde ea inventore nisi nesciunt labore et quasi provident aperiam commodi ad eveniet velit.</p>

<p>Corrupti animi doloremque accusamus eaque, dolores, aut dicta explicabo harum maiores fugiat id saepe nostrum voluptates tenetur. Suscipit quisquam repellendus qui, ducimus nulla quidem iste labore veniam soluta? Numquam, quidem.</p>

<p>Ad porro exercitationem laborum impedit deserunt repudiandae ipsa iste modi dicta sed? Cupiditate laborum nemo nisi beatae dolor odit illum officia aliquam. Ab dignissimos magni nisi veritatis sit dolore deserunt?</p>

<p>Maiores doloremque, neque aperiam velit laborum quam placeat, excepturi molestiae, quia consequatur dolores? Cumque esse ipsam necessitatibus ad itaque incidunt sunt eveniet molestiae id. Dignissimos veritatis explicabo nobis eos ab.</p>

<p>Cumque dignissimos voluptates dolorum illum ipsa incidunt vel corrupti molestias quasi ullam architecto asperiores totam consequatur, culpa distinctio in reiciendis. Totam facere repudiandae deleniti reiciendis! Voluptates laboriosam sunt harum esse.</p>',
                'image' => 'uploads/MnOtCfULljxMjjX22HBKlfaS7q5ehevhOXlo4ShW.jpeg',
                'image_portrait' => 'uploads/portrait_MnOtCfULljxMjjX22HBKlfaS7q5ehevhOXlo4ShW.jpeg',
                'status' => 'published',
                'type' => 'paid',
                'is_ended' => 'yes',
                'bet_result' => 'tie',
                'begin' => '2020-12-22 06:50:00',
                'end' => '2020-12-23 06:45:00',
                'publishing_time' => '2020-12-22 06:45:00',
                'created_at' => '2020-12-22 06:41:56',
                'updated_at' => '2020-12-23 09:09:03',
            ),
            8 => 
            array (
                'id' => 13,
                'user_id' => '1',
                'title' => 'ping pong jamoa1 va jamoa2 - ikkinchi o\'yin',
                'link' => 'ping-pong-jamoa1-va-jamoa2-ikkinchi-o-yin',
                'sport_type' => 'ping pong',
                'chempionat' => 'Wimbledon',
                'forecast_hint' => 'fksjhfjks yutadi',
                'first_team' => 'jamoa1',
                'second_team' => 'jamoa2',
                'bet_sum' => 30000,
                'bet_type' => 'ordinar',
            'forecast_value' => 'TM(3.7)',
                'partner_url' => 'https://1xbet.com/infojahon_uz',
                'kf' => 3.5,
                'explanation' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente hic repellendus dolores facilis sunt necessitatibus reiciendis asperiores, nesciunt vel commodi ipsa! Ad alias magnam doloribus exercitationem commodi. Pariatur, ad quia?</p>

<p>Consequuntur, eligendi nobis dignissimos aperiam aspernatur asperiores nulla recusandae tempora reiciendis dolores, doloremque fuga dolore distinctio laudantium non ab? Dolorum ipsa cum voluptas, repellendus explicabo itaque amet mollitia quod temporibus.</p>

<p>Dolores, commodi recusandae! Odit cupiditate nobis praesentium totam. Facere veritatis culpa cumque, minima assumenda sed harum odit sunt, ab labore, vero ex voluptates. Iure commodi eligendi earum, vero aperiam odit.</p>

<p>Recusandae id laudantium optio libero earum beatae dolorem rerum veniam reiciendis quas nesciunt sunt, consequuntur unde illo. Nihil quia, aperiam eius reprehenderit corrupti praesentium officia itaque rem, fuga ipsa voluptatibus.</p>

<p>Sunt quidem eaque ipsum quos veritatis sit nulla nemo non cupiditate. A optio sint molestiae dolore dolor porro quibusdam distinctio itaque! Blanditiis eius repellat, sapiente ab aliquid nam voluptatem maxime.</p>

<p>Nobis a eveniet, quas iusto vel odio, exercitationem consequuntur reprehenderit magnam dolores esse ipsam deserunt nam, atque porro architecto recusandae fugiat minima. Sapiente ratione, iste repudiandae magni tempora sit error.</p>

<p>Enim, nostrum! Hic vitae amet facere quisquam natus provident? Atque voluptatum, eius iure repellendus, alias a eum doloribus, consectetur voluptas expedita cumque blanditiis accusantium nesciunt itaque porro corporis necessitatibus autem.</p>

<p>Laboriosam eum enim assumenda, repudiandae voluptas cum voluptate? Praesentium tempora dolore laboriosam ipsum odio consequatur, ad iure quisquam cumque labore tempore quod vel architecto assumenda iste non. Ab, id illo.</p>',
                'image' => 'uploads/MjfAs6XUUU23q09OiHjMK2Z9Sl4jVI6eFTzhGnJk.jpeg',
                'image_portrait' => 'uploads/portrait_MjfAs6XUUU23q09OiHjMK2Z9Sl4jVI6eFTzhGnJk.jpeg',
                'status' => 'published',
                'type' => 'paid',
                'is_ended' => 'yes',
                'bet_result' => 'win',
                'begin' => '2020-12-22 11:40:00',
                'end' => '2020-12-22 00:17:00',
                'publishing_time' => '2020-12-22 08:10:00',
                'created_at' => '2020-12-22 08:01:47',
                'updated_at' => '2020-12-22 08:10:01',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('profiles')->delete();
        
        \DB::table('profiles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'first_name' => 'Admin',
                'last_name' => 'Adminovich',
                'url' => NULL,
                'image' => NULL,
                'created_at' => '2020-11-28 13:23:13',
                'updated_at' => '2020-11-28 13:23:13',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'first_name' => 'User',
                'last_name' => 'Userovich',
                'url' => NULL,
                'image' => NULL,
                'created_at' => '2020-11-28 13:23:13',
                'updated_at' => '2020-11-28 13:23:13',
            ),
            2 => 
            array (
                'id' => 9,
                'user_id' => 9,
                'first_name' => 'g1parmonov',
                'last_name' => NULL,
                'url' => NULL,
                'image' => NULL,
                'created_at' => '2020-12-02 06:39:00',
                'updated_at' => '2020-12-02 06:39:00',
            ),
            3 => 
            array (
                'id' => 10,
                'user_id' => 10,
                'first_name' => 'salomdunyouz',
                'last_name' => NULL,
                'url' => NULL,
                'image' => NULL,
                'created_at' => '2020-12-24 18:23:11',
                'updated_at' => '2020-12-24 18:23:11',
            ),
        ));
        
        
    }
}
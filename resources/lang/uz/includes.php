<?php

return[

    // top <=
    'top_working_time' => 'Du-Yak: 9:00-19:00',
    'top_contact_number' => '+998 71 200-05-35 ',
    'top_address_of_shop' => 'Magazinlar manzili',
    // top =>


    // search <=
    'search_title' => 'Telefon aksessuarlar magazini',
    'search_placeholder' => 'Tovarlar bo`yicha izlash',
    // search =>

    // navigation <=
    'nav_menu' => 'Menyu',
    'nav_categories' => 'Kategoriyalar',
    'no_nav_categories' => 'Kategoriya yo`q',
    'nav_about' => 'Haqida',
    'nav_news' => 'Yangiliklar',
    'nav_discounts' => 'Aksiyalar',
    'nav_contact' => 'Aloqa',
    'nav_help' => 'Yordam',
    'nav_terms' => 'Oferta',

    'nav_signin' => 'Kirish',
    'nav_signin_label' => 'Tizimga kiring buyurtmalar qilish uchun va kuzatish uchun',
    'nav_signedin_label' => 'Xush kelibsiz!',
    'nav_signup' => ' Ro`yxatdan o`tish ',
    'nav_favorites' => ' Yoqqanlari ',
    'nav_profile' => ' Profil ',
    'nav_cabinet' => ' Shaxsiy kabinet ',
    'nav_basket' => ' Savatcha ',
    'nav_compare' => ' Taqqoslash ',
    'nav_exit' => 'Chiqish',
    // navigation =>


    // footer <=
    'footer_title' => 'Telefon aksessuarlar magazini',
    'footer_email' => 'elmobile.uz@gmail.com',
    'footer_contact_number' => '+998 71 200-05-35 ',
    'footer_address_of_shop' => 'Magazinlar manzili',

    'footer_help' => 'Yordam',
    'footer_terms' => 'Foydalanish qonunlari',
    'footer_offer_vs_acceptance' => 'Oferta',
    'footer_working_time' => 'Du-Yak: 9:00-19:00',
    'footer_callback_btn' => 'Qaytib qo`ng`iroq qiling',
    // footer =>



    // modal windows <=
    'contact_name' => "Ism",
    'contact_phone' => "Telefon",
    'callmeback_form_submit_btn' => "Qo'ng'iroq qiling"
    // modal windows =>



];

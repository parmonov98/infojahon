<?php

return[
    'no_news_found' => 'Yangilik topilmadi!',
    'not_translated_post_title' => 'Bu yangilik title tarjima qilinmagan',
    'not_translated_post_description' => 'Bu yangilik description tarjima qilinmagan',
    // article <=
    'more_button' => "O'qish",
    // article =>

    // filter <=
    'search' => "Maqola izlash",
    'categories' => 'Kategoriyalar',
    'tags' => 'Teglar',
    // filter =>

];

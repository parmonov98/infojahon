<?php

return[
    // non registered <=
  'login-text' => 'Ushbu mahsulot uchun sharh qoldirish uchun tizimga kiring.',
  'login' => 'Kirish',
    // non registered =>

    // comment <=
  'comment' => 'Sharh qoldiring',
  'coment-text' => 'Sharh yozing',
  'send' => 'Yuborish',
    // comment =>

    // filter <=
  'search' => "Maqola qidirish",
  'categories' => 'Kategoriyalar',
  'tags' => 'Teglar',
    // filter =>

   'no_new_found' => 'Yangilik topilmadi!',
   'no_comment' => 'Izoh mavjud emas!',
   'add_comment_button' => "Jo'natish",
];

<?php
return [
    'login_or_email_invalid' => "Email yoki parol noto'g'ri.",
    'authentication' => "Kirish formasi",
    'remember_me' => 'Meni eslab qol',
    'email' => 'Elektron pochta',
    'password' => "Parol",
    "submit_button" => "Kirish",
    'forgot_password' => "Parolni unitdingizmi?"
];

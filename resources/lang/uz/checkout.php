<?php

// ru

return[
    // form <=
  'name' => 'Ismingiz',
  'surname' => 'Familiyangiz',
  'mail' => 'El. pochta',
  'tel-number' => 'Telefon',
  'region' => 'Viloyat',
  'city' => 'Shahar / Tuman',
  'address' => 'Manzil',
  'post-index' => 'Indeks',
  'mark' => "Qo'shimcha ma'lumot",
  // form =>

    // promo <=
  'promo' => 'Promo kod',
  'send' => 'Yuborish',
    // promo =>

    // orders <=
  'orders' => 'Xaridlaringiz',
  'product' => 'Mahsulot',
  'price' => 'Narx',
  'total-price' => 'Umumiy narx',
  'total-cost' => "To'lov summasi",
  'discount' => 'Chegirma',
  'discount_percentage' => '-:percentage %',
    // orders =>

    // pay type <=
  'pay-type' => "To'lov turi",
  'order' => 'Sotib olish',
    // pay type =>

  'price_value' => ":price so'm",
  'no_discount_found' => 'Promokod noto`g`ri!',
  'expired_coupon_code' => 'Promokod muddati tugagan!',

  'discount_applied' => ":percentage % skidka siz uchun!",
  'choose' => 'Tanlang',
  'no_order_found' => 'Buyurtma topilmadi yoki tugatilgan!',
  'order_updated' => 'Buyurtma ma`lumotlari saqlandi!',
];

<?php

return[
    // table <=
    'product' => 'Mahsulot',
    'price' => 'Narx',
    'number-of' => 'Miqdor',
    'total-price' => 'Umumiy narx',
    'continue' => 'Sotib olish',
    'refresh' => 'Yangilash',
    'no_items_added' => 'Savatingiz mahsulot qo`shing, kutib turaman',
    // table =>

    // order <=
    'total-cart' => 'Jami savatda',
    'total-items' => 'Jami narsalar',
    'summary-price' => 'Umumiy qiymat',
    'refresh' => 'Yangilash',
    'order' => 'BUYURTMA QILISH',
    // order =>

    'go_to_catalog' => 'Katalogga qaytish'
];


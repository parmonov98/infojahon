<?php return [
    'news' => 'Yangiliklar',
    'home' => 'Bosh sahifa',
    'contact' => 'Aloqa',
    'terms' => 'Siyosat',
    'help' => 'Yordam',
    'catalog' => 'Katalog',
    'shop' => 'Katalog',
    'profile' => 'Profil',
    'cart' => 'Savat',
    'search' => 'Qidiruv',
    'login' => 'Kirish',
    'checkout' => 'Buyurtma',
    'compare'=>"Taqqoslash ro'yxati"
];

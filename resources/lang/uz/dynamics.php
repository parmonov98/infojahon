<?php

return [
    'unauthorized_request' => 'Bu so`rov profilga kirishni talab qiladi!',
    'invalid_data_entered' => "Noto'gri ma'lumotlar kiritildi",
    'technical_error_occured' => 'Texnik xatolik sodir bo`ldi!',
    'accept_only_json_format_data_message' =>  'Faqat Json formatdagi ma`lumot qabul qilinadi!',
    'added_to_cart_toast_success' =>'Savatingizga :product_name qo`shildi.',
    'added_to_cart_toast_fail' =>'Savatga qo`shilmadi!',
    'added_to_cart_toast_no_available_product' =>  ':product_name skladda qolmagan!',
    'added_to_cart_toast_no_existing_product' =>  ':product_name bazada bunday tovar mavjud emas!',

    'added_to_favorites_toast_success' =>':product_name yoqqanlar ro`yxatiga qo`shildi .',
    'added_to_favorites_toast_fail' =>':product_name yoqqanlar ro`yxatiga qo`shilmadi!',
    'added_to_comparing_list_toast_success' =>':product_name Taqqoslash ro`yxatiga qo`shildi .',
    'added_to_comparing_list_toast_fail' =>':product_name Taqqoslash ro`yxatiga qo`shilmadi!',
    'already_added_to_comparing_list_toast_success' =>':product_name Taqqoslash ro`yxatiga oldin qo`shilgan!',


    'added_to_callbacks_toast_success' => "Sizning xabaring qabul qilindi siz bilan imkon bo'lishi bilan bog'lanamiz",
    'added_to_callbacks_toast_fail' => "Xabaringizni qabul qilishda texnik xatolik sodir bo'ldi, iltimos qayatada urinib ko'ring yoki telefon raqamlarimizga qo'ng'iroq qiling!",

    'no_added_cart_items' => 'Savatingizdagi mahsulotlar o`chirilgan!',
    'cart_item_amount_increased_successfully' => '+',
    'cart_item_amount_decreased_successfully' => '-',

    'cart_item_deleted_successfully' => "Savatingiz tovar omadli o'chirildi!",

    'cart_items_moved_to_orders_successfully' => 'Siz savatingizdagi tovarlar buyurtmalarga joylashtirildi.',


    'message_accepted_successfully' => "Xabaringiz omadli qabul qilindi!",
    'no_product_found' => "Bu tovar bazada topilmadi!",
    'current_product_is_out_of_stock' => "Bu tovar skladda qolmagan!",

    'added_to_orders_toast_success' => ":product_name ga buyurtmangiz qabul qilindi, sizni buyurtma sahifasiga yo'naltiramiz",

    'review_accepted_successfully' => 'Izohingiz uchun rahmat!',


    'mail_footer' => "Hurmat bilan, Elmobile.uz Jamoasi.",

    'If you’re having trouble clicking the :actionText button, copy and paste the URL below into your web browser:' => '',

    'userwelcome_mail_text' => "ElMobileUz saytida ro'yxatdan o'tishni yakunlash uchun Pochtangizni quyidagi manzil orqali tasdiqlashingiz kerak.",

    'i_confirm_my_email_btn' => 'Emailni tasdiqlayman',

    'welcome_to_profile' => 'Xush kelibsiz, profilga!',
    'already_signed_in_to_profile' => 'Xush kelibsiz, profilga kirgansiz!',

    'profile_update_successfully' => "Profil ma'lumotlari yangilandi!",

    'favorite_item_deleted_successfully' => "Yoqqanlar ro'yxatidan o'chirildi!",
    'your_email_not_verified' => 'Sizning Elektron pochtangiz tasdiqlanmagan, Iltimos pochtangizni tekshiring, sizga tasdiqlash xabari jo`natganmiz!',

    'no_product_characteristics_found' => 'Bu tovar uchun xarakteristika mavjud emas.',

    
    
    'phone_invalid' => "To'g'ri nomer kiriting",
    'email_invalid' => "To'g'ri email kiriting"
];

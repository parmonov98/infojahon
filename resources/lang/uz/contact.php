<?php

return[
      // contact form <=
    'contact-us' => "Biz bilan bog'lanish",
    'name' => 'Ismingiz',
    'tel-number' => 'Telefon',
    'email' => 'El. pochta',
    'message' => 'Xabar',
    'send' => 'Yuborish',
      // contact form=>

      // info <=
    'adress' => 'Bizning manzilimiz',
    'our-adress' => "Buxoro Qayum Murtazoev ko'chasi  5uy.",
    'contact' => 'Kontaktlar',
    'our-tel' => 'Telefon: (99) 994-31-11',
    'our-email' => 'El. pochta: info@mount.uz',
    'working-time' => 'Ish vaqti',
    'our-working-time' => 'Du-Sha: 9:00 - 18:00',
    'our-rest-time' => 'Yak: yopiq',
      // info =>

];


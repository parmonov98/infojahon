<?php

return [
    'signup_title' => 'Ro`yxatdan o`tish',
    'password' => 'Parol',
    'repeat_password' => 'Parolni qayta kiriting',
    'have_an_account' => 'Akkauntiz bormi?',
    'login' => 'Kirish',
    'signup_button' => 'Ro`yxatdan o`tish',
    'email_exists_in_db' => 'Bu email bazada mavjud!',
    'captcha_invalid' => "Tekshiruv kodini to'g'ri kiriting",
    'phone_invalid' => "Telefonni + bilan kiriting",
    'password_invalid' => "Parol 8 belgidan iborat bo'lish kerak"
    

];

<?php

return[
      // product <=
    'add-cart' => "Savatga qo'shish",
    'product_not_found' => 'Mahsulot topilmadi',
    'no_category_found' => "Bu mahsulot kategoriyasi o'chirib yuborilgan",
    'current_product_does_not_exists' => 'Bu product rasmi mavjud emas!',
    'no_translation_title_found' => 'Bu mahsulot uchun title tarjima qilinmagan yoki bazadan o`chib ketgan',
    'no_translation_description_found' => 'Bu mahsulot uchun description tarjima qilinmagan yoki bazadan o`chib ketgan',
      // product =>

      // description <=
    'description' => 'Xususiyatlari va tavsifi',
    'reviews' => "Izohlar",

      // description =>

      // reviews-form <=
    'new-review' => 'Sharh qoldirish',
    'name' => 'Ismingiz',
    'mail' => 'E-mailingiz',
    'review-text' => 'Sizning sharhingiz matni',
    'send' => 'Yuborish',
    'no_comment_found' => 'Bu tovar uchun izoh qoldirilmagan',
    'reviewer_name_invalid' => "Ism uchun noto'g'ri ma'lumot!",
    'reviewer_email_invalid' => "Email uchun mos emas!",
    'review_text_invalid' => "Fikr uchun nimadir yozing!",
      // reviews-form =>

      // non registered <=
    'login-text' => 'Ushbu mahsulot uchun sharh qoldirish uchun tizimga kiring.',
    'login' => 'Tizimga kirish',
      // non registered =>

      // new-products <=
    'new-products' => 'Yangilari',
    'buy' => 'Sotib olish',
      // new-products =>

];

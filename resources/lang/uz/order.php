<?php 

return [
  'shop_title' => 'Elmobile - Telefon aksessuarlar magazini',
  'shop_address' => "Buxoro Qayum Murtazoev ko'chasi 5uy.",
  'shop_phone' => '+998 71 200-05-35',
  "shop_email" => 'elmobile.uz@gmail.com',

  'receipt_for' => 'Check ',
  'receipt_id' => 'Check',
  'payment_perform_time' => "To'lov vaqti",
  
  'description' => "Ta'rif",
  'price' => 'Narx',
  'amount' => 'Soni',
  'all' => 'Umumiy',
  'no_product_found' => "Tovar o'chirilgan yoki mavjud emas",
  
  'total' => 'Umumiy',
  'discount_text' => "Chegirma :percentage%",
  'total_cost' => "To'lov qilingan",

  'thanks_for_purchase' => 'Xarid uchun rahmat!'

];
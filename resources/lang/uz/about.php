<?php

return[
      // about <=
    'about-title' => 'Biz haqimizda',
    'about-text' => '"elmobile" 2019 yil 18 dekabrda Shayxontohur viloyatida ochilgan,
    metro Tinchlik. Mijozlar qalbini zabt etgan do"konimiz
    sotuvchilarning mehribonligi, bugungi kungacha milliondan oshiq yordam berdi
    odamlar o"zlari uchun mos va qulay aksessuani topadilar. Halol
    davomida hurmat qilinmaydigan mahsulotlarimizning sotilishi va sifati
    ko"plab joylar bizning filiallarimizning ochilishining asosiy sababiga aylandi
    ko"p joylar. maqsadlari katta "elmobile",
    har doim o"z mijozlarini xursand qilishdan charchamaydi.',
      // about =>

      // info <=
      'info-year' => '9 yil',
      'info-year-text' => "O'zbekiston texnologiya bozorida",
      'info-clients' => '1000+',
      'info-clients-text' => 'mamnun mijoz',
      'info-products' => '1000+',
      'info-products-text' => 'tovarlar turlari',
      'info-market' => '7',
      'info-market-text' => 'magazin filiallari',
      // info =>

      // brand <=
      'brand' => 'Brendlar',
      // brand =>

      // callback <=
      'question-title' => 'Savollaringiz bormi?',
      'question-text' => "Agar sizda hali ham savollaringiz bo'lsa, qo'ng'iroq qilish uchun so'rovni to'ldiring va biz siz bilan yana bog'lanamiz",
      'name' => 'Ismingiz',
      'tel-number' => 'Telefoningiz raqami',
      'callback' => "Qo'ng'iroq qiling",
      // callback =>

      // review <=
      'review' => 'Bizning xaridorlarimizning fikri',
      // review =>

];


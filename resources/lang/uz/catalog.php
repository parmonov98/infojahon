<?php

return[
    'title' => 'Katalog',
      // filter <=

    'filter-manufacturer' => 'Ishlab chiqaruvchi',
    'filter-price' => 'Narx',
    'filter-more' => "Qo'shimcha",
    'filter-new' => 'Yangi',
    'filter-sale' => 'Chegirma bilan',
    'filter' => 'Tayor',
    'filter-product-price' => 'Narx O`sish bo`yicha',
    'filter-new-old' => 'yangi/eski mahsulot',
    'filter-balance' => 'qolgan mahsulotlar soni',
    'currency_label' => " so'm",
    // filter=>

    'no_category_found' => 'Kategoriya topilmadi.',
    'no_products_found' => "Bu kategoriya bo'yicha tovar topilmadi.",

      // catalog <=
    'buy' => 'Sotib olish',
    'discount_label' => 'Chegirmali',
      // catalog =>

];


<?php

return [
      // hero <=
    'market-name' => "Telefon aksessuarlari do'koni",
    'market-description' => "Progressiv mobil texnologiyalar do'koniga xush kelibsiz. Onlayn do'konimiz eng past narxlarni va eng sifatli mahsulotlarni taklif etadi. Minglab mijozlar allaqachon sharh qoldirishdi, keyingisi bo'ling!",
    'market-catalog' => 'Barcha mahsulotlarimiz',
      // hero =>

      // advantage <=
    'advantage1' => 'Tezkor yetkazib berish',
    'advantage2' => 'Barcha mahsulotlar uchun kafolat',
    'advantage3' => 'Qulay narx',
    'advantage4' => 'Tanlashda sifatli yordam',

    'advantage5' => "Yetkazilganda to'lov qilish ",
    'advantage6' => 'Almashish yoki tovarni qaytarish',
      // advantage =>
    'buy_now' => 'Xarid qilish',
    'new_arrivals' => 'Yangi kelganlar',
      // category <=
    'category' => 'Kategoriyalar',
    'category-go' => "Katalogga o'tish →",
      // category =>

      // new-products <=
    'new-product' => 'Yangi mahsulotlar',
    'buy' => 'Sotib olish',
    'product-price' => ":price so'm",
      // new-products =>

      // sale <=
    'discount_sale' => ':productName ga yetkazib berish va chegirma',
    'sale-date' => ' skidka onlayn buyurtmalar uchun :monthda',
    'sale-buy' => 'Buyurtma berish',
    'months_12' => 'dekabr',
    'months_11' => 'noyabr',
    'months_10' => 'oktabr',
    'months_01' => 'Yanvar',
      // sale =>

      // callback <=
    'question-title' => 'Hali ham savollaringiz bormi?',
    'question-text' => "Agar sizda hali ham savollaringiz bo'lsa, qo'ng'iroq qilish uchun so'rovni to'ldiring va biz siz bilan yana bog'lanamiz",
    'name' => 'Ismingiz',
    'tel-number' => 'Telefoningiz raqami',
    'callback' => "Qo'ng'iroq qiling",
      // callback =>

      // review <=
    'review' => 'Bizning xaridorlarimizning fikri',
      // review =>

      // brand <=
    'brand' => 'Brendlar',
      // brand =>
];




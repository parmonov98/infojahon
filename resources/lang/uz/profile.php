<?php

return[
    'personal_information' => 'Shaxsiy ma`lumotlar',
    'my_orders' => 'Buyurtmalarim',
    'customer_address' => 'Manzil',
    'favorites' => 'Yoqqan tovarlar',
    'edit_button' => 'Ma`lumotlarni o`zgartirish',
    'update_button' => 'Yangilash',


    // <= 'personal_information'
    'phone_number' => 'Telefon Nomer',
    'first_name' => 'Ism',
    'last_name' => 'Familiya',
    'email' => 'E-mail',
    'gender' => 'Jins',
    'password' => 'Parol',
    'telegram' => 'Telegram',

    'male' => 'Erkak',
    'female' => 'Ayol',
    '-' => '-',
    'profile_phone_invalid' => "To'g'ri nomer kiriting",
    'profile_string_invalid' => "Xato ma'lumot yoki bo'sh",
    'profile_numeric_invalid' => "Ro'yxatdan birini tanlang",
    // <= 'personal_information'

    // <= 'orders'
    'order_id' => 'Buyurtma ID',
    'order_date' => 'Buyurtma vaqti',
    'total_cost' => 'Umumiy narxi',
    'customer_name' => 'Murojaat nomi',
    'phone' => 'Telefon',
    'details_are_not_added' => 'Buyurtmat haqida hali ma`lumotlar mavjud emas!',
    // 'address' => 'Адрес', ->^
    // <= 'orders'

    // <= 'addresses'
    'region' => 'Viloyat',
    'district' => 'Tuman',
    'street' => 'Ko`cha',
    'address' => 'Manzil',
    // <= 'addresses'

    // <= 'favorites'
    'no_favorite_products' => 'Hech narsa topilmadi',

    // <= 'favorites'




];

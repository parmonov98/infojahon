<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Foydalanuvchi ma'lumotlari bazadan topilmadi yoki parol noto'g'ri!",
    'throttle' => "Ko'p marta kiriishga urindingiz. Iltimos, :seconds sekundan keyin urinib ko'ring."

];

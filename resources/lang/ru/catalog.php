<?php

return[

    'title' => 'Каталог',
      // filter <=
    'filter-manufacturer' => 'Производитель',
    'filter-price' => 'Цена',
    'filter-more' => 'Дополнительно',
    'filter-new' => 'Новинка',
    'filter-sale' => 'Со скидкой',
    'filter' => 'Готово',
    'filter-product-price' => 'по возрастанию ',
    'filter-new-old' => 'новый/старый товар',
    'filter-balance' => 'число оставшегося товара',

    'currency_label' => ' сум',
      // filter=>

      // catalog <=
    'buy' => 'Купить',
    'no_category_found' => 'Категория не найдено.',
    'no_products_found' => "Нет товаров по этой категории.",
    'discount_label' => 'скидка',
      // catalog =>



];


<?php 

return [
  'shop_title' => 'Elmobile - Магазин телефонных аксессуаров',
  'shop_address' => "г. Бухара ул. Каюм Муртазоев дом-5..",
  'shop_phone' => '+998 71 200-05-35',
  "shop_email" => 'elmobile.uz@gmail.com',

  'receipt_for' => 'Квитанция для',
  'receipt_id' => 'Квитанция',
  'payment_perform_time' => 'дата оплаты',
  
  'description' => 'Описание',
  'price' => 'Цена',
  'amount' => 'Количество',
  'all' => 'Всего',
  'no_product_found' => 'нет продуктов или удален',
  
  'total' => 'Итого',
  'discount_text' => "Скидка :percentage%",
  'total_cost' => 'Всего к оплате',

  'thanks_for_purchase' => 'Спасибо за покупку!'

];
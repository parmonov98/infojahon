<?php

return[
    // table <=
    'product' => 'Товар',
    'price' => 'Цена',
    'number-of' => 'Количество',
    'total-price' => 'Итоговая цена',
    'continue' => 'Продолжить покупку',
    'refresh' => 'Обновить',
    'no_items_added' => 'Ваш корзина пусто, добавьте что-нибуд я подожду!',
    // table =>

    // order <=
    'total-cart' => 'Всего в корзине',
    'total-items' => 'Всего предметов',
    'summary-price' => 'Общая сумма',
    'order' => 'ОФОРМИТЬ',
    // order =>

    'go_to_catalog' => 'Вернутся в каталог'
];


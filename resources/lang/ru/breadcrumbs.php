<?php return [
    'news' => 'Новости',
    'home' => 'Главная',
    'contact' => 'Контакты',
    'terms' => 'Политика',
    'help' => 'Помощ',
    'catalog' => 'Каталог',
    'shop' => 'Каталог',
    'profile' => 'Профил',
    'cart' => 'Корзина',
    'search' => 'Поиск',
    'login' => 'Вход',
    'checkout' => 'Заказ',
    'compare'=>"Список сравнения"
];

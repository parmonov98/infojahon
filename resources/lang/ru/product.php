<?php

return[
      // product <=
    'add-cart' => 'Добавить в корзину',
    'product_not_found' => 'Продукт на найден',
    'no_category_found' => "Это катеория был удален",
    'current_product_does_not_exists' => 'Нет фото!',
    'no_translation_title_found' => 'Этот продукт не доступен с БД или не переведен',
    'no_translation_description_found' => 'Этот продукт не доступен с БД или не переведен',
      // product =>

      // description <=
    'description' => 'Характеристики и описание',
    'reviews' => 'Отзывы',
      // description =>
    
      // reviews-form <=
    'new-review' => 'Оставить отзыв',
    'name' => 'Ваше имя',
    'mail' => 'Ваш e-mail',
    'review-text' => 'Текст вашего отзыва',
    'send' => 'Добавить отзыв',
    'no_comment_found' => 'Еще нет отзывов',
    'reviewer_name_invalid' => "Введите правильно для имя!",
    'reviewer_email_invalid' => "Это не формат Э-почти!",
    'review_text_invalid' => "Введите что для комментарии!",
      // reviews-form =>

      // non registered <=
    'login-text' => 'Авторизуйтесь чтобы оставить отзыв к данному товару.',
    'login' => 'Авторизоваться',
      // non registered =>

      // new-products <=
    'new-products' => 'Новинки',
    'buy' => 'Купить',
      // new-products =>

];

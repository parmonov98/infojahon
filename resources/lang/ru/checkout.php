<?php

// ru

return[
    // form <=
  'name' => 'Имя',
  'surname' => 'Фамилия',
  'mail' => 'Эл. почта',
  'tel-number' => 'Телефон',
  'region' => 'Область',
  'city' => 'Город / Район',
  'address' => 'Адрес',
  'post-index' => 'Индекс',
  'mark' => 'Торговая заметка',
  // form =>
  
    // promo <=
  'promo' => 'Промо код',
  'send' => 'Отправить',
    // promo =>

    // orders <=
  'orders' => 'Ваши покупки',
  'product' => 'Товар',
  'price' => 'Цена',
  'total-price' => 'Итоговая цена',
  'total-cost' => 'К оплате',
  'discount' => 'Скидка',
  'discount_percentage' => '-:percentage %',
    // orders =>

    // pay type <=
  'pay-type' => 'Тип платежа',
  'order' => 'ОФОРМИТЬ',
    // pay type =>

  'price_value' => ":price сум",
  'no_discount_found' => 'Не валидный промокод!',
  'expired_coupon_code' => 'срок Промокода истек!',

  'discount_applied' => ":percentage % скидка для вас!",
  'choose' => 'Выберите',
  'no_order_found' => 'Заказ не найден или заказ обработан!',
  'order_updated' => 'заказ обновлень',
];

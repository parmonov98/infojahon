<?php

return[
    // top <=
    'top_working_time' => 'Пн-В: 9:00-19:00',
    'top_contact_number' => '+998 71 200-05-35 ',
    'top_address_of_shop' => 'Адрес наших магазинов',
    // top =>

    // search <=
    'search_title' => 'Магазин телефонных аксессуаров',
    'search_placeholder' => 'Поиск по товарам',
    // search =>

    // navigation <=
    'nav_menu' => 'Меню',
    'nav_categories' => 'Категория',
    'nav_about' => 'О магазине',
    'nav_news' => 'Новости',
    'nav_discounts' => 'Акции',
    'nav_contact' => 'Контакты',
    'nav_help' => 'Помощ',
    'nav_terms' => 'Оферта',

    'nav_signin' => 'Войти',
    'nav_signin_label' => 'Войдите, чтобы делать покупки и отслеживать заказы',
    'nav_signup' => ' Зарегистрироваться ',
    'nav_favorites' => ' Избранные ',
    'nav_profile' => ' Профиль ',
    'nav_cabinet' => ' Личный кабинет ',
    'nav_basket' => ' Корзина ',
    'nav_compare' => ' Сравнение ',
    'nav_exit' => 'Выход',
    // navigation =>



    // footer <=
    'footer_title' => 'Магазин телефонных аксессуаров',
    'footer_email' => 'elmobile.uz@gmail.com',
    'footer_contact_number' => '+998 71 200-05-35 ',
    'footer_address_of_shop' => 'Адрес наших магазинов',

    'footer_help' => 'Помощь',
    'footer_terms' => 'Политика конфиденциальности',
    'footer_offer_vs_acceptance' => 'Оферта',
    'footer_working_time' => 'Пн-В: 9:00-19:00',
    'footer_callback_btn' => 'Обратный звонок',




    // footer =>
];

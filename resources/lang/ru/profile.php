<?php

return[
    'personal_information' => 'Персональные данные',
    'my_orders' => 'Мои заказы',
    'customer_address' => 'Адреса',
    'favorites' => 'Избранные товары',
    'edit_button' => 'Изменит данные',
    'update_button' => 'Обновить',
    // <= 'personal_information'
    'phone_number' => 'Тел Номер',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'email' => 'E-mail',
    'gender' => 'Пол',
    'password' => 'Пароль',
    'telegram' => 'Телеграм',

    'male' => 'Мужской',
    'female' => 'Женский',
    'profile_phone_invalid' => 'Введите правилный номер телефона',
    'profile_string_invalid' => 'Неправильно или пусто',
    '-' => '-',
    // <= 'personal_information'

    // <= 'orders'
    'order_id' => 'Номер заказа',
    'order_date' => 'Время заказа',
    'total_cost' => 'Общая стоимость',
    'customer_name' => 'Контактное лицо',
    'phone' => 'Телефон',
    'details_are_not_added' => 'данные о заказе еще не предоставлень!',
    // 'address' => 'Адрес', ->^
    // <= 'orders'

    // <= 'addresses'
    'region' => 'Регион',
    'district' => 'Район',
    'street' => 'Улица',
    'address' => 'Адрес',
    // <= 'addresses'


    // <= 'favorites'
    'no_favorite_products' => 'Hech narsa topilmadi',
    // <= 'favorites'


];

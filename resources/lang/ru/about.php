<?php

return[
      // about <=
    'about-title' => 'О нас',
    'about-text' => '"elmobile" открылся 18 декабря 2019 года в Шайхантахурском районе,
    метро Тинчлик. Наш магазин, завоевавший сердца покупателей
    добротой продавцов, на сегодняшний день помог более миллиона
    человек найти подходящий и удобный аксессуар для себя. Честные
    продажи и качество нашей продукции, которые не соблюдаются во
    многих местах, стали основной причиной открытия наших филиалов во
    многих местах. "elmobile", поставив- ший перед собой большие цели,
    не устает всегда радовать своих покупателей.',
      // about =>

    // info <=
    'info-year' => '9 Лет',
    'info-year-text' => 'на рынке технологий в Узбекистане',
    'info-clients' => '1000+',
    'info-clients-text' => 'довольных клиентов',
    'info-products' => '1000+',
    'info-products-text' => 'видов товара',
    'info-market' => '7',
    'info-market-text' => 'филиалы магазина',
    // info =>

    // brand <=
    'brand' => 'Бренды',
    // brand =>

    // callback <=
    'question-title' => 'Остались вопросы?',
    'question-text' => 'Если у вас остались вопросы, оформите заявку на звонок и мы обратно свяжемся с вами',
    'name' => 'Имя',
    'tel-number' => 'Номер телефона',
    'callback' => 'Заказать звонок',
    // callback =>

    // review <=
    'review' => 'Отзывы наших покупателей',
    // review =>

];


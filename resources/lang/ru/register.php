<?php

return [
    'signup_title' => 'Регистрация',
    'password' => 'Пароль',
    'repeat_password' => 'Повторите Пароль',
    'have_an_account' => 'Есть аккаунт',
    'login' => 'Войти',
    'signup_button' => 'Зарегистрироваться',
    'email_exists_in_db' => 'Э-почта существует!',
    'captcha_invalid' => "Введите правильно код проверку",
    'phone_invalid' => "Введите телефон с +",
    'password_invalid' => "Пароль должен содержать не менее 8 символов!"
];

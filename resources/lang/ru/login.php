<?php
return [
    'login_or_email_invalid' => "Эмаил и пароль неправильно.",
    'authentication' => "Авторизация",
    'remember_me' => 'Запомни меня',
    'email' => 'Эл. почта',
    "submit_button" => "Войти",
    'password' => 'Пароль',
    'authentication' => "Авторизация",
    'forgot_password' => "Забылы пароль?"
];

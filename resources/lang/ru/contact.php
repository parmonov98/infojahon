<?php

return[
      // contact form <=
    'contact-us' => 'Связаться с нами',
    'name' => 'Имя',
    'tel-number' => 'Телефон',
    'email' => 'Эл. почта',
    'message' => 'Сообщение',
    'send' => 'Отправить',
      // contact form=>

      // info <=
    'adress' => 'Наш адресс',
    'our-adress' => 'г. Бухара ул. Каюм Муртазоев дом-5.',
    'contact' => 'Контакты',
    'our-tel' => 'Телефон: (99) 994-31-11',
    'our-email' => 'Ел.почта: info@mount.uz',
    'working-time' => 'Рабочее время',
    'our-working-time' => 'Пн-сб: 9:00 - 18:00',
    'our-rest-time' => 'Вс: закрыто',
      // info =>

];


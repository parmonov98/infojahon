@auth
    <div class="col-md-3 col-sm-12  widget mobile-profile">
        <div class="card profile-card pt-4 pb-2">
            <div class="profile-image text-center">
                <a href="/cabinet">
                    <img src="{{$currentUser->image}}" alt="" class="w-50 rounded-circle">
                </a>

            </div>
            <div class="card-body text-center">
                <h3>
                    <a href="{{$currentUser->profile->url ?? '/cabinet'}}">
                        {{$currentUser->username}}
                    </a>
                </h3>
                <p class="text-muted h6">
                    {{$currentUser->profile->first_name}} {{$currentUser->profile->last_name}}
                </p>
                <div class="card-footer">
                    <strong>Hisobdagi pul: </strong><u id="accountBalance">{{$currentUser->account->balance}}</u> so'm.<br/>
                    <strong>Prognozlar soni: </strong><u id="forecasts">{{$currentUser->account->quantity}}</u> ta.
                </div>
            </div>
        </div>
    </div>
@endauth

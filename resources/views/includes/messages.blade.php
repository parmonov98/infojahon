{{-- @if ($errors->any())
    @foreach ($errors as $error)
        <div class="alert alert-danger">
            {{$error}}
        </div>
    @endforeach
@endif --}}
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{-- @if($errors->any())
        {{ implode('', $errors->all("<div class='alert alert-danger'>:message</div>")) }}
@endif --}}

@if (session('success'))
    <div class="mt-4 text-white bg-success text-center font-weight-bold">
        {{session('success')}}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>
@endif

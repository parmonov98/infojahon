<footer class="footer">
    <div class="container-fluid footer__container">
      <div class="footer__block">
        <div class="footer__block__item">
          <img class="logo" src="/imgs/infojahon.png" alt="InfoJahon.RU 1xbet stavkalar">
          <div class="copyright">Barcha huquqlar ximoyalangan &copy; 2020</div>
        </div>
        <div class="footer__block__item">
          <h5 class="widget_title">
            <mark>Menyu</mark>
          </h5>
          <div class="navbar__menu w-75 text-center mx-auto text-danger" style="border-radius: 5px;">
            <a class="menu_link " href="/stats">Statistika</a>
            <a class="menu_link" href="/tariffs">Tariflar</a>
            <a class="menu_link" href="/forecasts/free">Bepul prognozlar</a>
            <a class="menu_link" href="/theories/free">Nazariya</a>
            <a class="menu_link" href="/theories/paid">Pullik strategiya</a>
            <a class="menu_link" href="/contact">Adminga yozish</a>
          </div>
        </div>
        <div class="footer__block__item">
          <h5 class="widget_title">
            <mark>Men bilan aloqa</mark>
          </h5>
          <form class="contact__form" action="/contact" method="POST">
                @csrf
              <div class="inline-group">
                <input type="text" name="name" id="theme" hidden value="{{Request::ip()}}">
                <input type="text" name="phone" id="theme" placeholder="+998xx1234567">
                <input type="email" name="email" id="email" placeholder="E-mail">
              </div>
              <div class="inline-group">
                <textarea name="message" id="message" placeholder="Xabaringizni kiriting" rows="3"></textarea>
                <button class="message__submit__btn" type="submit">
                  <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-8.354 2.646a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L9.793 7.5H5a.5.5 0 0 0 0 1h4.793l-2.147 2.146z"/>
                  </svg>
                </button>
              </div>
          </form>
        </div>
      </div>

	  <div class="row h4 text-white text-center" style="    justify-content: center;font-familiy: 'Nunito', sans-serif">
		 <!-- MyCounter v.2.0 -->
        <script type="text/javascript"><!--
            my_id = 171652;
            my_width = 88;
            my_height = 41;
            my_alt = "MyCounter - счётчик и статистика";
            //--></script>
            <script type="text/javascript"
            src="https://get.mycounter.ua/counter2.0.js">
            </script><noscript>
            <a target="_blank" href="https://mycounter.ua/"><img
            src="https://get.mycounter.ua/counter.php?id=171652"
            title="MyCounter - счётчик и статистика"
            alt="MyCounter - счётчик и статистика"
            width="88" height="41" border="0" /></a></noscript>
            <!--/ MyCounter -->

	  </div>
    </div>
  </footer>

  {{-- <script src="/js/bootstrap.min.js"></script> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
<!-- NOTE: prior to v2.2.1 tiny-slider.js need to be in <body> -->
<script src="/js/scripts.js"></script>


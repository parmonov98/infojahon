@auth
    <div class="widget desktop-profile">
        <div class="card profile-card pt-4 pb-2 border border-warning">
            <div class="profile-image text-center">
                <a href="/cabinet">
                    <img src="{{$currentUser->image}}" alt="" class="w-50 rounded-circle">
                </a>

            </div>
            <div class="card-body text-center">
                <h3>
                    <a href="{{$currentUser->profile->url ?? '/cabinet'}}">
                        {{$currentUser->username}}
                    </a>
                </h3>
                <p class="text-muted h6">
                    {{$currentUser->profile->first_name}} {{$currentUser->profile->last_name}}
                </p>
                <div class="card-footer text-left">
                    <strong>Hisobdagi pul: </strong><u id="accountBalance">{{$currentUser->account->balance}}</u> so'm.<br>
                    <strong>Prognozlar: </strong><u id="accountBalance">{{$currentUser->account->quantity}}</u> ta. <br>
                    @if ($currentUser->account->expire_date)
                    <strong>Muddat: </strong><u id="accountBalance">{{date('d.m,Y h:i', strtotime($currentUser->account->expire_date))}}</u>.
                    @endif
                </div>
            </div>
        </div>
    </div>
@endauth
<div class="widget">
    <div class="col-md-12 px-2">
        <div class="card paid_forecasts pt-4 pb-2 border border-warning">
            <h5 class="widget_title">
                <mark>Rubl</mark> kursi</h5>

            <div class="card-body text-center">
                1 {{ isset($rate['Ccy']) ? $rate['Ccy'] : 'rubl' }} = <span id="rate">{{$rate['Rate'] ?? 135}}</span> so'm.
            </div>
        </div>
    </div>
</div>

<div class="widget social_networks">
    <a href="https://t.me/infojahon_uz" target="_blank" class="telegram">
        <img style="width: 24px;" src="{{asset("assets/img/telegram.png")}}" alt="@infojahon_uz">
        Biz Telegramda
    </a>
    <a href="https://www.youtube.com/channel/UCJebhTOoXzcL-6WlBEEAZVQ" target="_blank" class="youtube">
        <img style="width: 24px;"  src="{{asset("assets/img/youtube.png")}}" alt="infojahon">
        YouTube kanalimiz
    </a>
    <a href="https://www.instagram.com/infojahon/" class="instagram" target="_blank">
        <img style="width: 24px;"  src="{{asset("assets/img/instagram.png")}}" alt="infojahon">
        Biz Instagramda
    </a>
</div>
<div class="widget paid_forecasts border border-warning" >
    <h5 class="widget_title">
        <mark>Pullik</mark>
    <br>
    stavkalar statistika</h5>

    {{-- <select name="paid_forecasts-date" id="paid_forecasts-date">
        <option value="30.08.2018">30.08.2018</option>
        <option value="30.08.2018">31.08.2018</option>
        <option value="30.08.2018">01.09.2018</option>

    </select> --}}

    <div class="forecast__switcher">
        {{-- <a href="#" class="forecast_type">Kunlik</a> --}}
        <a href="#" class="forecast_type active">Beton</a>
    </div>
    <div class="bet__items">
        <div class="bet__item">
        G'alabalar <i>(Yutuqli)</i> <mark class="yellow_corner">{{$sidebarDetails['paid']['all']}}</mark>
        </div>
        <div class="bet__item">
        Yutqazildi<mark class="yellow_corner">{{$sidebarDetails['paid']['losses']}}</mark>
        </div>
        <div class="bet__item">
        Rasxod <i>(Qaytuv)</i> <mark class="yellow_corner">{{$sidebarDetails['paid']['ties']}}</mark>
        </div>

    </div>

    <span class="circled__coofficent">
        O'rtacha KF
        <mark>{{sprintf("%.1f", $sidebarDetails['paid']['average_kf'])}}</mark>
    </span>
</div>

@if ($sidebar && $sidebar->first())
    <div class="widget dynamic_widget border border-warning" style="margin-top: 80px;">
        <div class="widget_content w-100" style="position: relative;">
            {!! $sidebar->first()->content !!}
        </div>
        <a href="{{$sidebar->first()->url}}" target="_blank" class="btn btn-primary widget_button" style="position: absolute; ">{{$sidebar->first()->button}}</a>
    </div>

@endif

<div class="widget free_forecasts border border-warning">
    <h5 class="widget_title">
        <mark> Bepul</mark>
        <br>
        stavkalar statistika
    </h5>

    {{-- <select name="free_forecasts-date" id="free_forecasts-date">
        <option value="30.08.2018">30.08.2018</option>
        <option value="30.08.2018">31.08.2018</option>
        <option value="30.08.2018">01.09.2018</option>

    </select> --}}

    <div class="bet__items">
        <div class="bet__item">
        G'alabalar <i>(Yutuqli)</i> <mark class="yellow_corner">{{$sidebarDetails['free']['all']}}</mark>
        <span class="line">
            <span class="line__fill" style="width: {{$sidebarDetails['free']['wins_percentage'] ?? 0}}%;"></span>
        </span>
        </div>
        <div class="bet__item">
        Yutqazildi<mark class="yellow_corner">{{$sidebarDetails['free']['losses']}}</mark>
        <span class="line">
            <span class="line__fill" style="width: {{$sidebarDetails['free']['losses_percentage'] ?? 0}}%"></span>
        </span>
        </div>
        <div class="bet__item">

        Rasxod <i>(Qaytuv)</i> <mark class="yellow_corner">{{$sidebarDetails['free']['ties']}}</mark>
        <span class="line">
            <span class="line__fill" style="width: {{$sidebarDetails['free']['ties_percentage'] ?? 0}}%;"></span>
        </span>
        </div>

    </div>

    <span class="skewed_rect__coofficent">
        O'rtacha KF {{sprintf("%.1f", $sidebarDetails['free']['average_kf'])}}
    </span>
</div>

@if ($sidebar && $sidebar->count() > 1)
    <div class="widget dynamic_widget border border-warning" style="margin-top: 80px;">
        <div class="widget_content w-100" style="position: relative;">
            {!! $sidebar->last()->content !!}
        </div>
        <a href="{{$sidebar->last()->url}}" target="_blank" class="btn btn-primary widget_button" style="position: absolute; ">{{$sidebar->last()->button}}</a>
    </div>

@endif

<div class="widget telegram_subscription">
    <h5 class="widget_title">
    Telegram bot <br>
    <mark>@Infojahon_bot</mark>
    </h5>


    <a href="https://t.me/infojahon_bot" target="_blank" class="subscribe">

    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
        <path style="fill:#EBF0FA;" d="M135.876,280.962L10.105,225.93c-14.174-6.197-13.215-26.621,1.481-31.456L489.845,36.811
        c12.512-4.121,24.705,7.049,21.691,19.881l-95.571,406.351c-2.854,12.14-17.442,17.091-27.09,9.19l-112.3-91.887L135.876,280.962z"
        />
        <path style="fill:#BEC3D2;" d="M396.465,124.56L135.876,280.962l31.885,147.899c2.86,13.269,18.5,19.117,29.364,10.981
        l79.451-59.497l-65.372-53.499l193.495-191.693C410.372,129.532,403.314,120.449,396.465,124.56z"/>
        <path style="fill:#AFB4C8;" d="M178.275,441.894c5.858,2.648,13.037,2.302,18.85-2.052l79.451-59.497l-32.686-26.749l-32.686-26.749
        L178.275,441.894z"/>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
    </svg>
    Ulanish
    <span class="hint">
        Yutish sirlari, yangilar uchun foydali maslahatlar, va turli strategiyalar
    </span>
    </a>
    <img class="bot_image" src="/imgs/Bot-Telegram-logo.png" alt="Telegram bot">
</div>

<div class="col-md-3 col-sm-12 mb-3">
    <div class="list-group ">
    <a href="/cabinet" class="list-group-item list-group-item-action {{ Route::is('cabinet') ? 'active' : ''}}">Kabinet</a>

    <a href="/addup" class="list-group-item list-group-item-action {{ Route::is('addup') ? 'active' : ''}}">Pul kiritish</a>
    <a href="/withdrawal" class="list-group-item list-group-item-action {{ Route::is('withdrawal') ? 'active' : ''}}">Pul chiqarish</a>
    <a href="/purchased" class="list-group-item list-group-item-action {{ Route::is('purchased') ? 'active' : ''}}">Prognozlarim</a>
    <a href="/payments" class="list-group-item list-group-item-action {{ Route::is('payments') ? 'active' : ''}}">To'lovlar</a>
    {{-- <a href="/notifications" class="list-group-item list-group-item-action {{ Route::is('notifications') ? 'active' : ''}}" >Xabarlar</a> --}}
    <a href="/referals" class="list-group-item list-group-item-action {{ Route::is('referals') ? 'active' : ''}}" >Referallar</a>
    <a href="/affiliate" class="list-group-item list-group-item-action text-dark {{ Route::is('affiliate') ? 'active' : ''}}">
        <span class="bg-warning">
            Pul ishlash
        </span>
    </a>
    <a class="list-group-item list-group-item-action text-danger" href="{{ route('logout') }}"
        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        Chiqish
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>

    </div>
</div>

<nav class="navbar navbar-dark bg-custom fixed-top" id="navbar">
    <div class="container header">

        <div class="logo">

          <a class="navbar-brand" href="{{ url('/') }}">
            <img src="/imgs/infojahon.png" class="logo_img w-100" alt="InfoJahon.RU 1xbet stavkalar">
          </a>

        </div>


        <div class="navbar__menu" id="navbar-menu">
          <a class="menu_link {{ (Request::is('stats') ? 'active' : '') }}" href="/stats">Statistika</a>
          <a class="menu_link {{ (Request::is('tariffs') ? 'active' : '') }}" href="/tariffs">Tariflar</a>
          <a class="menu_link {{ (Request::is('forecasts/free') ? 'active' : '') }}" href="/forecasts/free">Bepul prognozlar</a>

          @auth
          <a class="menu_link {{ (Request::is('forecasts/paid') ? 'active' : '') }}" href="/forecasts/paid">Pullik prognozlar</a>
          @endauth

          <a class="menu_link {{ (Request::is('theories.free') ? 'active' : '') }}" href="/theories/free">Nazariya</a>
          <a class="menu_link {{ (Request::is('theories.paid') ? 'active' : '') }}" href="/theories/paid">Pullik Strategiya</a>
          <a class="menu_link {{ (Request::is('contact') ? 'active' : '') }}" href="/contact">Aloqa</a>


        </div>


        <div class="header__buttons">
            @guest
                {{-- <a class="register_button" href="/register">
                    Ro'yxatdan o'tish
                </a> --}}
                <a class="login_button" href="/login">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-door-open" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M1 15.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5zM11.5 2H11V1h.5A1.5 1.5 0 0 1 13 2.5V15h-1V2.5a.5.5 0 0 0-.5-.5z"/>
                    <path fill-rule="evenodd" d="M10.828.122A.5.5 0 0 1 11 .5V15h-1V1.077l-6 .857V15H3V1.5a.5.5 0 0 1 .43-.495l7-1a.5.5 0 0 1 .398.117z"/>
                    <path d="M8 9c0 .552.224 1 .5 1s.5-.448.5-1-.224-1-.5-1-.5.448-.5 1z"/>
                    </svg>
                    Kirish
                </a>

                {{-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li> --}}
                {{-- @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif --}}
            @else
                <li style="list-style: none;" class="dropdown dropdown ">
                    <a id="navbarDropdown" class="btn btn-white bg-light dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->username ?? Auth::user()->email }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-sm-left dropdown-menu-lg-right " aria-labelledby="navbarDropdown">

                        <a class="dropdown-item" href="{{ route('cabinet') }}">
                            {{ __('Kabinet') }}
                        </a>

                        @if (Auth::user()->role === 'admin')
                            <a class="dropdown-item" target="_blank" href="{{ route('admin') }}">
                                {{ __('Admin panel >') }}
                            </a>
                        @endif

                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Chiqish') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest

        </div>
		<a href="#navbar" class="hamburger">
            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-justify-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M2 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
            </svg>
         </a>
    </div>
  </nav>

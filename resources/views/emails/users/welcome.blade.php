@component('mail::message')
# Salom, Infojahon.RU ga xush kelibsiz!
Bu saytda siz turli sport stavkalar, prognozlar to'g'risida ma'lumotlar olishingiz mumkin.

@component('mail::button', ['url' => 'https://infojahon.ru'])
Saytga o'tish
@endcomponent

Ro'yxatdan o'tganingiz uchun rahmat, <br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
Jahon boy!

Sizga saytdan xabar keldi!

<p>Xabar jo'natuvchi: <b>{{$name}}</b></p>
<p>nomeri: <b>+{{$phone}}</b></p>
<p>emaili: <b>{{$email}}</b></p>
<p>kelgan vaqti: <b>{{$created_at}}</b></p>
<p>Xabar:</p>
<div>
    {{$text}}
</div>
{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

<br/>
Rahmat sizga qabul qilganiz uchun!<br>
{{ config('app.name') }}
@endcomponent

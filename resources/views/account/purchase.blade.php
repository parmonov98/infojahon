@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Sizning profilingiz ma'lumotlari</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="/addup">

                                @csrf
                                <div class="form-group row">
                                    <label for="account_id" class="col-4 col-form-label " >Sizning Hisob raqam: </label>
                                    <div class="col-8">
                                        <input id="account_id" value="{{$user->account->id}}" name="account_id" disabled placeholder="account_id" class="form-control here" required="required" type="text">
                                    </div>
                                    @error('account_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>


                                <div class="form-group row">
                                    <label for="sum" class="col-4 col-form-label">To'ldirish summa(so'm):</label>
                                    <div class="col-8">
                                        {{-- {{$user->tariffs->first()->price}} --}}
                                        <input id="sum" value="10000" name="sum" placeholder="Summa kiriting..." class="form-control here" type="text">
                                    </div>
                                    @error('sum')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                {{-- <div class="form-group row">
                                    <label for="last_name" class="col-4 col-form-label">To'lovga qo'shimcha qilish:</label>
                                    <div class="col-8">
                                        <input id="last_name" value="{{$user->profile->last_name}}" name="last_name" placeholder="Kiriting..." class="form-control here" type="text">
                                    </div>

                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> --}}

                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                    <button name="submit" type="submit" class="btn btn-primary">Hisobni to'ldirish</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('page_styles')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
@endsection

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    @if ($payment)
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>To'lovni tekshirish sahifasi</h4>
                                <hr>
                                <p>
                                    <b>{{$payment->sum}}</b> so'mni <b>{{ env('UZCARD_NUMBERS', "8600 4904 1139 0438") }}</b> plastikka o'tkazib "To'lovni o'tkazdim" tugmasini bosing.
                                    Keyin pastdagi To'lov ID sini nusxalab olib qo'ying. Agar 24 soat ichida hisobingizga pul tushmasa Telegram orqali <a href="https://t.me/jahongir1994" target="_blank">@jahongir1994</a> ga murojaat qiling.
                                </p>

                                <div class="alert alert-success">
                                    Bu oyna har 1 minutda yangilanadi yoki o'zingiz yangilash(F5) tugmasini bosing.
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-2">
                                To'lov ID:
                            </div>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <input type="text" id="paymentId" disabled class="form-control" value="{{$payment->public_id}}" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" onclick="copyToClipboard()" type="button">Nusxalash</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row align-items-center mt-2">
                            <div class="col-md-2">
                                Holati:
                            </div>
                            <div class="col-md-4">
                                {{-- {{$payment->status}} --}}
                                @switch($payment->status)
                                    @case('processing')
                                        <span class="spinner-border text-success spinner-border-sm" role="status" aria-hidden="true"></span>
                                        @break
                                    @case('confirmed')
                                        <i class="fa fa-check " aria-hidden="true"></i>
                                        @break
                                    @case('paid')
                                        <i class="fa fa-check text-success" aria-hidden="true"></i>
                                        @break
                                    @case('unpaid')
                                        <i class="fa fa-exit"></i>
                                        @break
                                    @default

                                @endswitch

                            </div>
                        </div>
                        @if ($payment->status == 'processing')
                            <div class="row align-items-center mt-4">
                                <div class="col-md-12 text-center">
                                    <a href="/payment/{{$payment->public_id}}?status=transferred"  class="btn btn-primary btn-block">To'lovni o'tkazdim</a>
                                </div>
                            </div>
                        @endif

                    </div>
                    @else
                    Bu to'lovni amalga oshirishda kerakli ma'lumotlar baza nofaollik sababli o'chirilgan
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection


@section('page_scripts')
<script>
    setTimeout(() => {
        window.location.reload();
    }, 60000);


    function copyToClipboard() {
    /* Get the text field */
        var copyText = document.getElementById("paymentId");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        alert("Buferga yozildi: " + copyText.value);
    }

</script>
@endsection

@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Hisobga kiruvchi mablag'lar:</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="bg-warning" >
                                      <tr class="">
                                        <th scope="col">№</th>
                                        {{-- <th scope="col">ID</th> --}}
                                        <th scope="col">Summa</th>
                                        <th scope="col">Vaqti</th>
                                        <th scope="col">Turi</th>
                                        <th scope="col">Holati</th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($user->payments as $i => $payment)
                                            <tr>
                                                <th scope="row">{{$i+1}}</th>
                                                {{-- <td>{{$payment->id}}</td> --}}
                                                <td>{{$payment->sum}} so'm</td>
                                                <td>{{date("d.m, Y H:i", strtotime($payment->created_at))}}</td>
                                                <td>{{$payment->type == 'purchase' ? "Sotib olish" : "To'ldirish" }}</td>
                                                <td>{{$payment->status == 'paid' ? "To'langan" : "To'lanmagan" }}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                  </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card mt-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Hisobdan chiquvchi mablag'lar:</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="bg-warning">
                                      <tr>
                                        <th scope="col">№</th>
                                        {{-- <th scope="col">ID</th> --}}
                                        <th scope="col">Summa</th>
                                        <th scope="col">Turi</th>
                                        <th scope="col">Vaqti</th>
                                        <th scope="col">Holati</th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($user->withdrawals as $i => $withdrawal)
                                            <tr>
                                                <th scope="row">{{$i+1}}</th>
                                                {{-- <td>{{$withdrawal->id}}</td> --}}
                                                <td>{{$withdrawal->sum}} so'm</td>
                                                <td>{{$withdrawal->wallet == 'yandex_money' ? 'Yandex koshelok' : 'Qiwi'}}</td>
                                                <td>{{date("d.m, Y H:i", strtotime($withdrawal->created_at))}}</td>
                                                <td>{{$withdrawal->status == 'paid' ? "To'langan" : "To'lanmagan" }}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                  </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

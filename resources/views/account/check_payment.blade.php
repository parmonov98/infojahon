@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    @if ($purchase && $purchase->payment)
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>To'lovni tekshirish sahifasi</h4>
                                <hr>
                                <p>
                                    To'lovni amalga ishirish uchun sizni yangi oynaga yo'naltiramiz, o'sha oyna orqali to'lov amalga oshirib ushbu oynaga qaytib kelsangiz sizni kerakli sahifaga yo'naltiramiz!
                                </p>

                                <div class="alert alert-success">
                                    Bu oyna har 1 minutda yangilanadi yoki o'zingiz yangilash(F5) tugmasini bosing.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                To'lov ID:
                            </div>
                            <div class="col-md-8">
                                {{$purchase->id}}-{{$purchase->payment->id}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                Holati:
                            </div>
                            <div class="col-md-4">
                                @if ($purchase->payment->status === 'processing')
                                    <span class="spinner-border text-success spinner-border-sm" role="status" aria-hidden="true"></span>
                                @endif
                                @if ($purchase->payment->status === 'paid')
                                    <i class="fa fa-check"></i>
                                @endif
                                @if ($purchase->payment->status === 'unpaid')
                                    <i class="fa fa-exit"></i>
                                @endif
                            </div>
                        </div>

                        <form method="POST" enctype="multipart/form-data" id="paymentForm" action="/addup/{{$purchase->payment_id}}" class="">

                            @csrf
                            <div class="form-group row invisible">
                                <label for="account_id" class="col-4 col-form-label " >Sizning Hisob raqam: </label>
                                <div class="col-8">
                                    <input id="account_id" value="{{$currentUser->account->id}}" name="account_id" disabled placeholder="account_id" class="form-control here" required="required" type="text">
                                </div>
                                @error('account_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <div class="form-group row invisible">
                                <label for="sum" class="col-4 col-form-label">To'ldirish summa(so'm):</label>
                                <div class="col-8">
                                    {{-- {{$user->tariffs->first()->price}} --}}
                                    <input id="sum" value="{{$purchase->payment->sum}}" name="sum" placeholder="Summa kiriting..." class="form-control here" type="text">
                                    <input hidden id="rate" value="{{ number_format($rate['Rate'], 2) }}" name="rate">
                                </div>
                                @error('sum')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <div class="offset-4 col-8">
                                <button name="submit" type="submit" id="subMitPaymentForm" class="btn btn-primary">To'lov qilish</button>
                                </div>
                            </div>
                            </form>
                        <script>
                            window.addEventListener('DOMContentLoaded', function () {
                                document.getElementById('subMitPaymentForm').click();
                            });
                            setTimeout(() => {
                                window.location.reload();
                            }, 60000);
                        </script>

                    </div>
                    @else
                    Bu to'lovni amalga oshirishda kerakli ma'lumotlar baza nofaollik sababli o'chirilgan
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

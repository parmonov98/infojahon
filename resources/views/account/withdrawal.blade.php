@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Hisobdan pul chiqarish uchun zayavka berish</h4>
                                <hr>

                                <div class="form-group row">
                                    <div class="alert alert-warning w-100 pb-0">
                                        <ul>
                                            <li>Diqqat bu ko'rsatilgan summa rublda beriladi!</li>
                                            <li>Pul chiqarish qo'lda amalga oshiriladi!</li>
                                        </ul>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="{{route('withdrawalStore')}}">

                                    @csrf
                                    @if ($user->account->yandex_money || $user->account->qiwi)
                                    <div class="form-group row">
                                        <label for="account_id" class="col-4 col-form-label "> Siz hamyoningiz: </label>
                                        <div class="col-8">
                                            <input type="hidden" name="account_id" value="{{$user->account->id}}">
                                            <select class="form-control" name="withdrawal_method" id="withrawal_method">
                                                @if ($user->account->yandex_money && $user->account->yandex_money != '-')
                                                <option value="yandex_money">Yandex koshelok</option>
                                                @endif

                                                @if ($user->account->qiwi && $user->account->qiwi != '-')
                                                <option value="qiwi">Qiwi koshelok</option>
                                                @endif

                                            </select>
                                        </div>
                                        @error('withrawal_method')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row">
                                        <label for="sum" class="col-4 col-form-label">Chiqarish summa(so'm):</label>
                                        <div class="col-8">
                                            {{-- {{$user->tariffs->first()->price}} --}}
                                            <input hidden id="rate" value="{{ number_format($rate['status'] == 'online' ? $rate['Rate'] : env('EXCHANGE_RATE'), 142) }}" name="rate">
                                            <input id="sum" value="10000" name="sum" placeholder="Summa kiriting..." class="form-control here" type="text">
                                        </div>
                                        @error('sum')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group row">
                                        <label for="sum" class="col-4 col-form-label"></label>
                                        <label for="sum" class="col-5 col-form-label py-0" id="previewPrice"></label>
                                    </div>

                                    <script>
                                        document.addEventListener('DOMContentLoaded', function (e) {
                                            let sum = parseFloat(document.getElementById('sum').value);

                                            // console.log(sum);
                                            const rate = parseFloat(document.getElementById('rate').value);
                                            // console.log(rate);
                                            document.getElementById('previewPrice').textContent = (sum / rate).toFixed(2) + ` rubl`;

                                            document.getElementById('sum').addEventListener('keyup', function (e) {
                                                let sum = parseFloat(document.getElementById('sum').value);
                                                // console.log(sum);
                                                const rate = parseFloat(document.getElementById('rate').value);
                                                // console.log(rate);
                                                document.getElementById('previewPrice').textContent = (sum / rate).toFixed(2) + ` rubl`;
                                            });

                                        });
                                        //
                                        </script>
                                    <div class="form-group row">
                                        <div class="offset-4 col-8">
                                            <button name="submit" type="submit" class="btn btn-primary">Pul chiqarishga yuborish</button>
                                        </div>
                                    </div>
                                    @else
                                    <h3>Koshelokni oldin kiriting</h3>
                                    @endif
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

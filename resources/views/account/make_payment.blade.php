@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Siz <a href="#" id="tariffTitle">{{$tariffs->current->tariffLabel}}</a> sotib olyapsiz!</h4>
                                <hr>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="/paybill">

                                @csrf

                                <div class="form-group row">
                                    <div class="alert alert-warning w-100 pb-0">
                                        <ul>
                                            <li>Diqqat bu ko'rsatilgan summa rublda qabul qilinadi!</li>
                                            <li>Agar qaysidir tariffga skidka mavjud bo'lsa, avtomat hisoblanadi!</li>
                                        </ul>

                                    </div>

                                </div>

                                <div class="form-group px-2">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="payment_method" onclick="paymentMethod(this)" id="paymentMethodFreeKassa" value="freekassa" checked>
                                        <label class="form-check-label" for="paymentMethodFreeKassa">
                                          FreeKassa orqali to'lov
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <input class="form-check-input" type="radio" name="payment_method" onclick="paymentMethod(this)" id="paymentMethodAccount" value="account">
                                        <label class="form-check-label" for="paymentMethodAccount">
                                          Ichki hisobdan <span class="summary text-danger"></span>
                                        </label>
                                      </div>
                                </div>
                                <div class="form-group row align-items-end">

                                    <div class="col-lg-6 col-md-12">
                                        <label for="tariff">Tariff tanlang</label>
                                        <select class="form-control" id="tariff" name="tariff_id">
                                            @foreach ($tariffs as $tariff)
                                                @if ($tariff->type === 'piece')
                                                    <option {{ $tariff->id == $tariffs->current->id ? 'selected' : '' }} data-price="{{ $tariff->price }}" value="{{ $tariff->id }}" data-discount="{{ $tariff->discount }}">
                                                        {{ $tariff->quantity }} ta prognoz({{ $tariff->price - ($tariff->price / 100 * $tariff->discount) }} so'm)
                                                    </option>
                                                @else
                                                    <option {{$tariff->id == $tariffs->current->id ? 'selected' : ''}} data-price="{{ $tariff->price }}" value="{{ $tariff->id }}" data-discount="{{ $tariff->discount }}">
                                                        {{ $tariff->term }} kunlik prognoz({{ $tariff->price - ($tariff->price / 100 * $tariff->discount) }} so'm)
                                                    </option>
                                                @endif

                                            @endforeach

                                        </select>
                                    </div>
                                    <label for="sum" class="col-lg-1 col-md-10 col-form-label text-left"> =></label>
                                    <div class="col-lg-3 col-md-10 ml-auto">
                                        {{-- {{$user->tariffs->first()->price}} --}}
                                        <input disabled id="tariff_id" value="{{$tariffs->current->id}}" name="tariff_id" type="hidden">
                                        <input data-rate="{{$rate['Rate']}}" id="sum1"
                                                value="{{ number_format(($tariffs->current->price - ($tariffs->current->price / 100 * $tariffs->current->discount) ) / $rate['Rate'], 2) }}"
                                                name="sum" placeholder="Summa kiriting..." class="form-control" readonly type="text">
                                        <input hidden id="sum"
                                                value="{{ $tariffs->current->price }}" name="sum">
                                        <input hidden id="rate" value="{{ number_format($rate['Rate'], 2) }}" name="rate">
                                    </div>
                                    <label for="sum" class="col-lg-2 col-md-2 col-form-label text-left"> Rubl.</label>
                                    @error('sum')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <script>
                                    var tariff = document.getElementById('tariff');
                                    function paymentMethod(el) {
                                        console.log(el);
                                        if (el.value === 'freekassa') {
                                            document.getElementById('paymentMethodAccount').nextElementSibling.querySelector('.summary').classList.add('invisible');

                                        }else{
                                            const tariffPrice = document.getElementById('sum1');

                                            // console.log(tariffPrice);
                                            if (parseFloat(document.getElementById('accountBalance').textContent) >= parseFloat(tariffPrice.value)) {

                                                // console.log(parseFloat(document.getElementById('accountBalance').textContent));
                                                console.log(parseFloat(document.getElementById('rate').value));
                                                // console.log((parseFloat(tariffPrice.value) * parseFloat(document.getElementById('rate').textContent)).toFixed(2));
                                                document.getElementById('paymentMethodAccount').nextElementSibling.querySelector('.summary').textContent =
                                                        `(${parseFloat(document.getElementById('accountBalance').textContent)}
                                                        - ${( Math.ceil(parseFloat(tariffPrice.value) * parseFloat(document.getElementById('rate').value)) ).toFixed(2) } =
                                                        ${(parseFloat(document.getElementById('accountBalance').textContent)
                                                        - Math.ceil(parseFloat(tariffPrice.value) * parseFloat(document.getElementById('rate').value))).toFixed(2) }
                                                        )`;
                                                console.log(document.getElementById('paymentMethodAccount').checked);
                                                if (document.getElementById('paymentMethodAccount').checked === true) {
                                                    document.getElementById('paymentMethodAccount').nextElementSibling.querySelector('.summary').classList.remove('invisible');
                                                }

                                            }else{

                                                document.getElementById('paymentMethodFreeKassa').click();
                                                document.getElementById('paymentMethodAccount').nextElementSibling.querySelector('.summary').classList.remove('invisible');
                                                document.getElementById('paymentMethodAccount').nextElementSibling.querySelector('.summary').textContent = 'pul yetarli emas!';
                                                setTimeout(() => {
                                                    document.getElementById('paymentMethodAccount').nextElementSibling.querySelector('.summary').classList.add('invisible');

                                                }, 5000);

                                            }

                                        }
                                    }
                                    // accountBalance



                                    tariff.addEventListener('change', function (e) {

                                        document.getElementById('tariffTitle').textContent = e.target.options[e.target.selectedIndex].textContent;
                                        const rate = document.getElementById('sum1');
                                        // console.log();

                                        let price = (e.target.options[e.target.selectedIndex].dataset.price -
                                                        (
                                                            parseFloat(e.target.options[e.target.selectedIndex].dataset.price)
                                                            / 100
                                                            * parseFloat(e.target.options[e.target.selectedIndex].dataset.discount)
                                                        )
                                                    ) / parseFloat(rate.dataset.rate);

                                        rate.value = price.toFixed(2);

                                        if (document.getElementById('paymentMethodAccount').checked) {
                                            paymentMethod(document.getElementById('paymentMethodAccount'));
                                        } else {
                                            paymentMethod(document.getElementById('paymentMethodFreeKassa'));
                                        }
                                        // console.log(e.target.options[e.target.selectedIndex].dataset.discount);

                                    })
                                </script>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <button name="submit" type="submit" class="btn btn-primary btn-lg btn-block">Tarif xarid qilish</button>
                                    </div>
                                </div>
                                <h6 class="alert alert-danger w-100">Free-kassa.ru tomonidan qo'shimcha komissiya bor va to'lov turiga qarab belgilanadi!</h6>
                            </form>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

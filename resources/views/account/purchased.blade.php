@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Sotib olgan prognozlaringiz:</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="bg-warning">
                                      <tr>
                                        <th scope="col">№</th>
                                        <th scope="col">Nomi</th>
                                        <th scope="col" title="Prognoz joylangan vaqt">Yaratilgan</th>
                                        <th scope="col">Holati</th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($forecasts as $i => $forecast)
                                        @if ($forecast)
                                        <tr>
                                            <th scope="row">{{$i+1}}</th>
                                            <td>
                                                <a href="/forecasts/{{$forecast->link}}">
                                                    {{$forecast->title}}
                                                </a>
                                            </td>
                                            <td>{{date("d.m, Y H:i", strtotime($forecast->created_at))}}</td>
                                            <td>{{$forecast->is_ended == 'yes' ? "Tugagan" : "Kutilmoqda" }}</td>
                                        </tr>

                                        @endif
                                        @endforeach

                                    </tbody>
                                  </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

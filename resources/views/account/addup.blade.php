@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Sizning profilingiz ma'lumotlari</h4>
                                <hr>

                                <div class="form-group row">
                                    <div class="alert alert-warning w-100 pb-0">
                                        <ul>
                                            <li>Diqqat bu ko'rsatilgan summa FreeKassa orqali Rublda qabul qilinadi!</li>
                                            <li>Free-Kassa to'lov tizimi o'z komissiyasi bor va u to'lov turiga qarab belgilanadi!</li>
                                        </ul>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="/addup">

                                    @csrf
                                    <div class="form-group row">
                                        <label for="account_id" class="col-4 col-form-label " >Sizning Hisob raqam: </label>
                                        <div class="col-8">
                                            <input id="account_id" value="{{$user->account->id}}" name="account_id" disabled placeholder="account_id" class="form-control here" required="required" type="text">
                                        </div>
                                        @error('account_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>


                                    <div class="form-group row">
                                        <label for="sum" class="col-4 col-form-label">To'ldirish summa(so'm):</label>
                                        <div class="col-8">
                                            {{-- {{$user->tariffs->first()->price}} --}}
                                            <input hidden id="rate" value="{{ number_format($rate['status'] == 'online' ? $rate['Rate'] : 135, 2) }}" name="rate">
                                            <input id="sum" value="10000" name="sum" placeholder="Summa kiriting..." class="form-control here" type="text">
                                        </div>
                                        @error('sum')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group row">
                                        <label for="sum" class="col-4 col-form-label"></label>
                                        <label for="sum" class="col-5 col-form-label py-0" id="previewPrice"></label>
                                    </div>
                                    <div class="form-group row">

                                        <label for="sum" class="col-4 col-form-label">To'lov turi:</label>

                                        <div class="form-check mr-2">
                                            <input class="form-check-input" type="radio" name="payment_method" id="paymentMethodUzcard" value="uzcard" checked>
                                            <label class="form-check-label" for="paymentMethodUzcard">
                                                UZCARD
                                            </label>
                                        </div>
                                        <br/>
                                        <div class="form-check ml-2">
                                            <input class="form-check-input" type="radio" name="payment_method" id="paymentMethodFreeKassa" value="freekassa">
                                            <label class="form-check-label" for="paymentMethodFreeKassa">
                                                Free-Kassa orqali
                                            </label>
                                        </div>
                                    </div>

                                    <script>
                                        document.addEventListener('DOMContentLoaded', function (e) {
                                            let sum = parseFloat(document.getElementById('sum').value);

                                            // console.log(sum);
                                            const rate = parseFloat(document.getElementById('rate').value);
                                            // console.log(rate);
                                            document.getElementById('previewPrice').textContent = (sum / rate).toFixed(2) + ` rubl`;

                                            document.getElementById('sum').addEventListener('keyup', function (e) {
                                                let sum = parseFloat(document.getElementById('sum').value);

                                                // console.log(sum);
                                                const rate = parseFloat(document.getElementById('rate').value);
                                                // console.log(rate);
                                                document.getElementById('previewPrice').textContent = (sum / rate).toFixed(2) + ` rubl`;
                                            });

                                        });
                                        //
                                    </script>
                                    <div class="form-group row">
                                        <div class="offset-4 col-8">
                                        <button name="submit" type="submit" class="btn btn-primary">Hisobni to'ldirish</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

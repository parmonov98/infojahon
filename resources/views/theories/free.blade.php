@extends('layouts.app')

@section('content')

<div class="timer_forecasts mt-2">
    <div class="new_forecasts_table" id="timerForecasts">
      <a class="font-weight-bold bg-primary text-white">
          <div class="date_td">Vaqti</div>
          <div>Stavka turi</div>
          <div>Chempionat</div>
          <div>Jamoalar</div>
          <div>Kupon kod</div>
          <div class="rate">KF</div>
          {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
          <div>Taymer</div>
      </a>
      @forelse($timerForecasts as $item)
      <a href="{{route('forecasts.show', $item->link)}}">
              <div class="date_td">{{date("m.d, H:i", strtotime($item->begin))}}</div>
              <div>{{$item->sport_type}}</div>
              <div>{{$item->chempionat}}</div>
              <div>{{$item->first_team}} - {{$item->second_team}}</div>
              <div>{{$item->forecast_value}}</div>
              <div class="rate">{{$item->kf}}</div>
              {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
              <div class="timer " data-datetime="{{date("M d, Y H:i:s", strtotime($item->begin))}}">
                    <span class="days"></span>
                    <span class="time">13:04:49</span>
                </div>
      </a>
      @empty
      Prognozlar hali chiqarilmagan!
      @endforelse


    </div>
    <p class="hint"><a href="/history/">Barcha prognozlar</a></p>
</div>

<div class="free-theories__section mt-2">
    <h3 class="free-forecast__title">
      <mark>Nazariya</mark>
    </h3>

    <div class="row free-theories__items">

        @if (count($free) > 0)


        <div class="column1">

            @foreach ($free as $item)
                <div class="free-theories__items_i border border-primary">
                    <span class="item_rating">
                    <mark>Reyting: {{ number_format($item->rating, 1, '.', ',') }}</mark>
                    </span>
                    <div class="item_image">
                    <img src="/storage/{{$item->image}}" alt="{{$item->title}}">
                    </div>
                    <div class="item_content">
                    <h4 class="item_title text-center" style="border-radius: 5px; background-color: #3490dc30;">{{$item->title}} </h4>
                        <p class="item_description">
                            {!! substr(strip_tags($item->content), 0, 120)!!}
                        </p>
                        <div class="item_footer">
                        <a href="/theories/{{$item->link}}" class="gototheory">To'liq o'qish </a>
                        <span class="item_views">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                            <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                            </svg>
                            {{$item->count_views}}
                        </span>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        @else
        Hali nazariyalar qo'shilmagan yoki chiqarilmagan
        @endif
    </div>

    <div class="mt-4">
        {{ $free->links() }}
    </div>

    <div class="row">
        @if($widgets->content->count() != 0)
        <div class="widget dynamic_widget border border-warning" style="margin-top: 80px;">
            <div class="widget_content d-flex w-100" style="position: relative; " >
                {!! $widgets->content->first()->content !!}
            </div>
            <a href="{{ $widgets->content->first()->url }}" target="_blank" class="btn btn-primary widget_button" style="position: absolute; ">{{ $widgets->content->first()->button }}</a>
        </div>
        @endif
    </div>
</div>



 <br>
 <br>
 <br>
@endsection


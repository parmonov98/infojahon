
@extends('layouts.app')

@section('page_styles')
    <style>
        /* "ShareThis" background image */
        /* .share {
            background: transparent url(https://cloud.githubusercontent.com/assets/1392632/7971761/3072de0a-0a15-11e5-9045-fd7e3c06d721.png) 3px 0 no-repeat;
            height: 32px;
            width: auto;
        }  */

    </style>
@endsection

@section('content')
<div class="timer_forecasts">
    <div class="new_forecasts_table" id="timerForecasts">
      <a class="font-weight-bold bg-primary text-white">
          <div class="date_td">Vaqti</div>
          <div>Stavka turi</div>
          <div>Chempionat</div>
          <div>Jamoalar</div>
          <div>Kupon kod</div>
          <div class="rate">KF</div>
          {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
          <div>Taymer</div>
      </a>
      @forelse($timerForecasts as $item)
      <a href="{{route('forecasts.show', $item->link)}}">
              <div class="date_td">{{date("m.d, H:i", strtotime($item->begin))}}</div>
              <div>{{$item->sport_type}}</div>
              <div>{{$item->chempionat}}</div>
              <div>{{$item->first_team}} - {{$item->second_team}}</div>
              <div>{{$item->forecast_value}}</div>
              <div class="rate">{{$item->kf}}</div>
              {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
              <div class="timer " data-datetime="{{date("M d, Y H:i:s", strtotime($item->begin))}}">
                    <span class="days"></span>
                    <span class="time">13:04:49</span>
                </div>
      </a>
      @empty
      Prognozlar hali chiqarilmagan!
      @endforelse


    </div>
    <p class="hint"><a href="/history/">Barcha prognozlar</a></p>
</div>



<div class="col-md-12 mt-2">
    <div class="item_image">
        <img class="w-100" src="/storage/{{$theory->image}}" alt="Bepul stavkalar">
    </div>
    <hr class="bg-primary">
    <div class="item_content mt-2 border border-primary p-1">
        <h2 class="item_title text-boldest bg-warning text-center border border-primary" style="border-radius: 5px; border-width: 3px;">
            {{$theory->title}}
        </h2>
        <br>
        <p class="d-flex" style="align-items: baseline !important; font-size: 10px;">
            <span class="blue_text d-inline-flex align-items-end mr-1">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                    <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                </svg>
            </span>
            <strong>{{date('d.m.Y', strtotime($theory->created_at)) }} </strong>
            <span class="blue_text d-inline-flex align-items-baseline ml-4 mr-1">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                </svg>
            </span>
            <strong>{{ $theory->count_views}} </strong>

        </p>
        <p>
            {{-- <span class="blue_text">Prognoz</span> --}}
            {!!$theory->content!!}
        </p>



    </div>

    <a target="_blank" class="btn btn-lg btn-block btn-warning border border-primary mt-2 " style="color: #e04a0c; font-weight: 700;" href="https://bit.ly/2RCNQOi" target="_blank">1.5 mln so'm bonus olish</a>

</div>
<div class="row mt-3">
    <div class="col-md-12 text-center ">
        <p class="h5 text-white"><span class="bg-primary px-2">Do'stlarga yuborish</span></p>
        <div class="sharethis-inline-share-buttons text-center"></div>
    </div>
</div>

<br/>
<br/>
<br/>

@endsection


@section('page_scripts')
<script src="https://rawgit.com/rgrove/lazyload/master/lazyload.js"></script>
<script>
        // LazyLoad ShareThis on mouseover
        var share = document.querySelector('.share');

        var loadShareThis = function () {
        // https://github.com/rgrove/lazyload
        LazyLoad.js('https://w.sharethis.com/button/buttons.js', function () {
            // ShareThis Options
            stLight.options({publisher: "UUID", doNotHash: true, doNotCopy: false, hashAddressBar: true});
        });

        // Remove background
        this.style.background = 'none';

        // Remove event listener
        this.removeEventListener('mouseover', loadShareThis, false);
        };

        // Add event listener
        share.addEventListener('mouseover', loadShareThis, false);
</script>
@endsection

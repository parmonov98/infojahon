@extends('layouts.app')

@section('content')






<div class="free-theories__section ">
    <h3 class="free-forecast__title">
      <mark>Bepul</mark>  Nazariya
    </h3>

    <div class="row free-theories__items">

        @if (count($free) > 0)


        <div class="column1">

            @foreach ($free as $item)
                <div class="free-theories__items_i border border-primary">
                    <span class="item_rating">
                    <mark>Reyting: {{ number_format($item->rating, 1, '.', ',') }}</mark>
                    </span>
                    <div class="item_image">
                    <img src="/storage/{{$item->image}}" alt="{{$item->title}}">
                    </div>
                    <div class="item_content">
                    <h4 class="item_title text-center" style="border-radius: 5px; background-color: #3490dc30;">{{$item->title}} </h4>
                        <p class="item_description">
                            {!! substr(strip_tags($item->content), 0, 120)!!}
                        </p>
                        <div class="item_footer">
                        <a href="/theories/{{$item->link}}" class="gototheory">To'liq o'qish </a>
                        <span class="item_views">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                            <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                            </svg>
                            {{$item->count_views}}
                        </span>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        @else
            Hali nazariyalar qo'shilmagan yoki chiqarilmagan
        @endif
        </div>

        {{ $free->links() }}


</div>

{{--
<div class="paid-theories__section">

    <h3 class="paid-forecast__title">
        <mark>Pullik</mark>  Nazariya
    </h3>
    <div class="row paid-theories__items">
        <div class="column1">

        <div class="paid-theories__items_i">
            <span class="item_rating">
            <mark>Reyting: 4.5</mark>
            </span>
            <div class="item_image">
            <img src="/storage/imgs/free-theory-1.png" alt="Free theroy 1">
            </div>
            <div class="item_content">
            <h4 class="item_title">СТРАТЕГИЯ «ТОТАЛ БОЛЬШЕ
                1,5 ГОЛА» </h4>
                <p class="item_description">
                Сегодня мы продолжаем знакомить читателей с различными стратегиями ставок на футбол. Предлагаем вашему вниманию систему ставок на «тотал больше 1,5 голов» в матче.
                </p>
                <div class="item_footer">
                <a href="#" class="gototheory">sotib olish</a>
                <span class="item_views">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                    </svg>
                    1246
                </span>
                </div>
            </div>
        </div>
        <div class="paid-theories__items_i">
            <span class="item_rating">
            <mark>Reyting: 4.5</mark>
            </span>
            <div class="item_image">
            <img src="/storage/imgs/free-theory-1.png" alt="Free theroy 1">
            </div>
            <div class="item_content">
            <h4 class="item_title">СТРАТЕГИЯ «ТОТАЛ БОЛЬШЕ
                1,5 ГОЛА» </h4>
                <p class="item_description">
                Сегодня мы продолжаем знакомить читателей с различными стратегиями ставок на футбол. Предлагаем вашему вниманию систему ставок на «тотал больше 1,5 голов» в матче.
                </p>
                <div class="item_footer">
                <a href="#" class="gototheory">sotib olish</a>
                <span class="item_views">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                    </svg>
                    1246
                </span>
                </div>
            </div>
        </div>
        </div>
        <div class="column2">
        <div class="paid-theories__items_i tall">
            <span class="item_rating">
            <mark>Reyting: 4.5</mark>
            </span>
            <div class="item_image">
            <img src="/storage/imgs/free-theory-3.png" alt="Free theroy 1">
            </div>
            <div class="item_content">
            <h4 class="item_title">
                СТРАТЕГИЯ <mark>«ТОТАЛ БОЛЬШЕ 1,5 ГОЛА»</mark>
            </h4>
                <p class="item_description">
                Сегодня мы продолжаем знакомить читателей с различными стратегиями ставок на футбол. Предлагаем вашему вниманию систему ставок на «тотал больше 1,5 голов» в матче.
                </p>
                <div class="item_footer">
                <a href="#" class="gototheory">sotib olish</a>
                <span class="item_views">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                    </svg>
                    1246
                </span>
                </div>
            </div>
        </div>
        </div>

    </div>
  </div> --}}




  <div class="paid-theories__section">
    <h3 class="paid-forecast__title">
        <mark>Pullik</mark>  Strategiya
    </h3>
    <div class="row paid-theories__items wide">
        @if ($paid)


        @foreach ($paid as $item)
        <div class="paid-theories__items_i border border-primary">
          <span class="item_rating">
            <mark>Reyting: {{ number_format($item->rating, 1, '.', ',') }}</mark>
          </span>
          <div class="col-md-4">
            <img class="w-100" src="/storage/{{$item->image}}" alt="{{$item->title}}">
          </div>
          <div class="item_content col-md-8">
            <h4 class="item_title text-center py-1" style="border-radius: 5px; background-color: #f5f1078e;">{{$item->title}} </h4>

              {!! Str::words($item->content, 15) !!}
              <div class="item_footer">
                <a href="/theories/{{$item->link}}" class="gototheory">sotib olish</a>
                <span class="item_views">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"></path>
                    <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"></path>
                  </svg>
                  {{$item->count_views}}
              </span>
              </div>
          </div>
        </div>
        @endforeach
        @else
            Hali pullik nazariyalar mavjud emas!
        @endif


    </div>
    {{ $paid->links()}}
  </div>

  <div class="ad_banner__section">

    <div class="banner_content">
      <img src="{{asset('/imgs/person.png')}}" alt="Telegram" class="person">
    <h4 class="ad_banner__title">
      Telegram kanalimizda  <mark>Bepul</mark>      Stavkalar
    </h4>
    <a href="#telegram" class="subscribe">

      <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
        <path style="fill:#EBF0FA;" d="M135.876,280.962L10.105,225.93c-14.174-6.197-13.215-26.621,1.481-31.456L489.845,36.811
        c12.512-4.121,24.705,7.049,21.691,19.881l-95.571,406.351c-2.854,12.14-17.442,17.091-27.09,9.19l-112.3-91.887L135.876,280.962z"
        />
        <path style="fill:#BEC3D2;" d="M396.465,124.56L135.876,280.962l31.885,147.899c2.86,13.269,18.5,19.117,29.364,10.981
        l79.451-59.497l-65.372-53.499l193.495-191.693C410.372,129.532,403.314,120.449,396.465,124.56z"/>
        <path style="fill:#AFB4C8;" d="M178.275,441.894c5.858,2.648,13.037,2.302,18.85-2.052l79.451-59.497l-32.686-26.749l-32.686-26.749
        L178.275,441.894z"/>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
      </svg>
      Obuna bo'lish
      <span class="hint">
        Tugmachani bosib hoziroq a'zo bo'ling.Biz bilan online pul ishlang.
      </span>
    </a>
  <img src="/imgs/papaznaet-logo-in-banner.png" class="telegram_channel">
  <img src="/imgs/telegram-lg-icon.png" alt="Telegram" class="telegram_md_icon">
    </div>

  </div>


 <br>
 <br>
 <br>

@endsection


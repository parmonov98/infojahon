
@extends('layouts.app')

@section('content')
<div class="timer_forecasts">
    <div class="new_forecasts_table" id="timerForecasts">
      <a class="font-weight-bold bg-primary text-white">
          <div class="date_td">Vaqti</div>
          <div>Sport turi</div>
          <div>Chempionat</div>
          <div>Jamoalar</div>
          <div>Kupon kod</div>
          <div class="rate">KF</div>
          {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
          <div>Taymer</div>
      </a>
      @forelse($timerForecasts as $item)
      <a href="{{route('forecasts.show', $item->link)}}">
              <div class="date_td">{{date("m.d, H:i", strtotime($item->begin))}}</div>
              <div>{{$item->sport_type}}</div>
              <div>{{$item->chempionat}}</div>
              <div>{{$item->first_team}} - {{$item->second_team}}</div>
              <div>{{$item->forecast_value}}</div>
              <div class="rate">{{$item->kf}}</div>
              {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
              <div class="timer " data-datetime="{{date("M d, Y H:i:s", strtotime($item->begin))}}">
                    <span class="days"></span>
                    <span class="time">13:04:49</span>
                </div>
      </a>
      @empty
      Prognozlar hali chiqarilmagan!
      @endforelse


    </div>
    <p class="hint"><a href="/history/">Barcha prognozlar</a></p>
</div>


<div class="col-md-12 pb-2 mt-2">
    <div class="item_image">
        <img class="w-100" src="/storage/{{$forecast->image}}" alt="Bepul stavkalar">
    </div>

    <div class="item_content border border-primary p-1 mt-3">
        <h3 class="item_title bg-primary text-white font-weight-bold text-center py-2" style="border-radius: 5px;">
            {{ $forecast->title}}
        </h3>
        <br>
        <p style="font-size: 12px;text-align: center;">
            <span class="blue_text">
                <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                    <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                </svg>
            </span>
            <strong class="mr-4">{{date('d.m.Y', strtotime($forecast->begin)) }} </strong> Boshlanish vaqti: <strong>{{date('H:i', strtotime($forecast->begin))}}</strong>
            <span class="blue_text ml-4">KF: </span>
            <strong>{{$forecast->kf}}</strong>
        </p>
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <td>

                        Chempionat: <br>
                        <b>{{$forecast->chempionat}}</b>
                    </td>
                    <td>
                        Sport turi: <br>
                        <b>{{$forecast->sport_type}}</b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a target="_blank" class="btn btn-lg btn-block btn-warning border border-primary" href="{{$forecast->partner_url}}">1.5 mln bonus olish</a>
                    </td>
                </tr>
            </table>
        </div>
        <hr class="border-2 border-primary">
        <p>
            {{-- <span class="blue_text">Prognoz</span> --}}
            {!!$forecast->explanation!!}
        </p>
        <br>
        <p class="color-danger">
            <div class="input-group mb-3">
                <input type="text" id="stavkaCode" style="border: 1px solid #ccb825;" class="form-control" value="{{$forecast->forecast_value}}" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button style="background-color: #ffe62e; color: #000;" class="btn btn-outline-secondary" onclick="copyToClipboard()" type="button">Kupon kodni kopiya qilish</button>
                </div>
            </div>
            {{-- <span class="blue_text">Prognoz</span> --}}
          {{-- {{$forecast->forecast_value}} --}}
          {{-- {{$forecast->bet_value}} --}}
        </p>
        <script>
            function copyToClipboard() {
            /* Get the text field */
                var copyText = document.getElementById("stavkaCode");

                /* Select the text field */
                copyText.select();
                copyText.setSelectionRange(0, 99999); /*For mobile devices*/

                /* Copy the text inside the text field */
                document.execCommand("copy");

                /* Alert the copied text */
                alert("Buferga yozildi: " + copyText.value);
            }
        </script>
    <div>

    <div class="card-footer" style="border: 1px solid #ccb825;">
        <div class="row px-2">
            <div class="col-md-6 border border-secondary py-1 d-inline-flex align-items-center">
                <span class="tournament_icon mr-2" style="    display: inline-block;
                width: 32px;
                background-repeat: no-repeat;
                height: 32px; background-image: url({{asset('assets/img/trophy.png')}})">

                </span>
                Chempionat:
                <strong>{{$forecast->chempionat}}</strong>
            </div>
            <div class="col-md-3 border-top border-bottom border-right border-secondary py-1 d-inline-flex align-items-center">
                <span class="tournament_icon mr-2" style="    display: inline-block;
                width: 32px;
                background-repeat: no-repeat;
                height: 32px; background-image: url({{asset('assets/img/running.png')}});
                ">

                </span>
                Sport turi:
                <strong>{{$forecast->sport_type}}</strong>
            </div>
            <div class="col-md-3 border-top border-bottom border-right border-secondary py-1 d-inline-flex align-items-center">
                <span class="tournament_icon mr-2" style="    display: inline-block;
                width: 36px;
                background-repeat: no-repeat;

                height: 32px; background-image: url({{asset('assets/img/clock.png')}});
                ">

                </span>
                <strong>{{date('d.m.Y H:m:i', strtotime($forecast->begin)) }} </strong>
            </div>

        </div>
        <div class="row mt-3 px-2">
            <a target="_blank" href="{{$forecast->partner_url}}" style="border: 1px solid #e04a0c;" class="btn-warning  bg-lg btn-block h4 p-2 text-center mt-2">Stavka qo'yish</a>
        </div>
    </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12 text-center ">
        <p class="h5 text-white"><span class="bg-primary px-2">Do'stlarga yuborish</span></p>
        <div class="sharethis-inline-share-buttons text-center"></div>
    </div>
</div>



<br/>
<br/>
<br/>

@endsection

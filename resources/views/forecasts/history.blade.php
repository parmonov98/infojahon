@extends('layouts.app')

@section('content')


<div class="free_forecasts__section">
    <div class="section_header">
      <h3 class="free_forecasts__title">
        <mark>Arxivlangan</mark> Prognozlar
      </h3>
      <p class="bg-primary p-1 rounded text-center text-white">Bu yerdagi prognozlar tugagan va bularni amalda qo'llamang!</p>
    </div>
    @if (count($forecasts) > 0)


    <div class="free_forecasts__items d-flex flex-wrap">
        @foreach ($forecasts as $forecast)

            <div class="col-6 free_forecasts__items_i flex-column border border-primary">
                <div class="item_image">
                    <img src="{{asset( "storage/" . $forecast->image)}}" alt="Bepul stavkalar">
                </div>
                <div class="item_content " style="">
                    <h3 class="item_title mt-1" style="">{{$forecast->title}}</h3>
                    <p class="my-0 " >
                        <span class="blue_text mr-0">
                            <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                            </svg>
                        </span>
                        <strong class="ml-1 mr-4">{{date('d.m.y', strtotime($forecast->begin)) }}</strong>

                        <span class="blue_text">KF</span>
                        {{$forecast->kf}}
                        <br/>
                        Boshlanish vaqti: <strong class="ml-1">{{date('H:i', strtotime($forecast->begin))}}</strong>
                    </p>
                    <p>
                        <span class="blue_text">Prognoz</span>
                        {{$forecast->forecast_value}}
                    </p>
                    <div class="mt-0 ">
                        <a href="{{route('forecasts.show', $forecast->link)}}" class="item_btn" style="">Prognozni ko'rish</a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="row">
        {{$forecasts->links()}}
    </div>
    @else
        Prognoz qo'shilmagan yoki e'lon qilinmagan.
    @endif

  </div>


@if($widgets->content->count() != 0)
<div class="widget dynamic_widget border border-warning" style="margin-top: 80px;">
    <div class="widget_content d-flex w-100" style="position: relative; " >
        {!! $widgets->content->first()->content !!}
    </div>
    <a href="{{ $widgets->content->first()->url }}" target="_blank" class="btn btn-warning widget_button" style="position: absolute; ">{{ $widgets->content->first()->button }}</a>
</div>
@endif

  <div class="ad_banner__section">

    <div class="banner_content">
        <img src="/imgs/person.png" alt="Telegram" class="person">

    <h4 class="ad_banner__title">
        Telegram kanalimizda  <mark>Bepul</mark>      Stavkalar
    </h4>
    <a href="#telegram" class="subscribe">

        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
        <path style="fill:#EBF0FA;" d="M135.876,280.962L10.105,225.93c-14.174-6.197-13.215-26.621,1.481-31.456L489.845,36.811
        c12.512-4.121,24.705,7.049,21.691,19.881l-95.571,406.351c-2.854,12.14-17.442,17.091-27.09,9.19l-112.3-91.887L135.876,280.962z"
        />
        <path style="fill:#BEC3D2;" d="M396.465,124.56L135.876,280.962l31.885,147.899c2.86,13.269,18.5,19.117,29.364,10.981
        l79.451-59.497l-65.372-53.499l193.495-191.693C410.372,129.532,403.314,120.449,396.465,124.56z"/>
        <path style="fill:#AFB4C8;" d="M178.275,441.894c5.858,2.648,13.037,2.302,18.85-2.052l79.451-59.497l-32.686-26.749l-32.686-26.749
        L178.275,441.894z"/>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        </svg>
        Obuna bo'lish
        <span class="hint">
        Tugmachani bosib hoziroq a'zo bo'ling.Biz bilan online pul ishlang.
        </span>
    </a>
        <img src="/imgs/papaznaet-logo-in-banner.png" class="telegram_channel">
        <img src="/imgs/telegram-lg-icon.png" alt="Telegram" class="telegram_md_icon">
    </div>

    </div>

  {{-- <div class="free_forecasts__section">
    <div class="section_header">
      <h3 class="free_forecasts__title">
        <mark>Prognozlar</mark>  Statistikasi

      </h3>
    </div>
    <div class="row free_forecasts__items">
      <div class="col-md-12 free_forecasts__items_i">
        <div class="item_image">
          <img src="/storage/imgs/free-forecast1-on-page.png" alt="Bepul stavkalar">
        </div>

        <div class="item_content">
          <h3 class="item_title">БЕНФИКА — ФЕНЕРБАХЧЕ  <span class="item_type">G'alaba</span></h3>
          <br>
          <p>
            <span class="blue_text">
              <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
              </svg>
            </span>
            24.07.2018  Boshlanish vaqti: 22:00
          </p>
          <p>
            <span class="blue_text">KF</span>
            1.8 <mark>(O'rtacha koefitsient)</mark>
          </p>
          <p>
            <span class="blue_text">Prognoz</span>
            ТБ(2)
          </p>

          <div>
            <a href="#buy" class="item_btn">Prognozni olish</a>
          </div>
        </div>
      </div>
      <div class="col-md-12 free_forecasts__items_i">
        <div class="item_image">
          <img src="/storage/imgs/free-forecast1-on-page.png" alt="Bepul stavkalar">
        </div>

        <div class="item_content">
          <h3 class="item_title">БЕНФИКА — ФЕНЕРБАХЧЕ  <span class="item_type">G'alaba</span></h3>
          <br>
          <p>
            <span class="blue_text">
              <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
              </svg>
            </span>
            24.07.2018  Boshlanish vaqti: 22:00
          </p>
          <p>
            <span class="blue_text">KF</span>
            1.8 <mark>(O'rtacha koefitsient)</mark>
          </p>
          <p>
            <span class="blue_text">Prognoz</span>
            ТБ(2)
          </p>

          <div>
            <a href="#buy" class="item_btn">Prognozni olish</a>
          </div>
        </div>
      </div>

    </div>
  </div> --}}

@endsection


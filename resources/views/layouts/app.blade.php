<!doctype html>
<html lang="uz">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Jahongir.RU') }}</title>


    {{-- <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ffc80d1ea07e80018406868&product=image-share-buttons" async="async"></script> --}}
    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ffc897889e276001860597e&product=inline-share-buttons" async="async"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('page_styles')
</head>
<body>
    <div id="app">

        @include('../includes.navbar')

        <div id="navbar_bottom"></div>

        <div class="wrapper mb-4 mt-lg-5">


            <div class="container-fluid pt-2" >
              @include('../includes.messages')
              <div class="row">
                  @if (Request::is('contact') || Request::is('login') || Request::is('register'))
                    <main class="col-sm-12 col-md-8 mx-auto content mx-auto mt-3 mb-4  mt-lg-3">
                        @yield('content')
                    </main>
                    @else
                        <main class="col-sm-12 col-md-8 content p-0 px-2 mt-lg-3 {{ Request::path() }}">

                            @yield('content')

                        </main>

                        <aside class="col-sm-12 col-md-4 col-lg-3 sidebar mb-5">
                            @include('../includes.sidebar')
                        </aside>
                    @endif


              </div>
            </div>
        </div>


        @include('../includes.footer')

    </div>




    <a class="arrow_up invisible" id="arrow_up" onclick="window.scrollTo(0, 0)"></a>
    {{-- <script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-beta.2/lazyload.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.3.0/dist/lazyload.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    @yield('page_scripts')

    <script>
        window.addEventListener('scroll', function (e){
            // console.log(window.pageYOffset);
            if (window.pageYOffset > 600) {
                document.getElementById('arrow_up').classList.remove('invisible');
            } else {
                document.getElementById('arrow_up').classList.add('invisible');
            }
        });


    document.addEventListener('DOMContentLoaded', function(){

var lazyLoadInstance = new LazyLoad({
  // Your custom settings go here
});

        if (document.getElementById("timerForecasts")) {
            // The data/time we want to countdown to
            var timers = document.getElementById("timerForecasts").querySelectorAll('.timer');
            timers.forEach(function (item) {
                // "Jan 25, 2021 16:37:52"
                var countDownDate = new Date(item.dataset.datetime).getTime();

                // Run myfunc every second
                var myfunc = setInterval(function() {

                var now = new Date().getTime();
                var timeleft = countDownDate - now;
                // Calculating the days, hours, minutes and seconds left
                var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
                // days = days < 10 ?  '0' + days : days;
                var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                hours = hours < 10 ?  '0' + hours : hours;
                var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
                minutes = minutes < 10 ?  '0' + minutes : minutes;
                var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
                seconds = seconds < 10 ?  '0' + seconds : seconds;
                // Result is output to the specific element
                var time = hours + ":" + minutes + ':' + seconds;

                if (days !== 0) {
                    item.querySelector(".days").innerHTML = days + " kun ";
                }
                item.querySelector(".time").innerHTML = time;

                // Display the message when countdown is over
                if (timeleft < 0) {
                    clearInterval(myfunc);
                    item.querySelector(".days").innerHTML = "";
                    item.querySelector(".time").innerHTML = "<span class='text-danger'>Live</span>";
                }



                }, 1000);
            });

        }
    });

    </script>
</body>
</html>

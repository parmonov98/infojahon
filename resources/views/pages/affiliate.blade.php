{{-- {{ddd($currentUser)}} --}}
@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Pul ishlamoqchisiz? men siz uchun imkon beraman!</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                Pul ishlash uchun sizdan, pul ishlashga qiziquvchi do'stlaringizni pastdagi <strong>maxsus manzil</strong> orqali taklif qiling va ular men tavsiya qiladigan prognozlarni yoki tariflarni sotib olsa, sizning hisobingizga
                                <strong>{{ env('AFFILIATE_PERCENT', 20) }}%</strong> summa yoziladi.
                                <br>
                                Bu pulni chiqarib olish uchun pul chiqarishga buyurtmani adminga yuborasiz, Pul chiqarishga berilgan buyurtmangiz summasi joyiga tashlab beriladi.
                                <br/>
                                <strong>Ish kunlari:</strong> Dushanba-Juma gacha.
                                <hr/>
                                <strong>Sizning maxsus manzilingiz:</strong>
                                <div class="input-group mb-3">
                                    <input disabled type="text" id="referalLink" class="form-control" value="{{Config::get('app.url')}}/register?referer={{$currentUser->username}}" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" onclick="copyToClipboard()" type="button">Nusxalash</button>
                                    </div>
                                </div>

                                <script>
                                    function copyToClipboard() {
                                    /* Get the text field */
                                        var copyText = document.getElementById("referalLink");

                                        /* Select the text field */
                                        copyText.select();
                                        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

                                        /* Copy the text inside the text field */
                                        document.execCommand("copy");

                                        /* Alert the copied text */
                                        alert("Buferga yozildi: " + copyText.value);
                                    }
                                </script>

                                <div class="text-danger ">Endi buferga olingan manzilni do'stlaringizga jo'nating!</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

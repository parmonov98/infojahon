@extends('layouts.app')

@section('content')


<!-- content -->

<div class="timer_forecasts mt-2">
    <div class="new_forecasts_table" id="timerForecasts">
      <a class="font-weight-bold bg-primary text-white">
          <div class="date_td">Vaqti</div>
          <div>Stavka turi</div>
          <div>Chempionat</div>
          <div>Jamoalar</div>
          <div>Kupon kod</div>
          <div class="rate">KF</div>
          {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
          <div>Taymer</div>
      </a>
      @forelse($timerForecasts as $item)
      <a href="{{route('forecasts.show', $item->link)}}">
              <div class="date_td">{{date("m.d, H:i", strtotime($item->begin))}}</div>
              <div>{{$item->sport_type}}</div>
              <div>{{$item->chempionat}}</div>
              <div>{{$item->first_team}} - {{$item->second_team}}</div>
              <div>{{$item->forecast_value}}</div>
              <div class="rate">{{$item->kf}}</div>
              {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
              <div class="timer " data-datetime="{{date("M d, Y H:i:s", strtotime($item->begin))}}">
                    <span class="days"></span>
                    <span class="time">13:04:49</span>
                </div>
      </a>
      @empty
      Prognozlar hali chiqarilmagan!
      @endforelse


    </div>
    <p class="hint"><a href="/history/">Barcha prognozlar</a></p>
</div>


<div class="paid__section mt-2">
    <div class="section_header">
      <div class="">
        <h3 class="paid__title">
          <mark>Prognozlarni</mark> sotib olish
        </h3>
        <p class="paid__title_description text-center">
          Donali yoki muddatli tarifflarni tanlang.
        </p>
      </div>
      <div class="section_right switcher">
        <div class="forecast__switcher">
          {{-- <a href="#" class="forecast_type bet_of_day ">Kun stavkasi</a> --}}
          <a href="#" class="forecast_type active">Beton stavkalar</a>
        </div>
      </div>
    </div>


    <div class="row paid__items">
        @if (count($tariffs->pieces) > 0)
        @foreach ($tariffs->pieces as $tariff)
            @if ($tariff->type !== 'term')
            @if ($tariff->discount > 0)
            <div class="col-md-3 w-md-50 paid__items_i stavka_dnya with_discount displayed">
                <div class="discount">
                    Skidka <span class="discount_percentage">{{$tariff->discount}}%</span>
                </div>
            @else
                <div class="col-md-3 paid__items_i stavka_dnya displayed ">
            @endif

                <h4 class="item_header " styl="font-size: 1.1em !important;">

                        {{$tariff->quantity}}

                    ta prognoz</h4>
                <div class="item_content">
                <h3 class="item_price " style="font-size: 1.5em;">
                    @if ($tariff->discount != 0 && $tariff->discount != null)
                        <span class="actual_price">{{$tariff->price}} so'm.</span>
                        {{$tariff->price - $tariff->discount/100 * $tariff->price}} so'm
                    @else
                    {{$tariff->price}} so'm
                    @endif
                </h3>
                <ul class="list item_advantages">
                    <li class="list_item">Yutuq extimoli 75%</li>
                    <li class="list_item">O'rtacha koeffitsient 3</li>
                    <li class="list_item">Har kunlik rejim</li>
                    <li class="list_item">Yutish sirlari o'rgatiladi</li>
                    <li class="list_item">Stavka yutqazilganda bepul beriladi</li>
                </ul>

                <a href="/tariffs/{{$tariff->id}}" class="item_buy_btn">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                    </svg>
                    Sotib olish
                </a>
                </div>
            </div>
            @endif
        @endforeach
        @else
            Hali tariflar qo'shilmagan.
        @endif
    </div>

    <div class="row mt-4 forecasts_by_period displayed">
        <div class="item_banner">
            <img src="/imgs/telegram-lg-icon.png" alt="Telegram Kanal" class="telegram_lg_icon">
          <h3 class="item_title">Hoziroq <a href="https://t.me/infojahon_uz"><mark>Telegramdagi</mark></a>vip kanalimga ulaning</h3>

            <p class="item_description">
              Har kunlik Liniya ,live va ordinar stavkalar. 2+ Expreslar.Bankni to'g'ri taqsimlash va yutish sirlari o'rgatiladi
            </p>

            <div class="item_stats">
              <div class="stats_bank border border-primary">
                <span class="">
                  Oxrigi haftada berilgan prognozlarni o'tish foizi
                </span>
                <span class="yellow_text" style="font-size: 1.5em;">{{sprintf("%.1f", $statDetails['last_weeks_wins_percentage'] ?? 0)}}%</span>
              </div>
              <div class="stats_bets border border-primary">
                <span class="">
                  Oxrigi haftada berilgan prognozlar soni
                </span>
                <span class="yellow_text" style="font-size: 1.5em;"> {{$statDetails['last_weeks_all']}} ta</span>
              </div>
            </div>

        </div>

        <div class="forecasts_by_period__items displayed">
            @foreach ($tariffs->terms as $tariff)
                @if ($tariff->type == 'term')
                    @if ($tariff->discount != 0)
                        <div class="forecasts_by_period__items_i mt-2">

                            <span class="item_discount_percent">
                                <mark>-{{$tariff->discount}}%</mark>
                            </span>
                            <h6 class="item_period">{{$tariff->term}} kunlik Vip </h6>

                            <h5 class="item_price">
                            @if ($tariff->discount != 0)
                                <span class="actual_price"> {{$tariff->price}} so'm.</span>
                            @endif
                            {{$tariff->price -  $tariff->discount / 100 * $tariff->price }} so'm.
                            </h5>
                            <a href="/tariffs/{{$tariff->id}}" class="item_btn">Sotib olish</a>
                        </div>
                    @else
                        <div class="forecasts_by_period__items_i">
                            <h6 class="item_period">{{$tariff->term}} kunlik Vip </h6>
                            <h5 class="item_price">

                            {{$tariff->price}} so'm.
                            </h5>
                            <a href="/tariffs/{{$tariff->id}}" class="item_btn">Sotib olish</a>
                        </div>
                    @endif
                @endif
            @endforeach

          {{-- <div class="forecasts_by_period__items_i">
            <span class="item_discount_percent">
              <mark>-30%</mark>
            </span>
            <h6 class="item_period">3 Oylik Vip</h6>
            <h5 class="item_price">
              <span class="actual_price">2.4 mln so'm.</span>
              1 mln so'm.
            </h5>
            <a href="#buy" class="item_btn">Sotib olish</a>
          </div>
          <div class="forecasts_by_period__items_i">
            <span class="item_discount_percent">
              <mark>-30%</mark>
            </span>
            <h6 class="item_period">Yillik Vip</h6>
            <h5 class="item_price">
              <span class="actual_price">10 mln so'm.</span>
              3 mln so'm.
            </h5>
            <a href="#buy" class="item_btn">Sotib olish</a>
          </div> --}}
        </div>

      </div>




  </div>

<br>
<br>
<br>

      <!-- content -->
@endsection

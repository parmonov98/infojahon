@extends('layouts.app')

@section('content')

<div class="timer_forecasts">
  <div class="new_forecasts_table" id="timerForecasts">
    <a class="font-weight-bold bg-primary text-white">
        <div class="date_td">Vaqti</div>
        <div>Stavka turi</div>
        <div>Chempionat</div>
        <div>Jamoalar</div>
        <div>Kupon kod</div>
        <div class="rate">KF</div>
        {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
        <div>Taymer</div>
    </a>
    @forelse($forecasts->free as $item)
    <a href="{{route('forecasts.show', $item->link)}}">
            <div class="date_td">{{date("m.d, H:i", strtotime($item->begin))}}</div>
            <div>{{$item->sport_type}}</div>
            <div>{{$item->chempionat}}</div>
            <div>{{$item->first_team}} - {{$item->second_team}}</div>
            <div>{{$item->forecast_value}}</div>
            <div class="rate">{{$item->kf}}</div>
            {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
            <div class="timer " data-datetime="{{date("M d, Y H:i:s", strtotime($item->begin))}}">
                  <span class="days"></span>
                  <span class="time">13:04:49</span>
              </div>
    </a>
    @empty
    Prognozlar hali chiqarilmagan!
    @endforelse


  </div>
  <p class="hint"><a href="/history/">Barcha prognozlar</a></p>
</div>


<div class="guides__section mt-4">
  <h3 class="guides__title text-center">
    👇 <mark>Yangilar</mark> uchun 👇
  </h3>

  <div class="guides__items">
    @foreach ($theories->beginners as $key => $theory)
    <a href="/theories/{{$theory->link}}" class="guides__items_i text-white bg-primary  text-center py-1">
      <span class="float-left text-dark px-2 bg-warning">{{$key + 1}}</span> {{$theory->title}}
    </a>
    @endforeach
  </div>
</div>

<!-- content -->
@if ($theories->free->count() > 0)
<div class="slider__section mt-4">

  <div class="main-slider">


    @foreach ($theories->free as $item)
    <div class="slider__item py-3" style="background-image: url( '/storage/{{$item->image}}')">
      {{-- <img class="lazy" data-src="/storage/{{$item->image}}" src="/storage/{{$item->image}}" alt="Slider image" class="slider__item_image"> --}}
      <div class="inner_wrapper">
        <h3 class="slider__item_title">

          <?php  $words = explode(" ", $item->title); ?>
          <mark class="match_score">«<?=$words[0]?>»</mark>
          {{ str_ireplace($words[0], ' ', $item->title) }}

        </h3>
        <p class="slider__item_text">

          {!! (Str::words(strip_tags($item->content), 10)) !!}

        </p>
        <a class="slider__item_morebtn" href="/theories/{{$item->link}}">
          Batafsil o'qish
        </a>
      </div>
    </div>
    @endforeach

  </div>
  <!-- or ul.my-slider > li -->

  <div id="custom-main-slider-controls">
    <li class="prev" data-controls="prev" aria-controls="customize" tabindex="-1">
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-left text-secondary " fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
      </svg>
    </li>
    <li class="next" data-controls="next" aria-controls="customize" tabindex="-1">
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-right text-secondary" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
      </svg>
    </li>
  </div>
</div>
@endif

<!-- free-forecast -->


<div class="free_forecasts__section mt-4">
    <div class="section_header">
      <h3 class="free_forecasts__title">
        <mark>Bepul</mark> Prognozlar
      </h3>
    </div>

    <div class="free_forecasts__items  d-flex flex-wrap">
    @forelse($forecasts->free as $item)
        <div class="col-6 free_forecasts__items_i flex-column border border-primary">
            <div class="item_image">
                <img class="lazy" data-src="/storage/{{$item->image}}" src="/storage/{{$item->image}}" alt="Bepul stavkalar">
            </div>

            <div class="item_content" style="">
                <h3 class="item_title" style="">{{$item->title}}
                </h3>
                <p class="my-0" style="">
                    <span class="blue_text mr-0">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                            <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                        </svg>
                    </span>
                    <strong class="ml-1 mr-4">{{date('d.m.y', strtotime($item->begin)) }}</strong>
                    <span class="blue_text">KF</span>
                    {{$item->kf}}
                    Boshlanish vaqti: <strong class="ml-1">{{date('H:i', strtotime($item->begin))}}</strong>
                </p>
                <p>
                    <span class="blue_text">Prognoz</span>
                    {{$item->forecast_value}}
                </p>
                <div class="mt-0">
                    <a href="/forecasts/{{$item->link}}" class="item_btn" style="padding:12px 5px;font-size: 10px; margin-top: 10px;">Prognozni ochish</a>
                </div>
            </div>
        </div>
    @empty
    Prognozlar hali chiqarilmagan!
    @endforelse

    </div>


</div>


@if($widgets->content->count() > 2 && $widgets->content->first())
<div class="widget dynamic_widget border border-warning" style="margin-top: 80px;">
  <div class="widget_content d-flex w-100" style="position: relative; ">
    {!! $widgets->content->first()->content !!}
  </div>
  <a href="{{ $widgets->content->first()->url }}" target="_blank" class="btn btn-primary widget_button" style="position: absolute; ">{{ $widgets->content->first()->button }}</a>
</div>
@elseif($widgets->content->count() != 0)
<div class="widget dynamic_widget border border-warning" style="margin-top: 80px;">
  <div class="widget_content d-flex w-100" style="position: relative; ">
    {!! $widgets->content->first()->content !!}
  </div>
  <a href="{{ $widgets->content->first()->url }}" target="_blank" class="btn btn-primary widget_button" style="position: absolute; ">{{ $widgets->content->first()->button }}</a>
</div>
@endif

<!-- paid-forecast -->
<div class="free_forecasts__section mt-5">
    <div class="section_header">
      <h3 class="free_forecasts__title">
        <mark>Pullik</mark> Stavkalar
      </h3>
    </div>
    @if (count($forecasts->paid) > 0)


    <div class="free_forecasts__items  d-flex flex-wrap">
        @foreach ($forecasts->paid as $item)

            <div class="col-6 free_forecasts__items_i flex-column border border-primary">
                <div class="item_image">
                    <img class="lazy" data-src="/storage/{{$item->image}}" src="/storage/{{$item->image}}" alt="Pullik stavkalar">
                </div>

                <div class="item_content" style="font-size:10px;">
                    <h3 class="item_title" style="font-size: 13px;">{{$item->title}}
                    </h3>
                    <p class="my-0" style="">
                        <span class="blue_text mr-0">
                            <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                            </svg>
                        </span>
                        <strong class="ml-1 mr-4">{{date('d.m.y', strtotime($item->begin)) }}</strong>
                        <span class="blue_text">KF</span>
                        {{$item->kf}}
                        Boshlanish vaqti: <strong class="ml-1">{{date('H:i', strtotime($item->begin))}}</strong>
                    </p>
                    <p>
                        <span class="blue_text">Prognoz</span>
                        {{$item->forecast_value}}
                    </p>
                    <div class="mt-0">
                        <a href="/forecasts/{{$item->link}}" class="item_btn" style="padding:12px 5px;font-size: 10px; margin-top: 10px;">Prognozni ochish</a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>

    @else
        Prognoz qo'shilmagan yoki e'lon qilinmagan.
    @endif

</div>


@if ($widgets->content->count() > 2 && $widgets->content->last())
<div class="widget dynamic_widget border border-warning" style="margin-top: 80px;">
  <div class="widget_content d-flex w-100" style="position: relative; ">
    {!! $widgets->content->last()->content !!}
  </div>
  <a href="{{ $widgets->content->last()->url }}" target="_blank" class="btn btn-primary widget_button" style="position: absolute; ">{{ $widgets->content->last()->button }}</a>
</div>
@endif

<!-- free-theories -->
<div class="free-theories__section mt-5">
  <h3 class="free-forecast__title">
    <mark>Bepul</mark> Nazariya
  </h3>

  <div class="row free-theories__items">
    @if (count($theories->free) > 0)
    <div class="column1">
      @foreach ($theories->free as $key => $item)

      <div class="free-theories__items_i border border-primary">
        <span class="item_rating">
          <mark>Reyting: {{ number_format($item->rating, 1, '.', ',')}}</mark>
        </span>
        <div class="col-md-6 item_image px-0">
          <img class="w-100 lazy" data-src="/storage/{{$item->image}}" src="/storage/{{$item->image}}" alt="{{$item->title}}">
        </div>
        <div class="item_content">
          <h4 class="item_title text-center" style="border-radius: 5px; background-color: #3490dc30;">{{$item->title}} </h4>
          <p class="item_description">
			 {!! (Str::words(strip_tags($item->content), 30)) !!}
          </p>
          <div class="item_footer">
            <a href="/theories/{{$item->link}}" class="gototheory">Batafsilroq</a>
            <span class="item_views">
              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z" />
                <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z" />
              </svg>
              {{$item->count_views}}
            </span>
          </div>
        </div>
      </div>
      @endforeach


    </div>

    @endif
  </div>
</div>

<div class="ad_banner__section">

  <div class="banner_content">
    <img src="/imgs/person.png" alt="Telegram" class="person">

    <h4 class="ad_banner__title">
      Telegram kanalimizda <mark>Bepul</mark> Stavkalar
    </h4>
    <a href="https://t.me/infojahon_uz" target="_blank" class="subscribe">

      <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
        <path style="fill:#2446cc;" d="M135.876,280.962L10.105,225.93c-14.174-6.197-13.215-26.621,1.481-31.456L489.845,36.811
    c12.512-4.121,24.705,7.049,21.691,19.881l-95.571,406.351c-2.854,12.14-17.442,17.091-27.09,9.19l-112.3-91.887L135.876,280.962z" />
        <path style="fill:#259ccf;" d="M396.465,124.56L135.876,280.962l31.885,147.899c2.86,13.269,18.5,19.117,29.364,10.981
    l79.451-59.497l-65.372-53.#259ccf.495-191.693C410.372,129.532,403.314,120.449,396.465,124.56z" />
        <path style="fill:#259ccf;" d="M178.275,441.894c5.858,2.648,13.037,2.302,18.85-2.052l79.451-59.497l-32.686-26.749l-32.686-26.749
    L178.275,441.894z" />
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
        <g>
        </g>
      </svg>
      Obuna bo'lish
      <span class="hint">
        Tugmachani bosib hoziroq a'zo bo'ling.Biz bilan online pul ishlang.
      </span>
    </a>
    <img class="telegram_channel"  src="/imgs/papaznaet-logo-in-banner.png" >
    <img class="telegram_md_icon"  src="/imgs/telegram-lg-icon.png" alt="Telegram" >
  </div>

</div>

<br/>
<br/>
<br/>


<!-- paid-forecast -->
<div class="free_forecasts__section mt-5">
    <div class="section_header">
      <h3 class="free_forecasts__title">
        <mark>Pullik</mark> strategiyalar
      </h3>
    </div>
    @if (count($theories->paid) > 0)


    <div class="free_forecasts__items  d-flex flex-wrap">
        @foreach ($theories->paid as $item)

            <div class="col-6 free_forecasts__items_i flex-column border border-primary">
                <div class="item_image">
                    <img class="lazy" data-src="/storage/{{$item->image}}" src="/storage/{{$item->image}}" alt="Pullik strategiyalar">
                </div>

                <div class="item_content" style="">
                    <h3 class="item_title mt-2" style="">{{$item->title}}
                    </h3>
                    <p class="my-0 mt-1" style="">
                        <span class="blue_text mr-0">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/>
                                <path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                            </svg>
                        </span>
                        <strong class="ml-1 mr-4">{{date('d.m.y', strtotime($item->created_at)) }}</strong>
                        <span class="blue_text"> {{ number_format($theory->rating, 1, '.', ',')}} </span>
                        {{$item->kf}}
                    </p>
                    <p>
                        {!! (Str::words(strip_tags($item->content), 7)) !!}
                    </p>
                    <div class="mt-0">
                        <a href="/theories/{{$item->link}}" class="item_btn" style="padding:12px 5px;font-size: 10px; margin-top: 10px;">Strategiyani olish</a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>

    @else
        Prognoz qo'shilmagan yoki e'lon qilinmagan.
    @endif

</div>

<!-- content -->
@endsection

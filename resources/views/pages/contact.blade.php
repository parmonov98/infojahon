@extends('layouts.app')

@section('content')

<div class="mt-2">


    <h2 class="contact_title">
        Barcha savollar bo'yicha <mark>adminga yozish</mark>
    </h2>
    <form class="contact__form" method="post" action="/contact">

    @csrf


        <div class="inline-group">
            <input type="text" name="name" id="name" value="{{ old('name') }}" placeholder="Ismingiz">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <input type="email" name="email" value="{{ old('email') }}" id="email" placeholder="E-mail">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="inline-group w-100">
            <input type="phone" class="w-100" name="phone" id="phone" value="{{ old('phone') }}" placeholder="998991112233">
            @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="inline-group">
            <textarea name="message" id="message" rows="6" placeholder="Xabaringizni kiriting...">{{ old('message') }}</textarea>
            @error('message')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <button class="message__submit__btn" type="submit">
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-8.354 2.646a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L9.793 7.5H5a.5.5 0 0 0 0 1h4.793l-2.147 2.146z"/>
                </svg>
            </button>
        </div>
    </form>
</div>



@endsection

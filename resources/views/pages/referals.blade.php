@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Referallaringiz:</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="overflow-x: auto;">
                                <table class="table">
                                    <thead class="bg-warning">
                                      <tr>
                                        <th scope="col">№</th>
                                        <th scope="col">Niki</th>
                                        <th scope="col">Hisobidagi mablag'</th>
                                        <th scope="col">Tushgan foyda</th>
                                        <th scope="col">Qo'shilgan vaqti</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($referals as $key => $referal)
                                        <tr>
                                            <th scope="row">
                                                {{$key + 1}}
                                            </th>
                                            <td>{{$referal->username}}</td>
                                            <td>{{ isset($referal->user->account) ? $referal->user->account->balance : 0}} so'm</td>
                                            <td>{{$referal->profit}} so'm</td>
                                            <td>
                                                {{date('d.m.Y', strtotime($referal->created_at)) }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                  </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

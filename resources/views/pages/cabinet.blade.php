@extends('layouts.app')

@section('content')

    <div class="container profile_page mt-4">
        <div class="row">

            @include('../includes/cabinet-profile')
            @include('../includes/cabinet-menu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Sizning profilingiz ma'lumotlari</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="/cabinet">

                                @csrf
                                <div class="form-group row">
                                    <label for="username" class="col-lg-4 col-sm-12 col-form-label " >Nikingiz</label>
                                    <div class="col-lg-8 col-sm-12">
                                        <input id="username" value="{{$user->username}}" name="username" disabled placeholder="Username" class="form-control here" required="required" type="text">
                                    </div>
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-lg-4 col-sm-12  col-form-label">Email</label>
                                    <div class="col-lg-8 col-sm-12">
                                        <input class="form-control" id="email" name="email" value="{{$user->email}}" disabled placeholder="Email" class="form-control here" required="required" type="text">
                                    </div>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group row">
                                    <label for="first_name" class="col-lg-4 col-sm-12 col-form-label">Ismingiz:</label>
                                    <div class="col-lg-8 col-sm-12">
                                        <input id="first_name" value="{{$user->profile->first_name}}" name="first_name" placeholder="Kiriting..." class="form-control here" type="text">
                                    </div>
                                    @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="last_name" class="col-lg-4 col-sm-12 col-form-label">Familiyangiz:</label>
                                    <div class="col-lg-8 col-sm-12">
                                        <input id="last_name" value="{{$user->profile->last_name}}" name="last_name" placeholder="Kiriting..." class="form-control here" type="text">
                                    </div>

                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="url" class="col-lg-4 col-sm-12 col-form-label">Vebsayt</label>
                                    <div class="col-lg-8 col-sm-12">
                                        <input id="url" value="{{$user->profile->url}}" name="url" placeholder="url" class="form-control here" type="url">
                                    </div>

                                    @error('url')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="website" class="col-lg-4 col-sm-12 col-form-label">Rasmingiz</label>
                                    <div class="col-lg-8 col-sm-12">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                                   id="image" name="image"
                                                   value="{{ old('image') }}"
                                                   autocomplete="image">
                                            <label class="custom-file-label" for="image">Choose file</label>
                                            @error('image')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="url" class="col-lg-4 col-sm-12 col-form-label">YooMoney (Яндекс.Деньги) </label>
                                    <div class="col-lg-8 col-sm-12">
                                        <input id="yandex_money" value="{{$user->account->yandex_money}}" name="yandex_money" placeholder="koshelok raqamingiz" class="form-control here" type="text">
                                    </div>

                                    @error('url')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="url" class="col-lg-4 col-sm-12 col-form-label">Qiwi </label>
                                    <div class="col-lg-8 col-sm-12">
                                        <input id="qiwi" value="{{$user->account->qiwi}}" name="qiwi" placeholder="koshelok raqamingiz" class="form-control here" type="text">
                                    </div>

                                    @error('url')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-8 col-sm-12 offset-lg-4">
                                    <button name="submit" type="submit" class="btn btn-block btn-primary">Profil ni yangilash</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

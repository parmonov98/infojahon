
@extends('layouts.app')

@section('page_styles')
<link rel="stylesheet" href="{{asset('css/animate.css')}}">
@endsection

@section('content')



<div class="timer_forecasts mt-2">
    <div class="new_forecasts_table" id="timerForecasts">
      <a class="font-weight-bold bg-primary text-white">
          <div class="date_td">Vaqti</div>
          <div>Stavka turi</div>
          <div>Chempionat</div>
          <div>Jamoalar</div>
          <div>Kupon kod</div>
          <div class="rate">KF</div>
          {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
          <div>Taymer</div>
      </a>
      @forelse($timerForecasts as $item)
      <a href="{{route('forecasts.show', $item->link)}}">
              <div class="date_td">{{date("m.d, H:i", strtotime($item->begin))}}</div>
              <div>{{$item->sport_type}}</div>
              <div>{{$item->chempionat}}</div>
              <div>{{$item->first_team}} - {{$item->second_team}}</div>
              <div>{{$item->forecast_value}}</div>
              <div class="rate">{{$item->kf}}</div>
              {{-- <div class="datetime">{{date("M d, Y H:i:s", strtotime($item->begin))}}</div> --}}
              <div class="timer " data-datetime="{{date("M d, Y H:i:s", strtotime($item->begin))}}">
                    <span class="days"></span>
                    <span class="time">13:04:49</span>
                </div>
      </a>
      @empty
      Prognozlar hali chiqarilmagan!
      @endforelse


    </div>
    <p class="hint"><a href="/history/">Barcha prognozlar</a></p>
</div>



<div class="stat__section mt-2">
    <div class="section_header">
        <h3 class="stat__title">
        <mark>Sport prognozlarini</mark>

        </h3>
        <p class="stat__title_description">
        Haqiqiy statistikasi (Foiz hisobida)
        </p>
    </div>
    <div class="stat__items">
        <div class="col-md-4 stat__items_i stavka_dnya">

        <div class="single-chart">
            <svg viewBox="0 0 36 36" class="circular-chart yellow">
            <path class="circle-bg" d="M18 2.0845
                a 15.9155 15.9155 0 0 1 0 31.831
                a 15.9155 15.9155 0 0 1 0 -31.831"></path>
            <path class="circle" stroke-dasharray="{{ $statDetails['today_bets_win_percentage']}}, 100" d="M18 2.0845
                a 15.9155 15.9155 0 0 1 0 31.831
                a 15.9155 15.9155 0 0 1 0 -31.831"></path>
            <text x="18" y="20.35" class="percentage progress_text">{{ $statDetails['today_bets_win_percentage']}}%</text>
            </svg>
        </div>
        <h4 class="item_title"><mark>Kunlik</mark></h4>
        <span class="type_forecast">

        </span>
        <span class="item_description">
            Prognozlarni o'rtacha o'tish foizi
        </span>

        </div>
        <div class="col-md-4 stat__items_i jelezka">

            <div class="single-chart">
                <svg viewBox="0 0 36 36" class="circular-chart yellow">
                <path class="circle-bg" d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                <path class="circle" stroke-dasharray="{{floor($statDetails['all_bets_win_percentage'])}}, 100" d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                <text x="18" y="20.35" class="percentage progress_text">{{floor($statDetails['all_bets_win_percentage'])}}%</text>
                </svg>
            </div>
            <h4 class="item_title"><mark>Barcha </mark></h4>
            <span class="type_forecast">

            </span>
            <span class="item_description">
                Prognozlar  o'rtacha o'tish foizi
            </span>

        </div>
        <div class="col-md-4 stat__items_i forecasts__numbers">
        <h5 class="widget_title"><mark>To'liq</mark>
            <br>
            Prognozlar statistikasi
        </h5>

        <div class="bet__items">
            <div class="bet__item">
            G'alabalar <i>(Yutuqli stavka)</i> <mark class="yellow_corner">{{$statDetails['wins']}}</mark>
            </div>
            <div class="bet__item">
            Yutqazildi<mark class="yellow_corner">{{$statDetails['losses']}}</mark>
            </div>
            <div class="bet__item">
            Rasxod <i>(Qaytuv)</i> <mark class="yellow_corner">{{$statDetails['ties']}}</mark>
            </div>

        </div>

        </div>

    </div>
</div>

<div class="row stats_section" data-threshold="50">
<div class="w-100 stats__header pl-3">
    <h4 class="stats__header_title font-weight-bold m-0">Stavkalar natijalari</h4>
    <div id="stats_data-controls">
        <li class="prev" data-controls="prev" aria-controls="customize" tabindex="-1">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-left text-secondary " fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path>
        </svg>
        </li>
        <li class="next" data-controls="next" aria-controls="customize" tabindex="-1">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-right text-secondary" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path>
        </svg>
        </li>
    </div>
    </div>

    <div class="stats_data__section" id="stats_data" data-animations="scrollByY s2" >
        <div class="stats_data__table" >
            <div class="data_row">
                <div class="data_item">Sana</div>
                <div class="data_item">Stavka turi</div>
                <div class="data_item">Natija</div>
                <div class="data_item">Toza foyda/zarar</div>
                <div class="data_item">KF</div>
                <div class="data_item">Rasmni ko'rish</div>
            </div>
            @if ($bets->count() > 0)

            @foreach ($bets as $bet)
                @continue($bet->result == null)
                {{-- @if ($bet->is_succeeded === 'yes') --}}


                    @if ($bet->result)
                    @switch($bet->result->result)
                        @case('win')

                            <div class="data_row success">
                                <div class="data_item">{{date('d.m.Y', strtotime($bet->end))}}</div>
                                <div class="data_item">
                                    {{$bet->bet_type}}
                                </div>
                                <div class="data_item result">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"></path>
                                    </svg>
                                    Yutdik
                                </div>
                                <div class="data_item">
                                    +{{ $bet->bet_sum * $bet->kf - $bet->bet_sum}} so'm.
                                </div>
                                <div class="data_item link">
                                    {{$bet->kf}}
                                </div>
                                <div class="data_item link">
                                    <a onclick="zoomIn(this)" style="cursor: pointer;" data-image="/storage/{{$bet->result->screenshot}}">
                                        Skrinni ko'rish
                                    </a>
                                </div>


                            </div>
                            @break
                        @case('lose')

                            <div class="data_row fail" >
                                <div class="data_item">{{date('d.m.Y', strtotime($bet->end))}}</div>
                                <div class="data_item">{{$bet->bet_type}}</div>
                                <div class="data_item result">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"></path>
                                        <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"></path>
                                    </svg>
                                    Yutqazdik
                                </div>
                                <div class="data_item">
                                    -{{$bet->bet_sum }} so'm.
                                </div>
                                <div class="data_item link">
                                    {{$bet->kf}}
                                </div>
                                <div class="data_item link">
                                    <a onclick="zoomIn(this)" style="cursor: pointer;" data-image="/storage/{{$bet->result->screenshot}}">
                                    Skrinni ko'rish
                                    </a>
                                </div>

                            </div>
                            @break
                        @default
                            <div class="data_row fail " >
                                <div class="data_item">{{date('d.m.Y', strtotime($bet->end))}}</div>
                                <div class="data_item">{{$bet->bet_type}}</div>
                                <div class="data_item result">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"></path>
                                        <path fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"></path>
                                    </svg>
                                    Pul qaytdi(nicheya)
                                </div>
                                <div class="data_item">
                                    0 so'm.
                                </div>
                                <div class="data_item link">
                                    {{$bet->kf}}
                                </div>
                                <div class="data_item link">
                                    <a onclick="zoomIn(this)" style="cursor: pointer;" data-image="/storage/{{$bet->result->screenshot}}">
                                    Skrinni ko'rish
                                    </a>
                                </div>

                            </div>
                    @endswitch
                    @endif

                {{-- @else --}}
                {{-- @endif --}}
            @endforeach
            @else
            Hali natijalar chiqmagan
            @endif

        </div>
    </div>

    @if($widgets->content->count() != 0)
    <div class="widget w-100 dynamic_widget border border-warning" style="margin-top: 40px;" >
        <div class="widget_content d-flex w-100" style="position: relative; " >
            {!! $widgets->content->first()->content !!}
        </div>
        <a href="{{ $widgets->content->first()->url }}" target="_blank" class="btn btn-primary widget_button" style="position: absolute; ">{{ $widgets->content->first()->button }}</a>
    </div>
    @endif

</div>



@endsection

@section('page_scripts')
<script src="js/animate.js"></script>
<div class="zoom_modal" id="zoomModal">

    <svg style="color: #ff0000; cursor: pointer;" class="close_zoom_modal" width="3.5em" height="3.5em" viewBox="0 0 16 16" class="bi bi-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path  style="pointer-events: none;" fill-rule="evenodd" d="M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z"></path>
        <path style="pointer-events: none;" fill-rule="evenodd" d="M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z"></path>
    </svg>

      <img class="" src="/storage/imgs/default-avatar.jpg" alt="Default">
</div>
<script>
    const Animater = new animate(false);

    document.querySelector('#stats_data-controls > .prev').addEventListener('click', function (e) {
      const stats_data = document.getElementById('stats_data');
      var MIN_PIXELS_PER_STEP = 100;
      console.log(stats_data.scrollLeft);
      // stats_data.scrollLeft = 0;
      if (stats_data.scrollLeft > 0) {
        stats_data.scrollLeft = stats_data.scrollLeft -   MIN_PIXELS_PER_STEP;
      }
    });



    document.querySelector('#stats_data-controls > .next').addEventListener('click', function (e) {
      const stats_data = document.getElementById('stats_data');
      var MIN_PIXELS_PER_STEP = 100;
      console.log(stats_data.scrollLeft);
      // stats_data.scrollLeft = 0;
      if (stats_data.scrollLeft !== 1000) {
        stats_data.scrollLeft = stats_data.scrollLeft + MIN_PIXELS_PER_STEP;
      }
    });

    function zoomIn(img){
        const zoomModal = document.getElementById('zoomModal');

        var image = new Image();
        var fit = 'height';

        image.onload = function() {
            console.log(this.width + 'x' + this.height);
            if(this.width > this.height){
                fit = 'width';
            }
            zoomModal.querySelector('img').style.height = 'auto';
            zoomModal.querySelector('img').style.width = '100%';
            // if (fit == 'width') {
            // }
            //  else {
            //     zoomModal.querySelector('img').style.width = 'auto';
            //     zoomModal.querySelector('img').style.height = '100%';
            // }
        }
        image.src = img.dataset.image;
        // zoomModal.querySelector('img').setAttribute('onclick', 'zoomImg(this.parentElement);');

        zoomModal.querySelector('img').src = img.dataset.image;


        // zoomModal.classList.add('shown_modal')
        setTimeout(function () {
            zoomModal.classList.add('shown_modal');
        }, 100)

        console.log(img);
    }

    </script>
@endsection

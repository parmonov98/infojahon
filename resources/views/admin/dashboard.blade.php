@extends('layouts.master')


@section('content')
<div class="container-fluid">
    <div class="fade-in">

        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-gradient-primary">
                    <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{ $users->counts }}</div>
                            <div>Foydalanuvchilar</div>
                        </div>

                    </div>
                    <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                        {{-- <canvas data-months='{"Jan", "Feb", "Apr"}' data-counts='{1, 3, 5}' class="chart" id="card-chart1"  height="70"></canvas> --}}
                        <canvas  data-months='{{$months}}'
                            data-counts='<?php echo $usersDetails; ?>' class="chart p-1" id="card-chart1"
                            height="70"></canvas>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-gradient-info">
                    <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{$allDetails->theories}}</div>
                            <div>Teoriyalar</div>
                        </div>

                    </div>
                    <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                        <canvas data-months="{{$months}}" data-counts="{{$theoriesDetails}}"
                            class="chart" id="card-chart2" height="70"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-gradient-warning">
                    <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{$allDetails->forecasts}}</div>
                            <div>prognozlar</div>
                        </div>

                    </div>
                    <div class="c-chart-wrapper mt-3" style="height:70px;">
                        <canvas data-months="{{$months}}" data-counts="{{$forecastsDetails}}"
                            class="chart" id="card-chart3" height="70"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-gradient-danger">
                    <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                        <div>
                            <div class="text-value-lg">{{$allDetails->messages}}</div>
                            <div>Xabalar</div>

                        </div>

                    </div>
                    <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                        <canvas data-months='{{$months}}'
                            data-counts='{{$messagesDetails}}' class="chart" id="card-chart4"
                            height="70"></canvas>
                    </div>
                </div>
            </div>

        </div>

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <h4 class="card-title mb-0">Kirimlar va Chiqimlar</h4>
                        <div class="small text-muted">Oylar hisobida</div>
                    </div>
                    <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">
                        <div class="btn-group btn-group-toggle mx-3" data-toggle="buttons">

                            <label class="btn btn-outline-secondary active">
                                <input id="option2" type="radio" name="options" autocomplete="off" checked=""> Month
                            </label>

                        </div>
                    </div>
                </div>
                <div class="c-chart-wrapper" style="height:300px;margin-top:40px;">
                    <canvas class="chart"
                    data-months='{{$months}}'
                    data-counts="{{$incomeDetails}}"
                    data-withdrawals="{{$withdrawalsDetails}}"
                    data-incomes="{{$incomeDetails}}"
                    class="chart" id="main-chart" height="300"></canvas>
                </div>
            </div>

        </div>



    </div>
</div>
@endsection


@section('page_scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>

  var lineData = {
      labels: eval(document.getElementById('card-chart1').dataset.months)
      , datasets: [{
        label: " oyida qo'shilganlar",
        backgroundColor: 'transparent'
        , borderColor: 'rgba(255,255,255,.55)'
        , pointBackgroundColor: coreui.Utils.getStyle('--primary')
        , data: eval(document.getElementById('card-chart1').dataset.counts)
      }]
    }
  var cardChart1 = new Chart(document.getElementById('card-chart1'), {
    type: 'line'
    , data:lineData
    , options: {
      maintainAspectRatio: false
      , legend: {
        display: false
      }
      , scales: {
        xAxes: [{
          gridLines: {
            color: 'transparent'
            , zeroLineColor: 'transparent'
          }
          , ticks: {
            fontSize: 2
            , fontColor: 'transparent'
          }
        }]
        , yAxes: [{
          display: false
          , ticks: {
            display: false
            , min: 1
            ,
            max: Math.max(...lineData.datasets[0].data) + 1
          }
        }]
      }
      , elements: {
        line: {
          borderWidth: 1
        }
        , point: {
          radius: 4
          , hitRadius: 10
          , hoverRadius: 4
        }
      }
    }
  });

//   theories
  var lineData = {
      labels: eval(document.getElementById('card-chart2').dataset.months)
      , datasets: [{
        label: " oyida qo'shilganlar",
        borderColor: 'rgba(255,255,255,.55)',
        pointBackgroundColor: coreui.Utils.getStyle('--info'),
        data: eval(document.getElementById('card-chart2').dataset.counts)
      }]
    }
  var cardChart2 = new Chart(document.getElementById('card-chart2'), {
    type: 'line'
    , data:lineData
    , options: {
      maintainAspectRatio: false
      , legend: {
        display: false
      }
      , scales: {
        xAxes: [{
          gridLines: {
            color: 'transparent'
            , zeroLineColor: 'transparent'
          }
          , ticks: {
            fontSize: 2
            , fontColor: 'transparent'
          }
        }]
        , yAxes: [{
          display: false
          , ticks: {
            display: false
            , min: 0
            , max: Math.max(...lineData.datasets[0].data) + 1
          }
        }]
      }
      , elements: {
        line: {
          borderWidth: 1
        }
        , point: {
          radius: 4
          , hitRadius: 10
          , hoverRadius: 4
        }
      }
    }
  });
//   forecasts
  var lineData = {
      labels: eval(document.getElementById('card-chart3').dataset.months)
      , datasets: [{
        label: " oyida berilgan ",
        backgroundColor: 'rgba(255,255,255,.2)',
        borderColor: 'rgba(255,255,255,.55)',
        data: eval(document.getElementById('card-chart3').dataset.counts)
      }]
    }
  var cardChart3 = new Chart(document.getElementById('card-chart3'), {
    type: 'line'
    , data:lineData
    , options: {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                display: false
            }],
            yAxes: [{
                display: false,
                max: Math.max(...lineData.datasets[0].data) + 1
            }]
        },
        elements: {
            line: {
                borderWidth: 2
            },
            point: {
                radius: 0,
                hitRadius: 10,
                hoverRadius: 4
            }
        }
    }
  });
//   forecasts
  var barData = {
      labels: eval(document.getElementById('card-chart4').dataset.months)
      , datasets: [{
        label: " oyida kelgan xabarlar",
        backgroundColor: 'rgba(255,255,255,.2)',
        borderColor: 'rgba(255,255,255,.55)',
        data: eval(document.getElementById('card-chart4').dataset.counts),
      }]
    }
  var cardChart4 = new Chart(document.getElementById('card-chart4'), {
    type: 'bar'
    , data:barData,
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                display: false,

            }],
            yAxes: [{
                ticks: {
                    min: 0,
                    // forces step size to be 5 units
                    stepSize: 1 // <----- This prop sets the stepSize
                },
                max: Math.max(...lineData.datasets[0].data) + 1
            }]
        }
    }
  });


  var mainData = {
        labels: eval(document.getElementById('main-chart').dataset.months),
        datasets: [{
            label: 'Kirimlar',
            backgroundColor: coreui.Utils.hexToRgba(coreui.Utils.getStyle('--info'), 10),
            borderColor: coreui.Utils.getStyle('--success'),
            pointHoverBackgroundColor: '#fff',
            borderWidth: 2,
            data: eval(document.getElementById('main-chart').dataset.incomes)
        }, {
            label: 'Chiqimlar',
            backgroundColor: 'transparent',
            borderColor: coreui.Utils.getStyle('--danger'),
            pointHoverBackgroundColor: '#fff',
            borderWidth: 2,
            data: eval(document.getElementById('main-chart').dataset.withdrawals)
        }]
    };
    console.log(mainData.datasets[0].data);
    console.log(Math.max(...mainData.datasets[0].data));
    // console.log(mainData);
    // console.log(Math.max(document.getElementById('main-chart')));
  var mainChart = new Chart(document.getElementById('main-chart'), {
    type: 'line',
    data: mainData,
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    drawOnChartArea: false
                }
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    maxTicksLimit: 5,
                    stepSize: Math.ceil(250 / 5),
                    max: Math.max(...mainData.datasets[0].data)
                }
            }]
        },
        elements: {
            point: {
                radius: 0,
                hitRadius: 10,
                hoverRadius: 4,
                hoverBorderWidth: 3
            }
        }
    }
});
</script>
@endsection

@extends('layouts.master')


@section('content')

<div class="container-fluid">
  <div class="fade-in">

    <!-- /.row-->
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <i class="fa fa-align-justify"></i> Barcha Widgetlar
            {{-- <a href="/admin/widget/add">Qo'shish</a> --}}
          </div>
          <div class="card-body">
            <div class="card">

                <div class="card-header">Boshqa sahifa</div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Bosh sahifa</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="tariffs-tab" data-toggle="tab" href="#tariffs" role="tab" aria-controls="tariffs" aria-selected="false">Tariflar</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="theories-tab" data-toggle="tab" href="#theories" role="tab" aria-controls="theories" aria-selected="false">Nazariyalar</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="strategies-tab" data-toggle="tab" href="#strategies" role="tab" aria-controls="strategies" aria-selected="false">Strategiyalar</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="forecasts-tab" data-toggle="tab" href="#forecasts" role="tab" aria-controls="forecasts" aria-selected="false">Prognozlar</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="stats-tab" data-toggle="tab" href="#stats" role="tab" aria-controls="stats" aria-selected="false">Statistika</a>
                        </li>
                    </ul>
                    <div class="row tab-content" id="widgets">
                        <div class="col-md-12 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <h3 class="mt-3 text-primary">Kontent qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->home->content as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                        <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                        <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach



                            </div>
                            <hr>
                            <h3 class="mt-3 text-primary">Saydbar qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->home->sidebar as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}" data-widget_id="{{$item->id}}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                        <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                        <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-md-12 tab-pane fade" id="tariffs" role="tabpanel" aria-labelledby="tariffs-tab">
                            <h3 class="mt-3 text-primary">Kontent qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->tariffs->content as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                        <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                        <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach



                            </div>
                            <hr>
                            <h3 class="mt-3 text-primary">Saydbar qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->tariffs->sidebar as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}" data-widget_id="{{$item->id}}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                            <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                            <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-md-12 tab-pane fade" id="theories" role="tabpanel" aria-labelledby="theories-tab">
                            <h3 class="mt-3 text-primary">Kontent qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->theories->content as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                        <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                        <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach



                            </div>
                            <hr>
                            <h3 class="mt-3 text-primary">Saydbar qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->theories->sidebar as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}" data-widget_id="{{$item->id}}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                            <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                            <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-md-12 tab-pane fade" id="strategies" role="tabpanel" aria-labelledby="strategies-tab">
                            <h3 class="mt-3 text-primary">Kontent qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->strategies->content as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                        <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                        <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach



                            </div>
                            <hr>
                            <h3 class="mt-3 text-primary">Saydbar qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->strategies->sidebar as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}" data-widget_id="{{$item->id}}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                            <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                            <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-md-12 tab-pane fade" id="forecasts" role="tabpanel" aria-labelledby="forecasts-tab">
                            <h3 class="mt-3 text-primary">Kontent qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->forecasts->content as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                        <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                        <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach



                            </div>
                            <hr>
                            <h3 class="mt-3 text-primary">Saydbar qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->forecasts->sidebar as $item)
                                <form class="col-md-6 widget_form"  action="{{ route('updateWidget', $item->id) }}" data-widget_id="{{$item->id}}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                            <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                            <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach

                            </div>
                        </div>
                        <div class="col-md-12 tab-pane fade" id="stats" role="tabpanel" aria-labelledby="stats-tab">
                            <h3 class="mt-3 text-primary">Kontent qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->stats->content as $item)
                                <form class="col-md-6 widget_form" action="{{ route('updateWidget', $item->id) }}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                        <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                        <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach



                            </div>
                            <hr>
                            <h3 class="mt-3 text-primary">Saydbar qismidagi widjetlar</h3>
                            <div class="row mt-3">
                                @foreach ($widgets->stats->sidebar as $item)
                                <form class="col-md-6 widget_form"  action="{{ route('updateWidget', $item->id) }}" data-widget_id="{{$item->id}}">
                                    <div class="form-group">
                                    <label for="formGroupExampleInput">URL(ссылка)</label>
                                    <input type="text" name="url" value="{{$item->url}}"  class="form-control url" id="formGroupExampleInput" placeholder="https://domen.ru">
                                    </div>
                                    <div class="form-group">
                                    <label for="formGroupExampleInput2">Tugma</label>
                                    <input type="text" name="button" value="{{$item->button}}" class="form-control" id="formGroupExampleInput2" placeholder="Bonus olish">
                                    </div>

                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label for="validationTextarea">kod</label>
                                            <textarea class="form-control " name="content" rows="6" id="validationTextarea" placeholder="HTML code">{{$item->content}}</textarea>
                                            <div class="invalid-feedback">
                                            widjetni kontentini kiriting
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState">Holati</label>
                                        <select name="status" id="inputState" class="form-control">
                                            <option value="active" {{$item->status == 'active' ? 'selected' : ''}}>Yoqilgan</option>
                                            <option value="inactive" {{$item->status == 'inactive' ? 'selected' : ''}}>O'chirilgan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary save_widget" data-widget_id="{{$item->id}}" type="submit">Saqla</button>
                                    </div>
                                </form>
                                @endforeach

                            </div>
                        </div>

                    </div>
                </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row-->


    </div>
  </div>
  @endsection


  @section('page_scripts')
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script>
      console.log(1111);
      $(document).ready(function() {
        $('.widget_form').submit( processForm );
      });
    //   (function($){
    function processForm( e ){
        e.preventDefault();
        console.log(e.target);
        console.log( e.target.action);
        $.ajax({
            url: e.target.action,
            type: 'post',
            dataType: 'json',
            contentType: 'application/x-www-form-urlencoded',
            data: $(this).serialize(),
            success: function( data, textStatus, jQxhr ){
                // $('#response pre').html( data );
                console.log(data);
                alert(data.content);

            },
            error: function( jqXhr, textStatus, errorThrown ){
                console.log( errorThrown );
            }
        });

    }




    // })(jQuery);
  </script>
  @endsection

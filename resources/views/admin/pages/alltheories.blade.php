@extends('layouts.master')

@section('content')

    <div class="container-fluid allforecasts mt-2">
        <div class="row">
            {{-- @include('../includes/cabinet-menu') --}}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Barcha teoriyalar</h4>
                                <hr class="mt-2">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 table-responsive" >


                                <table class="table" style="min-width: 1200px;">
                                    <caption>

                                        {{$theories->links()}}
                                    </caption>
                                    <thead>
                                      <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nomi</th>
                                        <th scope="col">Rasmi</th>
                                        <th scope="col">Matni</th>
                                        <th scope="col">Ko'rilgan</th>
                                        <th scope="col">Turi</th>
                                        <th scope="col">Holati</th>
                                        <th scope="col">vaqti</th>
                                        <th scope="col">O'chirish</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($theories as $index => $theory)
                                      <tr class="" style="max-width: 60px;" >
                                        <th scope="row">
                                            <a class="text-primary" href="/admin/theory/edit/{{$theory->id}}">
                                                {{-- {{$theory->id}} --}}
                                                {{$index+1}}
                                            </a>
                                        </th>

                                        <td>{{$theory->title}}</td>
                                        <td  class="" style="">
                                            <img style="max-width: 120px;" src="/storage/{{$theory->image}}" alt="">
                                        </td>
                                        <td class="w-25">{{ substr($theory->content, 0, 200)}}</td>
                                        <td>{{$theory->count_views}}</td>
                                        <td>{{$theory->type}}</td>
                                        <td>{{$theory->status}}</td>
                                        <td>{{date('d M, Y H:i:s', strtotime($theory->created_at))}}</td>
                                        <td>
                                            <a class="text-danger" href="#" onclick="event.preventDefault(); this.nextElementSibling.submit();">O'chir</a>
                                            <form action="{{route('deleteTheory', $theory->id)}}" method="post">
                                                @csrf
                                            </form>
                                        </td>

                                      </tr>
                                      @endforeach

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

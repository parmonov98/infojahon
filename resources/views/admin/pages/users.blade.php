@extends('layouts.master')


@section('content')

<div class="container-fluid">
    <div class="fade-in">

        <!-- /.row-->
        <div class="row">
            <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-align-justify"></i> Barcha foydalanuvchilar</div>
                <div class="card-body table-responsive">
                <table class="table table-responsive-sm table-bordered table-striped table-sm" style="min-width: 800px;">
                    <thead>
                    <tr>
                        <th>Nik</th>
                        <th>Ism famliya</th>
                        <th>Email</th>
                        <th>O'tgan sana</th>
                        <th>Rasmi</th>
                        <th>Mablag'i</th>
                        <th>Prognozlari</th>
                        <th scope="col">Amallar</th>
                    </tr>
                    </thead>
                    <tbody class="align-middle">
                        @foreach ($users as $user)
                             <tr style="" >
                                <td class="align-middle">{{$user->username}}</td>
                                <td class="align-middle">{{$user->profile->first_name ?? ''}} {{$user->profile->last_name ?? ''}}</td>
                                <td class="align-middle">{{$user->email}}</td>
                                <td>{{date('d M, Y H:i:s', strtotime($user->created_at))}}</td>
                                <td class="align-middle text-center" style="">
                                    @if (Str::startsWith($user->profile->image ?? '' , 'http'))
                                    <a href="/storage/{{$user->profile->image ?? 'imgs/default-avatar.jpg'}}">
                                        <img class="rounded-circle w-100" style="max-width: 60px;" src="{{$user->profile->image ?? '/imgs/default-avatar.jpg'}}" alt="">                                            
                                    </a>
                                    @else
                                    <a href="/storage/{{$user->profile->image ?? 'imgs/default-avatar.jpg'}}">
                                        <img class="rounded-circle w-100" style="max-width: 60px;" src="/storage/{{$user->profile->image ?? 'imgs/default-avatar.jpg'}}" alt="">                                            
                                    </a>                                            
                                    @endif
                                </td>
                                <td>{{$user->account ? $user->account->balance: 0}}</td>
                                <td>{{$user->account ? $user->account->quantity : 0}}</td>
                                <td>
                                    <a class="text-danger deleteUser" href="#" onclick="event.preventDefault();">O'chir</a>
                                    <form action="{{route('deleteUser', $user->id)}}" method="post" >
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>



                {{$users->links()}}

                </div>
            </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->


    </div>
</div>
  @endsection



  @section('page_scripts')
  <script>

window.addEventListener('DOMContentLoaded', function (e) {
  const elements = document.querySelectorAll('.deleteUser');
  console.log(elements);
  elements.forEach(el => {
      console.log(el);
      el.addEventListener('click', function (e) {
          e.preventDefault();
          console.log(e.target.nextElementSibling);
          url = e.target.nextElementSibling.action;
          postData(url, {})
              .then(data => {
  
                  console.log(data); // JSON data parsed by `data.json()` call
                  if (data.status == 'success') {
                      el.closest('tr').remove();
                      alert(data.content);
                    } else {
                      alert(data.content);
                    //   alert('serverda xatolik yuz berdi!');
                  }
              });
      });

  });
})


  </script>
@endsection
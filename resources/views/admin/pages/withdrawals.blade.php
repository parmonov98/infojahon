@extends('layouts.master')


@section('content')

<div class="container-fluid">
    <div class="fade-in">

        <!-- /.row-->
        <div class="row">
            <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-align-justify"></i> Barcha kelgan zayavkalar</div>
                <div class="card-body table-responsive">
                <table class="table table-responsive-sm table-bordered table-striped table-sm" style="min-width: 800px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Zayavkachi</th>
                        <th>Emaili</th>
                        <th>Foydalanuvchi Nomi</th>
                        <th>Kelgan vaqti</th>
                        <th>Summa</th>
                        <th>Holati</th>
                    </tr>
                    </thead>
                    <tbody class="align-middle">
                        @foreach ($withdrawals as $w)
                             <tr style="" >
                                <td class="align-middle">{{$w->id}}</td>
                                <td class="align-middle">{{$w->user->name}}</td>
                                <td class="align-middle">{{$w->user->email}}</td>
                                <td class="align-middle">{{$w->user->username}}</td>
                                <td class="align-middle text-center" style="">
                                    {{ date("d M, Y, H:m:i", strtotime($w->created_at)) }}
                                </td>
                                <td class="align-middle">{{ number_format($w->sum / $w->rate, 2, '.', ',') }} rubl</td>
                                <td class="align-middle">
                                    <select class="form-control status" name="status" data-withdrawal_id="{{$w->id}}">
                                        {{-- created,paid,cancelled --}}
                                        <option value="created" {{$w->status == 'created' ? 'selected' : ''}}>Yaratilgan</option>
                                        <option value="paid" {{$w->status == 'paid' ? 'selected' : ''}}>To'landi</option>
                                        <option value="cancelled" {{$w->status == 'cancelled' ? 'selected' : ''}}>Bekor qilindi!</option>
                                    </select>
                                </td>

                            </tr>
                        @endforeach

                    </tbody>
                </table>



                {{$withdrawals->links()}}

                </div>
            </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->


    </div>
</div>
@endsection


@section('page_scripts')
    <script>

window.addEventListener('DOMContentLoaded', function (e) {
    const elements = document.querySelectorAll('.status');
    console.log(elements);
    elements.forEach(el => {
        console.log(el);
        el.addEventListener('change', function (e) {
            console.log(e.target.value);

            postData(`/admin/ajax/update_withdrawal/${e.target.dataset.withdrawal_id}`, { status: e.target.value })
                .then(data => {

                    console.log(data); // JSON data parsed by `data.json()` call
                    if (data.success) {
                        alert(data.success);
                    } else {
                        alert('serverda xatolik yuz berdi!');
                    }
                });
        });

    });
})


    </script>
@endsection

@extends('layouts.master')


@section('content')

<div class="container-fluid">
    <div class="fade-in">

        <!-- /.row-->
        <div class="row">
            <div class="col-lg-12">
            <div class="card">
                <div class="card-header row">
                    <div class="col-md-6">
                        <i class="fa fa-align-justify"></i> Barcha kelgan zayavkalar
                    </div>
                    <div class="col-md-6">
                        <form class="input-group" method="get">
                            <input type="text" class="form-control" name="q" value="{{ request()->get('q') }}" placeholder="To'lov ID" aria-label="Search for...">
                            <span class="input-group-btn">
                              <button class="btn btn-secondary" type="submit">Izla!</button>
                            </span>
                        </form>
                    </div>
                </div>
                <div class="card-body table-responsive">
                <table class="table table-responsive-sm table-bordered table-striped table-sm" style="min-width: 800px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>To'lovchi</th>
                        <th>Emaili</th>
                        <th>Username </th>
                        <th>Kelgan vaqti</th>
                        <th>Summa</th>
                        <th>Holati</th>
                    </tr>
                    </thead>
                    <tbody class="align-middle">
                        @foreach ($payments as $item)
                             <tr style="" >
                                <td class="align-middle" title="{{$item->id}}">{{$item->public_id}}</td>
                                <td class="align-middle">{{$item->user->name ?? ''}}</td>
                                <td class="align-middle">{{$item->user->email ?? ''}}</td>
                                <td class="align-middle">{{$item->user->username ?? ''}}</td>
                                <td class="align-middle text-center" style="">
                                    {{ date("d M, Y, H:m:i", strtotime($item->created_at)) }}
                                </td>
                                <td class="align-middle" title="">{{ $item->sum }} so'm</td>
                                <td class="align-middle">
                                    <select class="form-control status" name="status" data-payment_id="{{$item->id}}">
                                        <option value="processing" {{$item->status == 'processing' ? 'selected' : ''}}>Yaratildi</option>
                                        <option value="confirmed" {{$item->status == 'confirmed' ? 'selected' : ''}}>Kutilmoqda</option>
                                        <option value="paid" {{$item->status == 'paid' ? 'selected' : ''}}>Tasdiqlandi</option>
                                        <option value="unpaid" {{$item->status == 'unpaid' ? 'selected' : ''}}>Qabul qilinmadi!</option>
                                    </select>
                                </td>

                            </tr>
                        @endforeach

                    </tbody>
                </table>



                {{$payments->links()}}

                </div>
            </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->


    </div>
</div>
@endsection


@section('page_scripts')
    <script>

window.addEventListener('DOMContentLoaded', function (e) {
    const elements = document.querySelectorAll('.status');
    console.log(elements);
    elements.forEach(el => {
        console.log(el);
        el.addEventListener('change', function (e) {
            console.log(e.target.value);

            postData(`/admin/ajax/update_payment/${e.target.dataset.payment_id}`, { status: e.target.value })
                .then(data => {

                    console.log(data); // JSON data parsed by `data.json()` call
                    if (data.success) {
                        alert(data.success);
                    } else {
                        alert('serverda xatolik yuz berdi!');
                    }
                });
        });

    });
})


    </script>
@endsection

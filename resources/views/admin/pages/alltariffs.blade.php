@extends('layouts.master')

@section('content')

    <div class="container-fluid allforecasts mt-2">
        <div class="row">
            {{-- @include('../includes/cabinet-menu') --}}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Barcha TARIFFLAR</h4>
                                <hr class="mt-2">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 table-responsive" >


                                <table class="table table table-bordered table-striped table-sm " style="min-width: 1200px;">
                                    <caption>

                                        {{$tariffs->links()}}
                                    </caption>
                                    <thead>
                                      <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Muddati</th>
                                        <th scope="col">Prognozlar Soni</th>
                                        <th scope="col">Turi</th>
                                        <th scope="col">NArxi</th>
                                        <th scope="col">Skidkasi</th>
                                        <th scope="col">Yaratilgan Vaqti</th>
                                        <th scope="col">Amallar</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($tariffs as $i => $tariff)
                                      <tr class="" style="max-width: 60px;" >
                                        <th scope="row">
                                            <a href="/admin/tariff/edit/{{$tariff->id}}">{{$i + 1}}</a>
                                        </th>
                                        <td>{{$tariff->term}}</td>
                                        <td>{{$tariff->quantity}}</td>
                                        <td>{{$tariff->type == 'piece' ? ' dona ' : 'kunlik' }}</td>
                                        <td>{{$tariff->price}} so'm</td>
                                        <td>{{$tariff->discount == null ? 0 : $tariff->discount}} %</td>
                                        <td>{{date('d M, Y H:i:s', strtotime($tariff->created_at))}}</td>
                                        <td>
                                            <a class="text-danger" href="/admin/tariff/delete/{{$tariff->id}}">O'chir</a>
                                        </td>

                                      </tr>
                                      @endforeach

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.master')

@section('content')

    <div class="container profile_page mt-2">
        <div class="row">
            {{-- @include('../includes/cabinet-menu') --}}

            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Yangi prognoz qo'shish</h4>
                                <hr class="mt-2">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">

                                <form method="POST" enctype="multipart/form-data" action="/admin/forecast/add">
                                    @method('post')
                                    @csrf
                                    <div class="form-row">
                                      <div class="form-group col-md-6">
                                        <label for="inputEmail4">Sport turi</label>
                                        <input type="text" name="sport_type" value="{{ old('sport_type') }}" class="form-control @error('sport_type') is-invalid @enderror" id="inputEmail4" placeholder="futbol">
                                        @error('sport_type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>
                                      <div class="form-group col-md-6">
                                        <label for="inputPassword4">Chempionat</label>
                                        <input type="text" name="chempionat" value="{{ old('chempionat') }}" class="form-control @error('chempionat') is-invalid @enderror" id="inputPassword4" placeholder="Yevropa chempinoati">
                                        @error('chempionat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>
                                    </div>


                                    <div class="form-group">
                                      <label for="inputAddress">Prognoz nomi</label>
                                      <input type="text" name="title" value="{{ old('title') }}" class="form-control @error('title') is-invalid @enderror" id="inputAddress" placeholder="Bavariya va FreiBurger komandalari o'yiniga prognoz">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputAddress">Prognoz ta'rif</label>
                                            <input type="text" value="{{ old('forecast_hint') }}" name="forecast_hint" class="form-control  @error('forecast_hint') is-invalid @enderror" id="inputAddress" placeholder="Bavariya yutadi 2:0 | platforma turi (1xbet, melbet)">
                                            @error('forecast_hint')
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                              </span>
                                            @enderror
                                          </div>
                                          {{-- <div class="form-group col-md-6 invisible"> --}}
                                            {{-- <label for="inputAddress">Nimaga tikamiz?</label>
                                            <select id="inputState" name="bet_result_type" class="form-control  @error('bet_result_type') is-invalid @enderror">
                                              <option selected value="Galaba" title="Odamlar e'lon qil ni bosgandan srazi ko'rishi mumkin.">Galaba</option>
                                              <option value="Maglubiyat" title="birinchi jamoga yutadi yoki umumiy hisobda berilgan prognoz yutuqsiz">Mag'lubiyat</option>
                                              <option value="Boshqa" title="birinchi jamoga yutadi yoki umumiy hisobda berilgan prognoz yutuqsiz">Boshqa</option>
                                            </select> --}}
                                            {{-- @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror --}}
                                            {{-- </div> --}}
                                            @error('forecast_hint')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                    </div>




                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputAddress">Stavka summasi</label>
                                            <input type="numeric" name="bet_sum" value="{{ old('bet_sum') }}" class="form-control @error('bet_sum') is-invalid @enderror" id="inputAddress" placeholder="1000">
                                            @error('bet_sum')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group d-flex align-items-end justify-content-center">
                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="btn btn-secondary active">
                                                <input type="radio" name="bet_type" id="ordinar_bet" value="ordinar" autocomplete="off" checked> ordinar
                                                </label>
                                                <label class="btn btn-secondary">
                                                <input type="radio" name="bet_type" id="express_bet" value="expres" autocomplete="off"> expres
                                                </label>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-row" id="teams">
                                        <div class="form-group col-md-6">
                                            <label for="inputPassword4">Birinchi jamoa</label>
                                            <input type="text" name="first_team" value="{{ old('first_team') }}" class="form-control @error('first_team') is-invalid @enderror" id="inputPassword4" placeholder="Bavariya">
                                            @error('first_team')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                          </div>
                                        <div class="form-group col-md-6">
                                          <label for="inputEmail4">Ikkinchi jamoa</label>
                                          <input type="text" name="second_team"  value="{{ old('second_team') }}" class="form-control  @error('second_team') is-invalid @enderror" id="inputEmail4" placeholder="FreiBurger">
                                          @error('second_team')
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                              </span>
                                          @enderror
                                        </div>
                                    </div>


                                    <script>

                                        document.getElementById('ordinar_bet').addEventListener('change', function (e) {
                                            console.log(e.target.value);
                                            if (e.target.value === 'ordinar') {
                                                document.getElementById('teams').classList.remove('hidden');
                                            }
                                        });
                                        document.getElementById('express_bet').addEventListener('change', function (e) {
                                            console.log(e.target.value);
                                            if (e.target.value === 'expres') {
                                                document.getElementById('teams').classList.add('hidden');
                                            }
                                        });

                                    </script>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputAddress">Stavka qo'yish silkasi</label>
                                            <input type="url" name="partner_url"  value="{{ old('partner_url') }}" class="form-control @error('partner_url') is-invalid @enderror" id="partner_url" placeholder="https://">
                                            @error('partner_url')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="inputAddress">Prognoz </label>
                                            <input type="text" value="{{ old('forecast_value') }}" name="forecast_value"
                                            class="form-control @error('forecast_value') is-invalid @enderror" id="inputAddress" placeholder="ТМ(2.5) | kupon">
                                            @error('forecast_value')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>


                                        <div class="form-group col-md-6">
                                            <label for="kf">Prognoz KF</label>
                                            <input type="text" value="{{ old('kf') }}" placeholder="1.7" name="kf"
                                            class="form-control @error('kf') is-invalid @enderror" id="kf" pattern="^\d*(\.\d{0,1})?$">
                                            @error('kf')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="image" class="">Prognoz rasm</label>

                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                                        id="image" name="image"
                                                        value="{{ old('image') }}"
                                                        autocomplete="image">
                                                <label class="custom-file-label" for="image">Rasm tanlang</label>
                                                @error('image')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-row">
                                      <div class="form-group col-md-6">
                                        <label for="begin">O'yin boshlanish vaqti</label>
                                        <input type="datetime-local" value="{{ old('begin') }}" placeholder="1.7" name="begin" class="form-control @error('begin') is-invalid @enderror" id="begin" pattern="^\d*(\.\d{0,1})?$">
                                        @error('begin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>

                                      <div class="form-group col-md-6">
                                        <label for="end">Prognoz tugash vaqti</label>
                                        <input type="datetime-local" value="{{ old('end') }}" placeholder="1.7" name="end" class="form-control @error('end') is-invalid @enderror" id="end" pattern="^\d*(\.\d{0,1})?$">
                                        @error('end')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>

                                    </div>

                                    <div class="form-row">
                                      <div class="form-group col-md-12">
                                        <label for="explanation">Prognoz tahlil matni</label>
                                        <textarea class="form-control @error('explanation') is-invalid @enderror" name="explanation" id="explanation">{{old('explanation')}}</textarea>
                                        @error('explanation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>


                                    </div>


                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="forecast_status">Chiqaramiz birdan?</label>
                                            <select id="forecast_status" name="status" class="form-control">
                                              <option  value="published" title="Odamlar e'lon qil ni bosgandan srazi ko'rishi mumkin.">ha</option>
                                              <option  selected value="created" title="Odamlar e'lon qil ni bosgandan ko'ra olmaydi o'zi faollashtirishiz kerak.">yo'q</option>
                                            </select>
                                            @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="inputState">Bepul yoki pullik?</label>
                                            <select id="inputState" name="type" class="form-control">
                                              <option selected value="paid">Pullik</option>
                                              <option value="free">Bepul</option>
                                            </select>
                                            @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>


                                    <div class="form-row " id="publishingTime">

                                        <div class="form-group col-md-12">
                                            <label for="kf">Prognoz chiqish vaqti</label>
                                            {{-- 2017-06-01T08:30 --}}
                                            <input type="datetime-local" placeholder="1.7" name="publishing_time" value="<?php echo date('Y-m-d\TH:i', time()); ?>" class="form-control" id="publishing_time">
                                            @error('publishing_time')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <script>

                                        document.getElementById('forecast_status').addEventListener('change', function (e) {
                                            console.log(e.target.value);
                                            if (e.target.value === 'published') {
                                                document.getElementById('publishingTime').classList.add('hidden');
                                            }else{
                                                document.getElementById('publishingTime').classList.remove('hidden');
                                            }
                                        });


                                    </script>
                                    <button type="submit" class="btn btn-primary">e'lon qil</button>
                                </form>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

    @section('page_scripts')

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script> --}}
    <script src="{{asset("js/ckeditor/ckeditor.js")}}"></script>
    <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>

    <script type="text/javascript">

        window.onload = function (){
            // CKFinder.start();
            // CKFinder.config( { connectorPath: '/ckfinder/connector' } );
            var editor = CKEDITOR.replace( 'explanation', {
                extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top};audio[*]{*}',
                // Adding drag and drop image upload.
                // extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
                extraPlugins: 'html5audio,justify,html5video,widget,widgetselection,clipboard,lineutils,youtube,colorbutton,removeformat,panelbutton',

                filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
                filebrowserImageUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",

                height: 480,
            } );

        };
    </script>

@endsection

@extends('layouts.master')

@section('content')

    <div class="container-fluid mt-2">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Yangi prognoz qo'shish</h4>
                                <hr class="mt-2">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">

                                <form method="POST" enctype="multipart/form-data" action="/admin/theory/update/{{$theory->id}}">
                                    @method('post')
                                    @csrf

                                    <div class="form-group">
                                      <label for="inputAddress">Teoriya nomi</label>
                                      <input type="text" name="title" value="{{$theory->title}}" class="form-control" id="inputAddress" placeholder="Yangi teoriya nomi">
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="inputAddress">Yangilar uchunmi?</label>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                          <label class="btn btn-secondary {{$theory->for_beginners == 'no' ? 'active' : ''}}">
                                            <input type="radio" value="no" name="for_beginners" id="option1" autocomplete="off" {{$theory->for_beginners == 'no' ? 'checked' : ''}}> yo'q
                                          </label>
                                          <label class="btn btn-secondary {{$theory->for_beginners == 'yes' ? 'active' : ''}}">
                                            <input type="radio" value="yes" name="for_beginners" id="option3" {{$theory->for_beginners == 'yes' ? 'checked' : ''}} autocomplete="off"> ha
                                          </label>
                                        </div>
                                      </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="image" class="">Teoriya rasm</label>

                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                                        id="image" name="image"
                                                        value="{{ old('image') }}"
                                                        autocomplete="image">
                                                <label class="custom-file-label" for="image">Rasm tanlang</label>
                                                @error('image')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-row">
                                      <div class="form-group col-md-12">
                                        <label for="content">Teoriya matni</label>
                                        <textarea class="form-control" name="content">
                                            {{$theory->content}}
                                        </textarea>
                                        @error('content')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>


                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="status">Teoriya holati</label>
                                            <select id="theoryStatus" name="status" class="form-control">
                                              <option {{$theory->status === 'published' ? 'selected' : '' }}  value="published" title="Odamlar ko'ra oladi.">published</option>
                                              <option  {{$theory->status === 'created' ? 'selected' : '' }} value="unpublished" title="Odamlar ko'ra olmaydi.">unpublished</option>
                                            </select>
                                            @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="inputState">Bepul yoki pullik?</label>
                                            <select id="inputState" name="type" class="form-control">
                                              <option {{$theory->type === 'paid' ? 'selected' : '' }} value="paid">Pullik</option>
                                              <option {{$theory->type === 'free' ? 'selected' : '' }} value="free">Bepul</option>
                                            </select>
                                            @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>

                                    <button type="submit" class="btn btn-primary">Saqla</button>
                                </form>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('page_scripts')

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script> --}}
    <script src="{{asset("js/ckeditor/ckeditor.js")}}"></script>
    <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>

    <script type="text/javascript">

        window.onload = function (){
            // CKFinder.start();
            // CKFinder.config( { connectorPath: '/ckfinder/connector' } );
            var editor = CKEDITOR.replace( 'content', {
                extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top};audio[*]{*}',
                // Adding drag and drop image upload.
                // extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
                extraPlugins: 'html5audio,justify,html5video,widget,widgetselection,clipboard,lineutils,youtube,colorbutton,removeformat,panelbutton',

                filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
                filebrowserImageUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",

                height: 480,
            } );

        };
    </script>

@endsection


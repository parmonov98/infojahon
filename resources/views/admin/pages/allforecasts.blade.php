@extends('layouts.master')

@section('content')

    <div class="container-fluid allforecasts mt-2">
        <div class="row">
            {{-- @include('../includes/cabinet-menu') --}}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body"  >
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Barcha prognozlar</h4>
                                <hr class="mt-2">
                            </div>
                        </div>
                        <div class="row mt-3" style="">
                            <div style="" class="table-responsive" >

                                <table class="table" style="min-width: 1300px;">
                                    <caption>
                                        {{$forecasts->links()}}
                                    </caption>
                                    <thead>
                                      <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Stavka tugadimi</th>
                                        <th scope="col">Sport turi</th>
                                        <th scope="col">Chempinoat</th>
                                        <th scope="col">Prognoz nomi</th>
                                        <th scope="col">Prognoz turi</th>
                                        <th scope="col">Ta'rif</th>
                                        <th scope="col">Prognoz</th>
                                        <th scope="col">Jamoa - 1</th>
                                        <th scope="col">Jamoa - 2</th>
                                        <th scope="col">KF</th>
                                        <th scope="col">$?</th>
                                        <th scope="col">Prognoz holati</th>
                                        <th scope="col">Stavka|o'yin boshi</th>
                                        <th scope="col">stavka|O'yin tugashi</th>
                                        <th scope="col">Prognoz chiqishi</th>
                                        <th scope="col">Amallar</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach ($forecasts as $index => $forecast)
                                      <tr>
                                        <th scope="row">
                                            <a class="text-primary" href="/admin/forecast/edit/{{$forecast->id}}">
                                            {{-- {{$forecast->id}} --}}
                                            {{$index+1}}
                                            </a>
                                        </th>
                                        <td>{{$forecast->is_ended}}</td>
                                        <td>{{$forecast->sport_type}}</td>
                                        <td>{{$forecast->chempionat}}</td>
                                        <td title="{{$forecast->title}}">{{substr($forecast->title, 0, 15)}}</td>
                                        <td>{{$forecast->bet_result_type}}</td>
                                        <td>{{$forecast->forecast_hint}}</td>
                                        <td>{{$forecast->forecast_value}}</td>
                                        <td>{{$forecast->first_team}}</td>
                                        <td>{{$forecast->second_team}}</td>
                                        <td>{{number_format($forecast->kf, 1)}}</td>
                                        <td>{{$forecast->type}}</td>
                                        <td>{{$forecast->status}}</td>
                                        <td>{{date('d M, Y H:i:s', strtotime($forecast->begin))}}</td>
                                        <td>{{date('d M, Y H:i:s', strtotime($forecast->end))}}</td>
                                        <td>{{date('d M, Y H:i:s', strtotime($forecast->publishing_time))}}</td>
                                        <td class="text-danger">
                                            <a href="/admin/forecast/delete/{{$forecast->id}}" class="close" aria-label="Close">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-file-earmark-x-fill" fill="red" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h5.293A1 1 0 0 1 10 .293L13.707 4a1 1 0 0 1 .293.707V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm7.5 1.5v-2l3 3h-2a1 1 0 0 1-1-1zM6.854 7.146a.5.5 0 1 0-.708.708L7.293 9l-1.147 1.146a.5.5 0 0 0 .708.708L8 9.707l1.146 1.147a.5.5 0 0 0 .708-.708L8.707 9l1.147-1.146a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146z"/>
                                                </svg>
                                            </a>
                                            <a class="mt-3 d-inline-flex" href="/admin/forecast/edit/{{$forecast->id}}">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil" fill="#333" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                                </svg>
                                            </a>
                                        </td>

                                      </tr>
                                      @endforeach

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.master')

@section('content')

    <div class="container profile_page mt-2">
        <div class="row">
            {{-- @include('../includes/cabinet-menu') --}}
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Prognoz natijasini belgilash</h2>
                                <hr class="mt-2">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="text-bold"> <b>{{$forecast->title}}</b> natijasi kiritish</h4>

                            </div>
                        </div>
                        @if ($forecast && $forecast->result == null)

                            <div class="row mt-3">
                                <div class="col-md-12">

                                    <form method="POST" enctype="multipart/form-data" action="/admin/forecastResult/{{$forecast->id}}/create">
                                        @method('post')
                                        @csrf

                                        <div class="form-row">
                                        <div class="form-group col-md-8 mx-auto">
                                            <label for="inputState">Natija qanday?(yes, no, tie)</label>
                                            <select id="forecast_result" name="forecast_result" class="form-control">
                                            <option value="win">Yutdik</option>
                                            <option value="lose">Yutkazdik</option>
                                            <option value="tie">Qaytdi</option>
                                            </select>
                                            @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        </div>

                                        {{-- discount --}}

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="image" class="">Prognoz natijasini skrinshot</label>

                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                                            id="image" name="image"
                                                            value="{{ old('image') }}"
                                                            autocomplete="image">
                                                    <label class="custom-file-label" for="image">Rasm tanlang</label>
                                                    @error('image')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>



                                        <button type="submit" class="btn btn-primary">Natijani yoz</button>
                                    </form>


                                    <script>
                                        document.getElementById('tariff_type').addEventListener('change', function (e) {
                                            // console.log(e.target.value);
                                            if (e.target.value == 'term') {
                                                document.getElementById('quantityItem').classList.add('hidden');
                                                document.getElementById('termItem').classList.remove('hidden');
                                            } else {
                                                document.getElementById('quantityItem').classList.remove('hidden');
                                                document.getElementById('termItem').classList.add('hidden');
                                            }
                                        })
                                    </script>
                                </div>
                            </div>
                        @else
                            <div class="alert alert-warning">
                                Hali bu Prognoz natijasini kiritish uchun barcha o'yinlar tugashi kerak!
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


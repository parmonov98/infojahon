@extends('layouts.master')

@section('content')

    <div class="container profile_page mt-2">
        <div class="row">
            {{-- @include('../includes/cabinet-menu') --}}
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Yangi Tariff qo'shish</h4>
                                <hr class="mt-2">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">

                                <form method="POST" enctype="multipart/form-data" action="/admin/tariff/create">
                                    @method('post')
                                    @csrf

                                    <div class="form-row">
                                      <div class="form-group col-md-8 mx-auto">
                                        <label for="inputState">Donali yoki Muddatli?</label>
                                        <select id="tariff_type" name="type" class="form-control">
                                          <option  value="piece">Donali</option>
                                          <option selected value="term">Muddatli</option>
                                        </select>
                                        @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>
                                      {{-- <div class="form-group col-md-6">
                                        <label for="inputState">Chiqaramiz birdan?</label>
                                        <select id="inputState" name="status" class="form-control">
                                          <option selected value="published" title="Odamlar e'lon qil ni bosgandan srazi ko'rishi mumkin.">ha</option>
                                          <option value="created" title="Odamlar e'lon qil ni bosgandan ko'ra olmaydi o'zi faollashtirishiz kerak.">yo'q</option>
                                        </select>
                                        @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div> --}}

                                    </div>

                                    {{-- discount --}}

                                    <div class="form-row">
                                        <div class="form-group col-md-6" id="termItem">
                                            <label for="term">Prognoz uchun muddat(kunlar soni)</label>
                                            <input type="number" placeholder="1+" name="term" value="0" class="form-control" id="term" >
                                            @error('term')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6 hidden" id="quantityItem">
                                            <label for="quantity">Prognoz soni</label>
                                            <input type="number" placeholder="1+" name="quantity" value="0" class="form-control" id="quantity" >
                                            @error('quantity')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="discount">Skidka %</label>
                                            <input type="number" placeholder="1+" name="discount" value="0" class="form-control" id="discount" >
                                            @error('discount')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="price">Narxi</label>
                                            <input type="number" placeholder="2000+" name="price" class="form-control" id="price" >
                                            @error('price')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
{{--
                                    <div class="form-row">
                                      <div class="form-group col-md-12">
                                        <label for="content">Teoriya matni</label>
                                        <textarea class="form-control" name="content"></textarea>
                                        @error('content')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                      </div>


                                    </div> --}}

                                    <button type="submit" class="btn btn-primary">e'lon qil</button>
                                </form>


                                <script>
                                    document.getElementById('tariff_type').addEventListener('change', function (e) {
                                        // console.log(e.target.value);
                                        if (e.target.value == 'term') {
                                            document.getElementById('quantityItem').classList.add('hidden');
                                            document.getElementById('termItem').classList.remove('hidden');
                                        } else {
                                            document.getElementById('quantityItem').classList.remove('hidden');
                                            document.getElementById('termItem').classList.add('hidden');
                                        }
                                    })
                                </script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <script src="//cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('content', {
            filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script> --}}
    {{-- @include('ckfinder::browser') --}}
{{-- <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>

<script>CKFinder.config( { connectorPath: '/ckfinder/connector' } );</script> --}}

    {{-- <script type="text/javascript">
        // $(document).ready(function () {
        //     $('.ckeditor').ckeditor();
        // });
    </script>

    --}}
@endsection

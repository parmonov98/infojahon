@extends('layouts.master')


@section('content')

<div class="container-fluid">
    <div class="fade-in">

        <!-- /.row-->
        <div class="row">
            <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-align-justify"></i> Barcha kelgan xabarlar</div>
                <div class="card-body table-responsive">
                <table class="table table-bordered table-striped table-sm " style="min-width: 800px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Xabar jo'natuvchi</th>
                        <th>Xabar matni</th>
                        <th>Email</th>
                        <th>Kelgan vaqti</th>
                    </tr>
                    </thead>
                    <tbody class="align-middle ">
                        @foreach ($messages as $message)
                             <tr style="" >
                                <td class="align-middle">{{$message->id}}</td>
                                <td class="align-middle">{{$message->name}}</td>
                                <td class="align-middle">{{$message->message}}</td>
                                <td class="align-middle">{{$message->email}}</td>

                                <td class="align-middle text-center" style="">
                                    {{ date("d M, Y, H:m:i", strtotime($message->created_at)) }}
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>



                {{$messages->links()}}

                </div>
            </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->


    </div>
</div>
  @endsection

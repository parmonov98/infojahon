<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
  <div class="c-sidebar-brand d-lg-down-none">
    <svg class="c-sidebar-brand-full" width="118" height="46" alt="CoreUI Logo">
      <use xlink:href="{{ asset('assets/brand/coreui.svg#full')}}"></use>
    </svg>
    <svg class="c-sidebar-brand-minimized" width="46" height="46" alt="CoreUI Logo">
      <use xlink:href="assets/brand/coreui.svg#signet')}}"></use>
    </svg>
  </div>
  <ul class="c-sidebar-nav">
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-speedometer')}}"></use>
        </svg> Bosh sahifa<span class="badge badge-info">yangi</span></a></li>
    <li class="c-sidebar-nav-title">Foydalanuvchilar</li>
    {{-- <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="colors.html">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-drop1')}}"></use>
    </svg> Yangi Qo'shilganlar </a></li> --}}
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/users">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-drop1')}}"></use>
        </svg> Barchasi </a></li>
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link" href="/admin/payments">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
        </svg> Pul Kiritishlar </a></li>
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link" href="/admin/withdrawals">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
        </svg> Pul chiqarishlar </a></li>
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link" href="/admin/messages">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-pencil')}}"></use>
        </svg> Xabarlar </a>
    </li>
    <li class="c-sidebar-nav-title">Prognoz amallar</li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/forecast/add">
        Qo'shish </a>
    </li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/forecast/all">
        Prognozlar </a>
    </li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/forecastResults">
        Natijalar </a>
    </li>
    <li class="c-sidebar-nav-title">Teoriyalar bilan ishlash</li>

    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/theory/create">
        Yozish </a>
    </li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/theory/all">
        Teoriyalar </a>
    </li>
    {{-- <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/theory/stats">
         Statistikasi </a></li> --}}


    <li class="c-sidebar-nav-title">Tariflar bilan ishlash</li>

    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/tariff/create">
        Qo'shish </a>
    </li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/tariff/all">
        Tariflar </a>
    </li>

    <li class="c-sidebar-nav-title">Widgetlar bilan ishlash</li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="/admin/widgets">
        Vidjetlar </a></li>


    {{-- <li class="c-sidebar-nav-item mt-auto"><a class="c-sidebar-nav-link c-sidebar-nav-link-success" href="https://coreui.io" target="_top">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-cloud-download')}}"></use>
    </svg> Download CoreUI</a></li> --}}
    <li class="c-sidebar-nav-item">
      <a class="c-sidebar-nav-link c-sidebar-nav-link-danger" href="{{ route('logout') }}" target="_top" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('vendors/@coreui/icons/svg/free.svg#cil-layers')}}"></use>
        </svg> Chiqish <strong> Paneldan</strong></a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
      </form>
    </li>


  </ul>
  <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div>


@extends('layouts.app')

@section('content')
<div class="container register_page">
    <div class="row justify-content-center mt-1">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header h3">{{ __("Ro'yxatdan o'tish") }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf


                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Foydalanuvchi nomi') }}</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username">

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mailingiz') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __("Parol qo'ying") }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Parolni takrorlang') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __("Ro'yxatdan o'tish") }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
				<hr/>
				{{-- <div class="card-body text-center">
                    <div class="tg_login_btn text-center">
                        <script async src="https://telegram.org/js/telegram-widget.js?12" data-telegram-login="DevstoreUZ_bot" data-size="large" data-auth-url="https://devstore.uz/auth" data-request-access="write"></script>
                    </div>
                </div> --}}
                <div class="card-body text-center">
                    <div class="tg_login_btn text-center">
                        <script async src="https://telegram.org/js/telegram-widget.js?14" data-telegram-login="JahongirRU_bot" data-size="medium" data-auth-url="https://infojahon.ru/auth" data-request-access="write"></script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

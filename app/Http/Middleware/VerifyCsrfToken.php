<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    // var $url = route('ckeditor.image-upload');

    protected $except = [
        // 'ckfinder/*',
        'bot/*',
        'admin/ajax/*',
        'ckeditor/upload',
        'ckfinder/*',
        'freekassa/*'
    ];
}

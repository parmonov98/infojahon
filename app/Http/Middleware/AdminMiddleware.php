<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (Auth::user()->role == 'admin' or Auth::user()->role == 'developer') {
            // if ($request->fullUrl() ) {
            //     # code...
            // }
            return $next($request);
        }else{
            return redirect('/cabinet')->with('error', 'Siz Admin emassiz!');
        }
    }
}

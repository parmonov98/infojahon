<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {

        $this->middleware('guest');

        // ddd()
        // ddd($value = Cookie::get('referer'));
        // ddd(Cookie::get('referer'));

        // ddd(Crypt::decrypt(Cookie::get('referer'), false));

        if ($request->query('referer')) {
            // $response->withCookie(cookie()->forever('key', $value));
            Cookie::queue(Cookie::forever('referer', $request->query('referer'), 5));
        }

    }



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'username.required' => "Foydalanuvchi nomi harf va raqamlardan iborat bo'lishi shart",
            'username.unique' => "Foydalanuvchi nomi baza mavjud, iltimos boshqa tanlang!",
            'password.required' => "Parol kamida 8 ta belgidan iborat bo'lishi shart!",
            'password.min' => "Parol kamida :min ta belgidan iborat bo'lishi shart!",
            'password.confirmed' => "Takroriy parol birinchisi bilan bir xil bo'lishi kerak!",
            'email.required' => "Email kiritish majburiy!",
            'email.email' => "Email noto'g'ri formatda kiritildi!",
            'email.unique' => "Email bazada mavjud!",
        ];
        return Validator::make($data, [
            'username' => ['required', "unique:users,username", 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        // ddd($data);
        // ddd(Cookie::get('referer'));
        // $referer = explode('|', Crypt::decrypt(Cookie::get('referer'), false));

        if (User::firstWhere('username', Cookie::get('referer'))) {
            // ddd(User::firstWhere('username', Cookie::get('referer')));
            return User::create([
                'email' => $data['email'],
                'name' => $data['username'],
                'username' => $data['username'],
                'password' => Hash::make($data['password']),
                'referer' => Cookie::get('referer'),
                'token' => Str::random(32)
            ]);

        }else{
            return User::create([
                'email' => $data['email'],
                'username' => $data['username'],
                'name' => $data['username'],
                'password' => Hash::make($data['password']),
                'token' => Str::random(32)
            ]);
        }

    }
}

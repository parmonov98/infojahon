<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function authenticate(Request $request)
    {

        // ddd($request->all());
        // $credentials = $request->only('email', 'password');
        // ddd($request->all());

        $validator = Validator::make($request->all(), [
            'email'           => 'required|email',
            'password'           => 'required',
            // 'captcha'           => 'required|captcha',
        ]);


        if ($validator->fails()) {
            // ddd($validator->errors());
            return $validator;
        } else {

            // ddd($request->all());

            $credentials = ['username' => $request->email, 'password' => $request->password ];

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                // Success
                // dd(1);
                if (!Auth::user()->email_verified_at && Auth::user()->type == 'signup') {
                    // Auth::user()->sendEmailVerificationNotification();
                    Auth::logout();
                    // ddd($request);
                    return redirect('/login')->with('error', __('Sizning Emailingiz tasdiqlanmagan! Pul chiqarish uchun tasdiqlanishi kerak!'));
                }
                // dd(Auth::user()->role);
                // if (Auth::user()->role === 'admin') {
                //     return redirect()->intended('/admin');
                // }
                return redirect()->back();
            } else {
                // Go back on error (or do what you want)
                // ddd($validator->errors());

                // $errors = new MessageBag(['password' => [__('login.login_or_email_invalid')]]);


                return redirect()->back()->withErrors('error', "Email Yoki parol noto'g'ri!");
                // return redirect()->back();
            }
        }

    }

    public function __construct(Request $request)
    {
        // ddd($request->all());
        $this->authenticate($request);

        $this->middleware('guest')->except('logout');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Forecast;
use App\Models\Tariff;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TariffController extends Controller
{
    //
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function createTariff()
    {
        return view('admin.pages.addtariff');
    }


    public function editTariff(Tariff $tariff)
    {
        // ddd($tariff);

        return view('admin.pages.edittariff', compact('tariff'));

    }

    public function storeTariff(Request $request)
    {

        $tariff = new Tariff;
        // ddd(1, $forecast);
        $data = Validator::make($request->all(), [
            'type' => ['required', Rule::in(['term', 'piece'])],
            'term' => 'sometimes|numeric',
            'quantity' => 'sometimes|numeric',
            'discount' => 'sometimes|numeric',
            'price' => 'required|numeric'
        ]);

        // $tariff->user_id = Auth::id();
        // ddd($data);
            // ddd($request->type);
        // ddd($data->discount);
        $tariff->type = $request->type;

        $tariff->term = $request->term;
        $tariff->quantity = $request->quantity ? $request->quantity : 0;
        $tariff->discount = $request->discount;
        $tariff->price = $request->price;

        $res = $tariff->save();


        // ddd($res);
        // $data = array_merge($data1, $data2, $data3, $data4);

        if ($res) {
            // ddd(11);
            // ddd($res);
            return redirect('/admin/tariff/all')->with("success", "tariff omadli saqlandi!");
        }else{
            return redirect('/admin/tariff/add')->with("error", "tariff saqlanmadi iltimos to'g'ri ma'lumot kiriting!");
        }


    }

    public function updateTariff(Tariff $tariff, Request $request)
    {

        // ddd($tariff);
        // ddd($request->all());


        if(Gate::forUser(Auth::user())->allows('update', $tariff)){
            # code...
            // return view('admin.pages.edittariff', compact('tariff'));

            // $tariff = new Tariff;
            // ddd(1, $forecast);

            $data = Validator::make($request->all(), [
                'type' => ['required', Rule::in(['term', 'piece'])],
                'term' => 'sometimes|numeric',
                'quantity' => 'sometimes|numeric',
                'discount' => 'sometimes|numeric',
                'price' => 'required|numeric'
            ]);

            // $tariff->user_id = Auth::id();
            // ddd($data);
                // ddd($request->type);
            // ddd($data->discount);
            $tariff->type = $request->type;

            $tariff->term = $request->term;
            $tariff->quantity = $request->quantity ? $request->quantity : 0;
            $tariff->discount = $request->discount;
            $tariff->price = $request->price;

            $res = $tariff->save();


            // ddd($res);
            // $data = array_merge($data1, $data2, $data3, $data4);

            if ($res) {
                // ddd(11);
                // ddd($res);
                return redirect('/admin/tariff/all')->with("success", "tariff omadli bazaga qo'shildi!");
            }else{
                return redirect('/admin/tariff/add')->with("error", "tariff saqlanmadi iltimos to'g'ri ma'lumot kiriting!");
            }
        }else{
            return abort(401);
        }

    }


    public function deleteTariff($tariff)
    {

        // ddd($tariff);

        $t = Tariff::findOrFail($tariff);

        $t->delete();
        return redirect('/admin/tariff/all')->with('success', "Tariff omadli o'chirildi.");

    }


    public function allTariffs()
    {
        $tariffs = Tariff::paginate(10);
        return view('admin.pages.alltariffs', compact('tariffs'));
    }




}

<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    //index


    public function __contruct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }

    //
    public function edit(){

        if (Auth::check()) {
            // ddd(111);
            $user = Auth::user();
            // $user->profile = Profile::findOrFail(Auth::id());
            $user->load(['profile', 'account']);
            // ddd($user);
            return view('pages.cabinet', compact('user'));
        }else{
            return redirect('login');
        }

    }

    public function store(Request $request)
    {

        if (Auth::check()) {
            $user = Auth::user();
            $user->load(['profile', 'account']);
            $profile = $user->profile;
            $account = $user->account;
            // ddd(1, $profile);
            // $messages = [
            //     'first_name' => 'sometimes|string',
            //     'last_name' => 'sometimes|string',
            // ];
            // $validator = Validator::make($request->all(), [
            //     'image' => 'sometimes|image',
            //     'first_name' => 'sometimes|string',
            //     'last_name' => 'sometimes|string',
            //     'yandex_money' => 'sometimes|string',
            //     'qiwi' => 'sometimes|string',
            //     'url' => 'sometimes|url'
            // ], $messages);

            // if ($validator->fails()) {
            //     return redirect()->back()->withErrors($validator);
            // }
            // ddd($validator);

            if ($request->hasFile('image')) {

                $img_path = Storage::disk('public')->put('uploads', $request->image);
                // ddd($img_path);
                $img = Image::make(storage_path('app/public/') . "{$img_path}")->fit(400, 400);
                $img->save();
                $profile->image = $img_path;

            }

            $profile->first_name = $request->first_name;
            $profile->last_name = $request->last_name;
            $profile->url = $request->url;


            $res = $profile->save();

            $account->yandex_money = $request->yandex_money ? $request->yandex_money : '';
            $account->qiwi = $request->qiwi ? $request->qiwi : '-';
            $res = $account->save();
            // ddd($res);
            // $data = array_merge($data1, $data2, $data3, $data4);

            if ($res) {
                // ddd(11);
                // ddd($res);
                return redirect('/cabinet')->with("success", "Profilingiz ma'lumotlari saqlandi!");
            }else{
                return redirect('/cabinet')->with("error", "Profilingiz ma'lumotlaringiz o'zgarmadi!");
            }

        }else{
            abort(401);
        }

        // ddd(222);


    }

    public function notifications(){

        if (Auth::check()) {
            // ddd(111);
            $user = Auth::user();
            // ddd($user->account);
            // $user->profile = Profile::findOrFail(Auth::id());

            // ddd($user);
            return view('pages.notifications', compact('user'));
        }else{
            return redirect('login');
        }


    }

    public function referals(){

		// ddd(1);
        if (Auth::check()) {
            // ddd(111);
            $user = Auth::user();

            $referals = User::where('referer', $user->username)->get();

			if($referals->count() > 0){
				$referals->each(function($ref){
					$ref->profit = 0;
					$payments = Payment::where('user_id', $ref->id)->get();
					foreach ($payments as $key => $payment) {
						$ref->profit += $payment->sum;
					}
					$ref->profit = $ref->profit / 100 * env('AFFILIATE_PERCENT');

				});
			}

            // dd($user);

            return view('pages.referals', compact('user', 'referals'));
        }else{
            return redirect('login');
        }


    }


    public function affiliate(){

        if (Auth::check()) {
            // ddd(111);
            $user = Auth::user();
            // ddd($user->account);
            // $user->profile = Profile::findOrFail(Auth::id());
            // ddd($user);

            return view('pages.affiliate', compact('user'));
        }else{
            return redirect('login');
        }


    }


}

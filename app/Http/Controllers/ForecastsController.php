<?php

namespace App\Http\Controllers;

use Acadea\CollectionPaginator\CollectionPaginator;
use App\Models\Forecast;
use App\Models\ForecastResult;
use App\Models\purchasedItem;
use App\Models\Tariff;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ForecastsController extends Controller
{


    public function addForecast()
    {
        return view('admin.pages.addforecast');
    }

    public function allForecasts()
    {
        $forecasts = Forecast::orderBy('id', 'DESC')->paginate(50);
        return view('admin.pages.allforecasts', compact('forecasts'));
    }

    public function storeForecast(Request $request)
    {



        $forecast = new Forecast;

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|unique:forecasts,title',
            'sport_type' => 'required|string',
            'chempionat' => 'required|string|max:200',
            'forecast_hint' => 'required|string|max:200',
            'bet_type' => 'required|string',
            'bet_sum' => 'required|numeric',
            'first_team' => 'sometimes',
            'second_team' => 'sometimes',
            'image' => 'required|image',
            'partner_url' => 'required|string',
            'explanation' => 'required',
            'forecast_value' => 'required|string',
            'kf' => 'required|numeric|between:0,99.99',
            'type' => 'required',
            'status' => 'required',
            'begin' => 'required',
            'end' => 'required',
            'publishing_time' => 'sometimes',
        ]);

        if ($validator->fails()) {
            // ddd($validator->errors());
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // ddd($request->all());

        if ($request->image) {
            $img_path = Storage::disk('public')->put('uploads', $request->image);
            // dd($request->image);

            $forecast->image = $img_path;
            $new_img_name = explode('/', $img_path);
            $new_img_name = 'portrait_' . $new_img_name[1];
            // dd($new_img_name);

            $img1 = storage_path('app/public/') . $img_path;
            $img2 = storage_path('app/public/uploads/') . $new_img_name;
            // dd($img1, $img2);
            if (\File::copy($img1, $img2)) {
                // dd("success");
                $new_img = Image::make($img2)->fit(320, 580);
                $new_img->save();
                // dd($new_img_name, $img2);
                $forecast->image_portrait =  'uploads/' . $new_img_name;
            }
            // dd(file_exists($img2));

        }



        $forecast->user_id = Auth::id();
        $forecast->sport_type = $request->sport_type;
        $forecast->begin = $request->begin;
        $forecast->end = $request->end;
        $forecast->chempionat = $request->chempionat;
        $forecast->title = $request->title;
        $forecast->link = ru2lat($request->title);
        $forecast->forecast_hint = $request->forecast_hint;
        $forecast->bet_type = $request->bet_type;
        $forecast->forecast_value = $request->forecast_value;
        $forecast->partner_url = $request->partner_url;
        $forecast->first_team = $request->first_team;
        $forecast->second_team = $request->second_team;
        $forecast->kf = $request->kf;
        $forecast->explanation = $request->explanation;
        $forecast->type = $request->type;
        $forecast->status = $request->status;
        $forecast->bet_sum = $request->bet_sum;
        $forecast->publishing_time = $request->publishing_time;

        // dd($request->publishing_time);
        // ddd($forecast);
        $res = $forecast->save();

        // ddd($res);
        // $data = array_merge($data1, $data2, $data3, $data4);

        if ($res) {
            // ddd(11);
            // ddd($res);
            return redirect('/admin/forecast/all')->with("success", "Prognoz omadli bazaga qo'shildi!");
        } else {
            return redirect('/admin/forecast/add')->with("error", "Prognoz saqlanmadi iltimos to'g'ri ma'lumot kiriting!");
        }


    }

    public function editForecast($forecast_id)
    {
        // ddd($forecast_id);
        // ddd(Forecast::find($forecast_id));
        $forecast = Forecast::findOrFail($forecast_id);
        return view('admin.pages.editforecast', compact('forecast'));
    }

    // // editForecastResult
    // public function editForecastResult($forecast_id)
    // {

    //     $forecastResult = ForecastResult::where('forecast_id', $forecast_id)->get();

    //     ddd($forecastResult);

    //     return view('admin.pages.editforecast', compact('forecast'));
    // }
    public function updateForecast(Request $request, $forecast_id)
    {

        // ddd($request->all());
        // ddd($forecast_id);

        $forecast = Forecast::find($forecast_id);

        // ddd($forecast);

        $data = Validator::make($request->all(), [
            'title' => 'required|string',
            'sport_type' => 'required|string',
            'chempionat' => 'required|string|max:200',
            'forecast_hint' => 'required|string|max:200',
            'bet_type' => 'required|string',
            'bet_sum' => 'required|numeric',
            'first_team' => 'required|string',
            'second_team' => 'required|string',
            'image' => 'required|image',
            'explanation' => 'required',
            'forecast_value' => 'required|string',
            'kf' => 'required|numeric|between:0,99.99',
            'type' => 'required',
            'status' => 'required',
            'begin' => 'required|datetime',
            'end' => 'required|datetime',
            'publishing_time' => 'required|datetime',
        ]);

        // ddd($request->all());

        if ($request->image) {
            $img_path = Storage::disk('public')->put('uploads', $request->image);
            // dd($request->image);
            // ddd($img_path);

            $img = Image::make(storage_path('app/public/') . $img_path)->fit(320, 260);
            $img->save();
            $forecast->image = $img_path;
            $new_img_name = explode('/', $img_path);
            $new_img_name = 'portrait_' . $new_img_name[1];
            // dd($new_img_name);

            $img1 = storage_path('app/public/') . $img_path;
            $img2 = storage_path('app/public/uploads/') . $new_img_name;
            // dd($img1, $img2);
            if (\File::copy($img1, $img2)) {
                // dd("success");
                $new_img = Image::make($img2)->fit(320, 580);
                $new_img->save();
                // dd($new_img_name, $img2);
                $forecast->image_portrait =  'uploads/' . $new_img_name;
            }
            // dd(file_exists($img2));

        }

        $forecast->user_id = Auth::id();
        $forecast->sport_type = $request->sport_type;
        $forecast->begin = $request->begin;
        $forecast->end = $request->end;
        $forecast->chempionat = $request->chempionat;
        $forecast->title = $request->title;
        $forecast->link = ru2lat($request->title);
        $forecast->forecast_hint = $request->forecast_hint;
        $forecast->bet_type = $request->bet_type;
        $forecast->forecast_value = $request->forecast_value;
        $forecast->first_team = $request->first_team;
        $forecast->second_team = $request->second_team;
        $forecast->kf = $request->kf;
        $forecast->explanation = $request->explanation;
        $forecast->type = $request->type;
        $forecast->status = $request->status;
        $forecast->bet_sum = $request->bet_sum;
        $forecast->publishing_time = $request->publishing_time;

        // ddd($forecast);
        $res = $forecast->save();
        // ddd($res);
        // $data = array_merge($data1, $data2, $data3, $data4);

        if ($res) {
            // ddd(11);
            // ddd($res);
            return redirect('/admin/forecast/all')->with("success", "Prognoz omadli yangilandi!");
        } else {
            return redirect('/admin/forecast/add')->with("error", "Prognoz saqlanmadi iltimos to'g'ri ma'lumot kiriting!");
        }

    }

    public function deleteForecast($forecast_id)
    {

        $f = Forecast::findOrFail($forecast_id);

        $f->delete();
        ForecastResult::where('forecast_id', $f->id)->delete();


        return redirect('/admin/forecast/all')->with('success', "Prognoz omadli o'chirildi.");
    }



    public function addForecastResult(Forecast $forecast, Request $request)
    {
        $forecast->load('result');

        if ($forecast->is_ended !== 'yes') {
            abort('404', 'Bu prognoz Stavka vaqti hali tugamadi!');
        }
        // ddd($forecast);

        return view('admin.pages.addForecastResut', compact('forecast'));
    }

    public function allForecastResult(Request $request)
    {
        $forecasts = Forecast::whereHas('result', function ($builder) {
            $builder->where('screenshot', null);
        })->orderBy('created_at', 'desc')->get();
        // ddd($forecasts);

        $forecastResults = CollectionPaginator::paginate($forecasts, 5);

        // $forecast->load('result');
        // ddd($forecast);

        return view('admin.pages.allForecastResut', compact('forecastResults'));
    }

    public function storeForecastResult(Forecast $forecast, Request $request)
    {


        $result = new ForecastResult();

        // $result->screenshot = $request->;

        $data = Validator::make($request->all(), [
            'forecast_result' => 'required|string',
            'image' => 'required|image',
        ]);

        $img_path = Storage::disk('public')->put('uploads', $request->image);
        // ddd($img_path);
        $img = Image::make(storage_path('app/public/') . $img_path);
        $img->save();
        $result->screenshot = $img_path;
        $result->forecast_id = $forecast->id;
        $result->result = $request->forecast_result;

        $result->save();
        $result->refresh();

        $forecast->is_ended = 'yes';
        $forecast->save();
        // ddd($result);
        // ddd($request->all());

        return redirect('/admin/')->with('success', 'Prognoz natijasi yozib qo`yildi!');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = 'free') // type of forecasts page.
    {
        ddd($type);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($link)
    {


        $forecast = Forecast::where('link', $link)->first();
        // ddd($forecast);
        if ($forecast != null) {

            if ($forecast->type === 'paid') {
                // ddd($forecast);
                // ddd(Auth::user()->account->purchased_forecasts);
                // ddd($forecast->id);

                if (Auth::check()) {
                    # code...
                    $item = purchasedItem::where('forecast_id', $forecast->id)->where('user_id', Auth::id())->get();

                    if ($item->count() == 1){
                        // dd($item);

                    }elseif(Auth::user()->account->quantity > 0) {
                        Auth::user()->account->quantity -= 1;
                        Auth::user()->account->save();

                        $purchasedItem = new purchasedItem();
                        $purchasedItem->user_id = Auth::id();
                        $purchasedItem->forecast_id = $forecast->id;
                        $purchasedItem->type = 'forecast';
                        $purchasedItem->save();

                    }elseif(Carbon::parse(Auth::user()->account->expire_date) > Carbon::now()) {
                        $purchasedItem = new purchasedItem();
                        $purchasedItem->user_id = Auth::id();
                        $purchasedItem->forecast_id = $forecast->id;
                        $purchasedItem->type = 'forecast';
                        $purchasedItem->save();
                    }else{
                        return redirect('/tariffs')->with('success', "Pullik prognozni ko'rish uchun tariff sotib olishingiz kerak!");
                    }
                }else{
                    return redirect('/tariffs')->with('success', "Pullik prognozni ko'rish uchun tariff sotib olishingiz kerak!");
                }
            }
        }else{
            // ddd($link, $forecast);
            return redirect(route('forecasts.free'))->with('alert', "Prognoz topilmadi!");
        }

        $timerForecasts = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->get();

        return view('forecasts.show', compact('forecast', 'timerForecasts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Acadea\CollectionPaginator\CollectionPaginator;
use App\Models\Forecast;
use App\Models\purchasedItem;
use App\Models\Theory;
use App\Models\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;

class TheoriesController extends Controller
{
    //theories

    public function index(){


        $free = Theory::where('type', 'free')->where('status', 'published')->orderBy('id', 'DESC')->take(6)->get();
        $paid = Theory::where('type', 'paid')->where('status', 'published')->orderBy('id', 'DESC')->take(6)->get();
        // $views['free'] = [];
        // $views['paid'] = [];
        $min = $free->min('count_views');
        $max = $free->max('count_views');
        $free->each(function ($item) use ($min, $max)
        {
            if ($item->count_views != 0) {
                $item->rating = $item->count_views / ($max / 5);
            }else{
                $item->rating = 0;
            }
        });

        $min = $paid->min('count_views');
        $max = $paid->max('count_views');
        $paid->each(function ($item) use ($min, $max)
        {
            if ($item->count_views != 0) {
                $item->rating = $item->count_views / ($max / 5);
            }else{
                $item->rating = 0;
            }
        });
        // dd($item->rating);


        // $paid->each(function ($item)
        // {
        //     $views['free'][] = $item->count_views;
        // });

        // $theories['free'] = $free;
        // $theories['paid'] = $paid;

        // ddd($free, $paid);

        $free = CollectionPaginator::paginate($free, 5);
        $paid = CollectionPaginator::paginate($paid, 5);
        // dd($free);
        return view('theories.index', compact('free', 'paid'));
    }

    public function free(){


        $free = Theory::where('type', 'free')->where('status', 'published')->orderBy('id', 'DESC')->get();
        // $views['free'] = [];
        // $views['paid'] = [];
        $min = $free->min('count_views');
        $max = $free->max('count_views');
        $free->each(function ($item) use ($min, $max)
        {
            if ($item->count_views != 0) {
                $item->rating = $item->count_views / ($max / 5);
            }else{
                $item->rating = 0;
            }
        });

        // dd(1);

        $free = CollectionPaginator::paginate($free, 7);
        $widgets = new Collection();
        $widgets->content = Widget::where('type', 'content')->where('page', 'theories')->where('status', 'active')->get();
        $widgets->sidebar = Widget::where('type', 'sidebar')->where('page', 'theories')->where('status', 'active')->get();
        // dd($widgets);

        // timer forecast
        $timerForecasts = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('theories.free', compact('free', 'timerForecasts', 'widgets'));
    }

    public function paid(){

        $paid = Theory::where('type', 'paid')->where('status', 'published')->orderBy('id', 'DESC')->get();

        $min = $paid->min('count_views');
        $max = $paid->max('count_views');
        $paid->each(function ($item) use ($min, $max)
        {
            if ($item->count_views != 0) {
                $item->rating = $item->count_views / ($max / 5);
            }else{
                $item->rating = 0;
            }
        });

        $paid = CollectionPaginator::paginate($paid, 7);
        // dd($free);

        $widgets = new Collection();
        $widgets->content = Widget::where('type', 'content')->where('page', 'strategies')->where('status', 'active')->get();
        $widgets->sidebar = Widget::where('type', 'sidebar')->where('page', 'strategies')->where('status', 'active')->get();

        // dd($widgets);
        // timer forecast
        $timerForecasts = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->get();

        return view('theories.paid', compact('paid', 'widgets', 'timerForecasts'));
    }



    public function show($link)
    {
        $theory = Theory::where('link', $link)->first();
        // dd($theory);
        if ($theory) {
            # code...
            if ($theory->type == 'paid') {
                if (Auth::check()) {

                    $item = purchasedItem::where('theory_id', $theory->id)->where('user_id', Auth::id())->get();

                    if ($item->count() == 1){
                        // dd($item);

                    }elseif(Auth::user()->account->quantity > 0) {
                        $purchasedItem = new purchasedItem();
                        $purchasedItem->user_id = Auth::id();
                        $purchasedItem->theory_id = $theory->id;
                        $purchasedItem->type = 'theory';
                        $purchasedItem->save();

                    }elseif(Carbon::parse(Auth::user()->account->expire_date) > Carbon::now()) {
                        $purchasedItem = new purchasedItem();
                        $purchasedItem->user_id = Auth::id();
                        $purchasedItem->theory_id = $theory->id;
                        $purchasedItem->type = 'theory';
                        $purchasedItem->save();
                    }else{
                        return redirect('/tariffs')->with('success', "Pullik strategiyani ko'rish uchun tariff sotib olishingiz kerak!");
                    }
                }else{
                    return redirect('/tariffs')->with('success', "Pullik strategiyani ko'rish uchun tariff sotib olishingiz kerak!");
                }
            }else{
                // free theory
            }
        }else{
            return redirect(route('home'))->with('alert', "Bu teoriya baza topilmadi!");
        }

        Theory::where('link', $link)->increment('count_views');
        // ddd($link, $forecast);

        // timer forecast
        $timerForecasts = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->get();

        return view('theories.show', compact('theory', 'timerForecasts'));
    }

    public function createTheory()
    {
        return view('admin.pages.createtheory');
    }

    public function storeTheory(Request $request)
    {
        $theory = new Theory;
        // ddd(1, $request->all());

        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'image' => 'required|image',
            'for_beginners' => 'required|in:yes,no',
            'type' => 'required',
            'status' => 'required',
            'content' => 'required|string',
        ]);

        if ($validator->fails()) {
            // ddd($validator->errors());
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // ddd($img_path);

        // if ($request->image) {
        //     $img_path = Storage::disk('public')->put('uploads', $request->image);
        //     // dd($request->image);
        //     // ddd($img_path);

        //     $img = Image::make(storage_path('app/public/') . $img_path)->fit(320, 260);
        //     $img->save();
        //     $theory->image = $img_path;
        //     $new_img_name = explode('/', $img_path);
        //     $new_img_name = 'portrait_' . $new_img_name[1];
        //     // dd($new_img_name);

        //     $img1 = storage_path('app/public/') . $img_path;
        //     $img2 = storage_path('app/public/uploads/') . $new_img_name;
        //     // dd($img1, $img2);
        //     if (\File::copy($img1, $img2)) {
        //         // dd("success");
        //         $new_img = Image::make($img2)->fit(320, 580);
        //         $new_img->save();
        //         // dd($new_img_name, $img2);
        //         $theory->image_portrait =  'uploads/' . $new_img_name;
        //     }
        //     // dd(file_exists($img2));

        // }

        if ($request->image) {
            $img_path = Storage::disk('public')->put('uploads', $request->image);
            // dd($request->image);

            $theory->image = $img_path;
            $new_img_name = explode('/', $img_path);
            $new_img_name = 'portrait_' . $new_img_name[1];
            // dd($new_img_name);

            $img1 = storage_path('app/public/') . $img_path;
            $img2 = storage_path('app/public/uploads/') . $new_img_name;
            // dd($img1, $img2);
            if (\File::copy($img1, $img2)) {
                // dd("success");
                $new_img = Image::make($img2)->fit(320, 580);
                $new_img->save();
                // dd($new_img_name, $img2);
                $theory->image_portrait =  'uploads/' . $new_img_name;
            }
            // dd(file_exists($img2));

        }

        // ddd($theory);

        $theory->user_id = Auth::id();

        $theory->title = $request->title;
        $theory->link = ru2lat($request->title);
        $theory->for_beginners = $request->for_beginners;
        $theory->content = $request->content;
        $theory->type = $request->type;
        $theory->status = $request->status;

        $res = $theory->save();
        $theory->refresh();
        // dd($theory);
        // ddd($res);
        // $data = array_merge($data1, $data2, $data3, $data4);

        if ($res) {
            // ddd(11);
            // ddd($res);
            return redirect('/admin/theory/all')->with("success", "Teoriya omadli bazaga qo'shildi!");
        } else {
            return redirect('/admin/theory/add')->with("error", "Teoriya saqlanmadi iltimos to'g'ri ma'lumot kiriting!");
        }

    }


    public function editTheory(Theory $theory)
    {
        // ddd($theory);
        return view('admin.pages.edittheory', compact('theory'));
    }

    public function updateTheory(Theory $theory, Request $request)
    {

        // dd($request->all());

        $data = Validator::make($request->all(), [
            'title' => 'required|string',
            'image' => 'required|image',
            'for_beginners' => 'required|in:yes,no',
            'type' => 'required',
            'status' => 'required',
            'content' => 'required|string',
        ]);

        // if ($request->image) {
        //     $img_path = Storage::disk('public')->put('uploads', $request->image);
        //     // dd($request->image);
        //     // ddd($img_path);

        //     $img = Image::make(storage_path('app/public/') . $img_path)->fit(320, 260);
        //     $img->save();
        //     $theory->image = $img_path;
        //     $new_img_name = explode('/', $img_path);
        //     $new_img_name = 'portrait_' . $new_img_name[1];
        //     // dd($new_img_name);

        //     $img1 = storage_path('app/public/') . $img_path;
        //     $img2 = storage_path('app/public/uploads/') . $new_img_name;
        //     // dd($img1, $img2);
        //     if (\File::copy($img1, $img2)) {
        //         // dd("success");
        //         $new_img = Image::make($img2)->fit(320, 580);
        //         $new_img->save();
        //         // dd($new_img_name, $img2);
        //         $theory->image_portrait =  'uploads/' . $new_img_name;
        //     }
        //     // dd(file_exists($img2));

        // }
        if ($request->image) {
            $img_path = Storage::disk('public')->put('uploads', $request->image);
            // dd($request->image);

            $theory->image = $img_path;
            $new_img_name = explode('/', $img_path);
            $new_img_name = 'portrait_' . $new_img_name[1];
            // dd($new_img_name);

            $img1 = storage_path('app/public/') . $img_path;
            $img2 = storage_path('app/public/uploads/') . $new_img_name;
            // dd($img1, $img2);
            if (\File::copy($img1, $img2)) {
                // dd("success");
                $new_img = Image::make($img2)->fit(320, 580);
                $new_img->save();
                // dd($new_img_name, $img2);
                $theory->image_portrait =  'uploads/' . $new_img_name;
            }
            // dd(file_exists($img2));

        }

        $theory->user_id = Auth::id();

        $theory->title = $request->title;
        $theory->link = ru2lat($request->title);
        $theory->for_beginners = $request->for_beginners;
        $theory->content = $request->content;
        $theory->type = $request->type;
        $theory->status = $request->status;
        $res = $theory->save();

        // ddd($res);
        // $data = array_merge($data1, $data2, $data3, $data4);

        if ($res) {
            // ddd(11);
            // ddd($res);
            return redirect('/admin/theory/all')->with("success", "Teoriya omadli yangilandi!");
        } else {
            return redirect('/admin/theory/add')->with("error", "Teoriya saqlanmadi iltimos to'g'ri ma'lumot kiriting!");
        }
    }

    public function allTheories()
    {
        $theories = Theory::orderBy('id', 'DESC')->paginate(50);
        return view('admin.pages.alltheories', compact('theories'));
    }


    public function deleteTheory(Theory $theory, Request $request)
    {
        // dd(1);
        // dd($request->all());

        if ($theory) {
            $theory->delete();
            return redirect(route('alltheories'))->with('success', 'Teoriya o`chirildi');
        }else{
            return redirect(route('alltheories'))->with('error', 'Teoriya o`chirilmadi');
        }
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use function GuzzleHttp\json_encode;

class BotController extends Controller
{
    //


    public function sendMessage(Request $request)
    {
        file_put_contents('bot-test.json', json_encode($request->all()));
        if ($request->has('type')) {
            $data = [
                'chat_id' => env('TG_ADMIN_USERNAME', -1001489553175),
                'text' => "Turi:" . $request->type . "\n" . $request->message,
                'parse_mode' => 'HTML'
            ];
            // print_r($data);
            echo $this->sendBotMessage($data, 'sendMessage');

        }else{

            $data = [
                'chat_id' => $request->message['chat']['id'],
                'text' => 'got it'
            ];
            // print_r($data);
            echo $this->sendBotMessage($data, 'sendMessage');
            // $http = Http::post(env('BOT_URL'), $data);
            // file_put_contents()
            // return response()->json()
        }
        // print_r($request->all());
        die;
    }


    function sendBotMessage($content, $method, $type = '')
    {

        $curl = curl_init();

        // set url
        curl_setopt($curl, CURLOPT_URL, env('BOT_URL', "https://api.telegram.org/bot1360040119:AAFa0m4cQlsi9b2WpiEBRITE40uebkD8Ztg/") . $method);

        //return the transfer as a string
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        // $output contains the output string
        $output = curl_exec($curl);

        curl_close($curl);

        // file_put_contents("return_sent.txt", $output);
        return $output;

    }
}

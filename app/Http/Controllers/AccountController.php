<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\Purchase;
use App\Models\Tariff;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Maksa988\FreeKassa\Facades\FreeKassa;
use Illuminate\Support\Str;


class AccountController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }
    public function purchase(){

        $user = Auth::user();

        return view('account.addup', compact('user'));
    }

       //
    public function addup(){

        $user = Auth::user();
            // ddd($user->account);
            // $user->profile = Profile::findOrFail(Auth::id());

            // ddd($user);
            // ddd($user->referer);

        return view('account.addup', compact('user'));

    }
    public function withdrawals(){

        $withdrawals = Withdrawal::with(['user', 'user.profile'])->paginate(25);

        // dd($withdrawals->first()->account->user);
        // ddd($withdrawals);
        return view('admin.pages.withdrawals', compact('withdrawals'));

    }
    public function allPayments(Request $request){


        if ($request->has('q')) {
            $payments = Payment::where('public_id', 'LIKE', "%{$request->q}%")->orderBy('id', 'DESC')->with(['user', 'user.profile'])->paginate(25);

        }else{
            $payments = Payment::orderBy('id', 'DESC')->with(['user', 'user.profile'])->paginate(25);
        }

        // dd($withdrawals->first()->account->user);
        // ddd($withdrawals);
        return view('admin.pages.payments', compact('payments'));
    }
       //
    public function withdrawal(){

        $user = Auth::user();
        $user->load('account');
            // ddd($user->account);
            // $user->profile = Profile::findOrFail(Auth::id());

            // ddd($user);
            // ddd($user->referer);
        return view('account.withdrawal', compact('user'));

    }
       //
    public function withdrawalStore(Request $request){

        $validator = Validator::make($request->all(),
        [
            'account_id' => "required|numeric",
            "sum" => 'required|integer',
            "rate" => 'required',
            "withdrawal_method" => 'required|in:qiwi,yandex_money'
        ]);
        // ddd($request->all());
        $user = Auth::user();
        $user->load('account');
        // dd($user->account);
        // dd((int)$user->account->balance, (int)$request->sum);
        if ((int)$user->account->balance >= (int)$request->sum) {
            $user->account->balance = $user->account->balance - $request->sum;
            $user->account->save();
            $withdrawal = new Withdrawal();
            $withdrawal->user_id = $user->id;
            $withdrawal->sum = $request->sum;
            $withdrawal->wallet = $request->withdrawal_method;
            $withdrawal->rate = $request->rate;
            $withdrawal->save();
        }else{

            return redirect()->back()->with('error', "Pul chiqarish uchun yetarli mablag' mavjud emas");
        }

        return redirect()->back()->with('success', 'Pul chiqarish zayavkangiz qabul qilindi!');

    }
       //
    public function updateWithdrawal(Withdrawal $withdrawal, Request $request){

        $validator = Validator::make($request->all(),
        [
            'status' => "required|in:created,paid,cancelled",
        ]);



        if ($validator->fails()) {
            return response()->json(['error' => "Pul chiqarish uchun yetarli mablag' mavjud emas"]);
        }else{
            $withdrawal->status = $request->status;
            $withdrawal->save();
        }

        return response()->json(['success' => 'Zayavka holati yangilandi!']);

    }

    public function editPayment(Payment $payment, Request $request)
    {
        dd($request->all());

        return view('admin.pages.payment', compact('payment'));
    }
    public function updatePayment(Payment $payment, Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'status' => "required|in:processing,paid,unpaid",
        ]);



        if ($validator->fails()) {
            return response()->json(['error' => "Pul kiritish holati o'zgartirilmadi!"]);
        }else{
            $payment->status = $request->status;
            $payment->save();

            if ($request->status == 'paid') {
                $payment->load('user.account');
                $payment->user->account->balance += $payment->sum;
                $payment->user->account->save();
            }
        }

        return response()->json(['success' => 'Zayavka holati yangilandi!']);
    }


    public function fillup($payment, Request $request){

        // ddd($payment, $request->all());
        // ddd($request->all());
        // dd($payment->isEmpty());
        $payment = Payment::find($payment);
        // dd($payment);

        if (!$payment->isEmpty()) {
            // ddd($payment->sum);
            $amount = number_format($request->sum / $request->rate, 2); // Payment`s amount
            switch ($request->payment_method) {
                case 'click':

                    break;

                default:
                    $url = FreeKassa::getPayUrl($amount, $payment->id);

                    $redirect = FreeKassa::redirectToPayUrl($amount, $payment->id);
                    return redirect($url);
                    break;
            }

        }else{
			// ddd(2);
            $request->validate([
                'sum' => 'numeric',
                'rate' => 'numeric'
            ]);
        }


        $user = Auth::user();

        $payment = new Payment();
        $payment->user_id = Auth::id();
        $payment->type = 'account';
        $payment->rate = $request->rate;
        $payment->sum = $request->sum;
        $payment->hint = $request->sum. " so'm ". Auth::user()->username ." hisobiga qabul qilindi .";

        $payment->save();

        $amount = number_format($request->sum / $request->rate, 2); // Payment`s amount

        $url = FreeKassa::getPayUrl($amount, $payment->id);



        $redirect = FreeKassa::redirectToPayUrl($amount, $payment->id);
        return redirect($url);


        // ddd($user);
        return redirect('/cabinet')->with('success', "To'lov omadli oshirildi!");

    }

    public function storePayment(Request $request){

        // dd($rate);
        // $rate = file_get_contents();
        $rate = file_get_contents( 'storage/currency/' . date("Y-m-d", time()) . '.json');
        // dd($request->all());
        $rate = json_decode($rate, 1);
        // dd($rate);
        $user = Auth::user();
        // dd($user);
        $payment = new Payment();
        $payment->user_id = Auth::id();
        $payment->type = $request->payment_method;
        $payment->rate = $request->sum == $rate['Rate'] ? $request->sum : $rate['Rate'];
        $payment->public_id = Str::uuid();
        $payment->sum = $request->sum;
        $payment->hint = $request->sum. " so'm ". Auth::user()->username ." hisobiga qabul qilindi .";

        $payment->save();
        $payment->refresh();

        $amount = number_format($request->sum / $request->rate, 2); // Payment`s amount

        if ($request->payment_method == 'freekassa') {
            $url = FreeKassa::getPayUrl($amount, $payment->public_id);

        }else{
            $url = URL::to('/') . "/payment/" . $payment->public_id . "?status=waiting";
        }


        // dd($url);
        // $redirect = FreeKassa::redirectToPayUrl($amount, $payment->id);
        return redirect($url);
    }

    // make_payment blade view
    public function payBill(Request $request){


        // ddd(Carbon::now()->toDateTimeString());
        // ddd($request->all());
        $request->validate([
            'payment_method' => 'string',
            'tariff_id' => 'numeric',
            'sum' => 'numeric',
            'rate' => 'numeric'
        ]);

        $user = Auth::user();

        if ($request->payment_method === 'account') {
            # code...
            $tariff = Tariff::where('id', $request->tariff_id)->first();
            // dd($tariff);
            if ($tariff->type === 'piece') {
                $user->account->quantity += $tariff->quantity;
            }else{
                $user->account->expire_date = Carbon::now()->addDays($tariff->term)->toDateTimeString();
            }
            if ($user->account->balance < $tariff->price - ($tariff->price / 100 * $tariff->discount) ) {
                return redirect(route('addup'))->with('error', 'Hisobingizda yetarli mablag` mavjud emas!' );
            }
            $user->account->balance -= $tariff->price - ($tariff->price / 100 * $tariff->discount);
            $user->account->save();


            $payment = new Payment();
            $payment->user_id = Auth::id();
            $payment->user_id = Auth::id();
            $payment->type = 'account';
            $payment->status = 'paid';
            $payment->rate = $request->rate;
            $payment->sum = $tariff->price - ($tariff->price / 100 * $tariff->discount);
            $payment->hint = $tariff->price - ($tariff->price / 100 * $tariff->discount). " so'm ". Auth::user()->username ." hisobidan yechib to'lov qilindi.";

            $payment->save();
            $payment->refresh();

            $purchase = new Purchase();
            $purchase->user_id = Auth::id();
            $purchase->payment_id = $payment->id;

            $purchase->tariff_id = $tariff->id;
            if ($tariff->type == 'piece') {
                $purchase->quantity = $tariff->quantity;

            }else{

                if ($user->account->expire_date > Carbon::now()) {
                    $user->account->expire_date = Carbon::parse($user->account->expire_date)->addDays($tariff->term)->toDateTimeString();
                }else{
                    $user->account->expire_date = Carbon::now()->addDays($tariff->term)->toDateTimeString();
                }
            }

            $purchase->payment_method = $request->payment_method;

            $purchase->save();
            $purchase->refresh();
            $payment->purchase_id = $purchase->id;
            $payment->save();

            return redirect('/forecasts/paid')->with('success', "To'lov qabul qilindi, tariffga mos prognozlardan foydalanish mumkin.");
            // ddd($request->all());

        }




                // ddd($user->account);
        // $user->profile = Profile::findOrFail(Auth::id());
        // ddd($user->payments->type = 'freekassa');
        // ddd($_ENV['AFFILIATE_PERCENT']);

        // ddd($fillup);

        $payment = new Payment();
        $payment->user_id = Auth::id();
        $payment->type = 'purchase';
        $payment->rate = $request->rate;
        $payment->sum = $request->sum;
        $payment->hint = $request->sum. " so'm ". Auth::user()->username ." hisobiga qabul qilindi .";

        $payment->save();

        // ddd($payment);

        $tariff = Tariff::findOrFail($request->tariff_id);

        // dd($tariff);

        $purchase = new Purchase();
        $purchase->user_id = Auth::id();
        $purchase->payment_id = $payment->id;
        $purchase->tariff_id = $tariff->id;
        if ($tariff->type == 'piece') {
            $purchase->quantity = $tariff->quantity;

        }else{

            if ($user->account->expire_date > Carbon::now()) {
                $user->account->expire_date = Carbon::parse($user->account->expire_date)->addDays($tariff->term)->toDateTimeString();
            }else{
                $user->account->expire_date = Carbon::now()->addDays($tariff->term)->toDateTimeString();
            }
        }

        $purchase->payment_method = $request->payment_method;

        $purchase->save();


        $payment->purchase_id = $purchase->id;
        $payment->save();

        // ddd($purchase);
        // ddd($user);

        return redirect('/checkpayment/' . $purchase->id)->with('success', "To'lov amalga oshirilgandan keyin avtomatik tarzda aniqlanadi!");

    }

    public function checkPayment(Purchase $purchase)
    {
        // ddd($purchase);
        if ($purchase != null && $purchase->user_id === Auth::id()) {
            $purchase->load('payment');

            if ($purchase->payment != null) {
                if ($purchase->payment->status === 'paid') {
                    return redirect('/cabinet/forecasts')->with('success', "To'lov qabul qilindi, tariffga mos prognozlardan foydalanishiz mumkin.");

                } else {
                    $payment = $purchase->payment;
                    // dd($payment);
                    return view('account.check_payment', compact('purchase', 'payment'));
                }

            }else{
                return view('account.check_payment', compact('purchase'));
            }
        }else{
            abort(401);
        }


    }

    public function payments(){

        $user = Auth::user();
        $user->load(['payments', 'withdrawals']);
        // ddd($user->payments, $user->withdrawals);
        // ddd($user->payments);
        // $user->profile = Profile::findOrFail(Auth::id());

        // ddd($user);
        return view('account.payments', compact('user'));


    }
}

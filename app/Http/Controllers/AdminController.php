<?php

namespace App\Http\Controllers;

use Acadea\CollectionPaginator\CollectionPaginator;
use App\Models\Contact;
use App\Models\Forecast;
use App\Models\ForecastResult;
use App\Models\Payment;
use App\Models\Purchase;
use App\Models\Tariff;
use App\Models\Theory;
use App\Models\User;
use App\Models\Widget;
use App\Models\Withdrawal;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Validation\Validator;
use Intervention\Image\Facades\Image;
use PhpParser\ErrorHandler\Collecting;

class AdminController extends Controller
{
    //index

    // admin panel - index
    public function index()
    {


        $purchases = Payment::select(
            DB::raw('sum(`sum`) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
        )
        ->where('status', 'paid')
        ->whereHas('purchase')
        ->groupBy('monthKey')
        ->orderBy('created_at', 'ASC')
        ->get();

        // dd($purchases);
        $months = ['Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'Iyun', 'Iyul', 'Avgust', 'Sentabr', 'Oktabr', 'Noyabr', 'Dekabr'];
        $incomeDetails = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($purchases as $item){
            $incomeDetails[(int)$item->monthKey-1] = $item->sums;
        }

        $withdrawals = Withdrawal::select(
            DB::raw('sum(`sum`) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
        )
        ->where('status', 'paid')
        ->groupBy('monthKey')
        ->orderBy('created_at', 'ASC')
        ->get();

        // dd($purchases);
        $months = ['Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'Iyun', 'Iyul', 'Avgust', 'Sentabr', 'Oktabr', 'Noyabr', 'Dekabr'];
        $withdrawalsDetails = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($withdrawals as $item){
            $withdrawalsDetails[(int)$item->monthKey-1] = $item->sums;
        }
        // ddd($withdrawalsDetails);

        // $incomeDetails = Payment::select(
        //     DB::raw('sum(`sum`) as sums'),
        //     DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
        //     DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
        // )
        // ->groupBy('months', 'monthKey')
        // ->orderBy('created_at', 'ASC')
        // ->get();
        // $incomeDetails = $purchases->pluck('sums');

        $users = DB::table('users')->take(5)->get();
        // ddd($users);
        // $users = DB::table('users')
        //     ->select(DB::raw('DATE(created_at) as month'), DB::raw('count(*) as count'))
        //     ->groupBy('month')
        //     ->get();



        $allDetails = new Collection();
        $allDetails->theories = Theory::count();
        $allDetails->forecasts = Forecast::count();
        $allDetails->finished_forecasts = Forecast::where('is_ended', 'yes')->count();
        $allDetails->messages = Contact::count();
        // dd($allDetails->theories);

        // dd(User::all());
        $users = User::select(
                DB::raw('count(*) as counts'),
                DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
            )
            ->where('role', 'user')
            ->groupBy('monthKey')
            ->orderBy('created_at', 'ASC')
            ->get();

        $usersDetails = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($users as $item){
            $usersDetails[(int)$item->monthKey-1] = $item->counts;
        }
        // dd($users, $usersDetails);


        $theories = Theory::select(
                DB::raw('count(*) as counts'),
                DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
            )
            ->groupBy('monthKey')
            ->orderBy('created_at', 'ASC')
            ->get();

            // dd($purchases);
        $theoriesDetails = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($theories as $item){
            $theoriesDetails[(int)$item->monthKey-1] = $item->counts;
        }

        $forecasts = Forecast::select(
                DB::raw('count(*) as counts'),
                DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
            )
            ->groupBy('monthKey')
            ->orderBy('created_at', 'ASC')
            ->get();

            // dd($purchases);
        $forecastsDetails = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($forecasts as $item){
            $forecastsDetails[(int)$item->monthKey-1] = $item->counts;
        }


        $messages = Contact::select(
                DB::raw('count(*) as counts'),
                DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
            )
            ->groupBy('monthKey')
            ->orderBy('created_at', 'ASC')
            ->get();

            // dd($purchases);
        $messagesDetails = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach($messages as $item){
            $messagesDetails[(int)$item->monthKey-1] = $item->counts;
        }

        $users->counts = array_sum($usersDetails);
        return view('admin.dashboard', [
            'allDetails' => $allDetails,
            'users' => $users,
            'usersDetails' =>  json_encode($usersDetails),
            'theoriesDetails' => json_encode($theoriesDetails),
            'forecastsDetails' => json_encode($forecastsDetails),
            'messagesDetails' => json_encode($messagesDetails),
            'incomeDetails' => json_encode($incomeDetails),
            'withdrawalsDetails' => json_encode($withdrawalsDetails),
            'months' => json_encode( $months),
        ]);
    }
    // admin panel - users
    public function users()
    {
        $users = User::with('profile')->orderBy('id', 'DESC')->paginate(50);

        return view('admin.pages.users', ['users' => $users]);
    }

    public function deleteUser(User $user, Request $request)
    {
        // print_r($request->all());
        
        if ($user) {
            $user->delete();
            return response()->json(['status' => 'success', 'content' => "Foydalanuvchi o'chirildi"]);
        }else{
            if ($widget->save()) {
                return response()->json(['status' => 'error', 'content' => "Foydalanuvchi o'chirilmadi"]);
            }
        }
        
    }


    public function allMessages()
    {
        // ddd(1);
        $messages = Contact::paginate(50);
        return view('admin.pages.messages', compact('messages'));
    }
    public function allWidgets()
    {

        // ddd(1);
        $widgets = new Collection();
        $widgets->home = new Collection();
        $widgets->home->sidebar = Widget::where('page', 'home')->where('type', 'sidebar')->get();
        $widgets->home->content = Widget::where('page', 'home')->where('type', 'content')->get();
        $widgets->stats = new Collection();
        $widgets->stats->sidebar = Widget::where('page', 'stats')->where('type', 'sidebar')->get();
        $widgets->stats->content = Widget::where('page', 'stats')->where('type', 'content')->get();
        $widgets->tariffs = new Collection();
        $widgets->tariffs->sidebar = Widget::where('page', 'tariffs')->where('type', 'sidebar')->get();
        $widgets->tariffs->content = Widget::where('page', 'tariffs')->where('type', 'content')->get();
        $widgets->forecasts = new Collection();
        $widgets->forecasts->sidebar = Widget::where('page', 'forecast')->where('type', 'sidebar')->get();
        $widgets->forecasts->content = Widget::where('page', 'forecast')->where('type', 'content')->get();
        $widgets->theories = new Collection();
        $widgets->theories->sidebar = Widget::where('page', 'theories')->where('type', 'sidebar')->get();
        $widgets->theories->content = Widget::where('page', 'theories')->where('type', 'content')->get();
        $widgets->strategies = new Collection();
        $widgets->strategies->sidebar = Widget::where('page', 'strategies')->where('type', 'sidebar')->get();
        $widgets->strategies->content = Widget::where('page', 'strategies')->where('type', 'content')->get();
        // ddd($widgets);

        return view('admin.pages.widgets', compact('widgets'));
    }

    public function updateWidget(Widget $widget, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required|string',
            'button' => 'required|string',
            'status' => 'required|in:active,inactive',
            'status' => 'required|in:active,inactive',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'content' => "Ma'lumotlar to'g'ri emas",  'errors'=>$validator->errors()->all()]);
        }

        $widget->url = $request->url;
        $widget->content = $request->content;
        $widget->button = $request->button;
        $widget->status = $request->status;

        if ($widget->save()) {
            return response()->json(['status' => 'success', 'content' => "Widget yangilandi"]);
        }
    }

}

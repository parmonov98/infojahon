<?php

namespace App\Http\Controllers;

use Acadea\CollectionPaginator\CollectionPaginator;
use App\Models\Contact;
use App\Rules\PhoneNumber;
use Illuminate\Http\Request;
use App\Http\Requests\StoreContactMessage;
use App\Mail\UserContactMail;
use App\Models\Forecast;
use App\Models\Payment;
use App\Models\purchasedItem;
use App\Models\Tariff;
use App\Models\Theory;
use App\Models\Widget;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{

    public function index()
    {
        $forecasts = new Collection();
        $theories = new Collection();
        $forecasts->free = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->take(5)
                            ->get();
        $forecasts->paid = Forecast::where('type', 'paid')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->take(5)
                            ->get();

        $theories->paid = Theory::where('type', 'paid')->where('status', 'published')->orderBy('id', 'DESC')->take(5)->get();
        $theories->free = Theory::where('type', 'free')->where('status', 'published')->orderBy('id', 'DESC')->take(5)->get();

        $min = $theories->free->min('count_views');
        $max = $theories->free->max('count_views');
        $theories->free->each(function ($item) use ($min, $max)
        {
            if ($item->count_views != 0) {
                $item->rating = $item->count_views / ($max / 5);
            }else{
                $item->rating = 0;
            }
        });

        $min = $theories->paid->min('count_views');
        $max = $theories->paid->max('count_views');
        $theories->paid->each(function ($item) use ($min, $max)
        {
            if ($item->count_views != 0) {
                $item->rating = $item->count_views / ($max / 5);
            }else{
                $item->rating = 0;
            }
        });


        $theories->beginners = Theory::where('type', 'free')->where('status', 'published')->where('for_beginners', 'yes')->orderBy('id')->get();

        $widgets = new Collection();
        $widgets->content = Widget::where('page', 'home')->where('type', 'content')->where('status', 'active')->get();

        // dd($widgets->content);

        // dd($forecasts->free);

        return view('pages.index')->with(['forecasts' => $forecasts, 'theories' => $theories, 'widgets' => $widgets]);
    }

    //
    public function contact(){

        return view('pages.contact');
    }
    //
    public function stats(){

        $stats = new Collection();
        // Carbon
        $last10Foreacsts = Forecast::has('result')->where('is_ended', 'yes')->take(10)->get();

        $stats->todays = Forecast::has('result')->where('is_ended', 'yes')->where('end', '>=', Carbon::today())->get();
        $stats->all = Forecast::has('result')->where('is_ended', 'yes')->get();

        $last10Foreacsts->each(function ($item)
        {
            $item->setResult();
        });
        $stats->all->each(function ($item)
        {
            $item->setResult();
        });


        $statDetails['all'] = 0;
        $statDetails['wins'] = 0;
        $statDetails['losses'] = 0;
        $statDetails['ties'] = 0;

        $statDetails['todays_all'] = 0;
        $statDetails['todays_wins'] = 0;
        $statDetails['todays_losses'] = 0;


        // ddd($stats->all);

        foreach ($stats->all as $key => $item) {
            $statDetails['all'] = $statDetails['all'] + 1;
            if ($item->result && $item->result->result === 'win') {
                $statDetails['wins'] = $statDetails['wins'] + 1;
            }elseif($item->result && $item->result->result === 'return'){
                $statDetails['return'] = $statDetails['return'] + 1;
            }else{
                $statDetails['losses'] = $statDetails['losses'] + 1;
            }
        }

        foreach ($stats->todays as $key => $item) {
            $statDetails['todays_all'] = $statDetails['todays_all'] + 1;
            if ($item->result && $item->result->result === 'win') {
                $statDetails['todays_wins'] = $statDetails['todays_wins'] + 1;
            }else{
                $statDetails['todays_losses'] = $statDetails['todays_losses'] + 1;
            }
        }



        if ($statDetails['todays_all'] != 0 && $statDetails['todays_wins'] != 0) {
            $statDetails['today_bets_win_percentage'] = floor(100 / $statDetails['todays_all']  * ($statDetails['todays_wins'] != 0 ? $statDetails['todays_wins'] : 1) );
        }else{
            $statDetails['today_bets_win_percentage'] = 0;
        }

        // ddd($last10Foreacsts);
        // ddd($statDetails, $last10Foreacsts, $stats);
        // ddd($stats);
        // ddd($statDetails);
        // ddd($last10Foreacsts);

        if ($statDetails['all'] != 0 && $statDetails['wins'] != 0) {
            $statDetails['all_bets_win_percentage'] = 100 / $statDetails['all']  * $statDetails['wins'];
        }else{
            $statDetails['all_bets_win_percentage'] = 0;
        }



        $widgets = new Collection();
        $widgets->content = Widget::where('type', 'content')->where('page', 'stats')->where('status', 'active')->get();

        $timerForecasts = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->get();
        // dd($widgets);

        return view('pages.stats', compact('widgets', 'timerForecasts'))->with(['statDetails' => $statDetails, 'bets' => $last10Foreacsts]);
    }





    public function tariffs() // type of forecasts page.
    {



        $forecasts = Forecast::where('created_at', '>=', Carbon::now()->subDays(30))
                             ->where('is_ended', '=', 'yes')
                             ->whereHas('result')
                             ->where('status', '=', 'published')->get();

        $lastWeek = Forecast::where('created_at', '>=', Carbon::now()->subDays(7))
                            ->where('is_ended', '=', 'yes')
                            ->whereHas('result')
                            ->where('status', '=', 'published')->get();

        // ddd($lastWeek);
        $statDetails['last_weeks_all'] = 0;
        $statDetails['last_weeks_wins'] = 0;
        $statDetails['last_weeks_losses'] = 0;


        if ($lastWeek) {
            # code...
            foreach ($lastWeek as $key => $item) {
                $statDetails['last_weeks_all'] = $statDetails['last_weeks_all'] + 1;
                if ($item->result && $item->result->result === 'win') {
                    $statDetails['last_weeks_wins'] = $statDetails['last_weeks_wins'] + 1;
                }else{
                    $statDetails['last_weeks_losses'] = $statDetails['last_weeks_losses'] + 1;
                }
            }
            if ($statDetails['last_weeks_wins'] != 0) {
                $statDetails['last_weeks_wins_percentage'] = (100 / $statDetails['last_weeks_all']) * $statDetails['last_weeks_wins'];
            }
        }

        // ddd($statDetails);

		// ddd($forecasts);


        $tariffs = new Collection();


        $tariffs->terms = Tariff::where('type', 'term')
                            ->orderBy('price', 'asc')->get();

        $tariffs->pieces = Tariff::where('type', 'piece')
                            ->orderBy('price', 'asc')->get();

        $timerForecasts = Forecast::where('type', 'free')
        ->where('is_ended', 'no')
        ->where('status', 'published')
        ->orderBy('id', 'DESC')
        ->get();
        // ddd($statDetails);
        // ddd($tariffs);
        return view('pages.tariffs', compact('tariffs', 'forecasts', 'timerForecasts', 'statDetails'));
    }


    public function makePayment($tariff_id)
    {
        // ddd($tariff_id);


        $tariffs = Tariff::all();

        // ddd($tariffs);

        $tariffs->current = Tariff::findOrFail($tariff_id);

        $tariffs->current->setTariffLabel();
        // ddd($tariff);

        return view('account.make_payment', compact('tariffs'));
    }


    public function checkPayment($public_id, Request $request){


        // dd($request->all());

        $payment = Payment::where('public_id', $public_id)->first();
        if ($payment) {
            if ($request->status === 'transferred') {

                if ($payment->status === 'processing') {

                    $payment->status = 'confirmed';

                    $payment->save();

                    $payment->refresh();

                    $url = env('APP_URL', 'https://infojahon.ru') . '/admin/payments/?q=' . $payment->public_id;

                    $http = Http::post( env('APP_URL', 'https://infojahon.ru') . '/bot/receive',
                    [
                        'message' =>  $payment->user->email . " (". $payment->user->username   . ") hisobiga " . $payment->sum . " so'm pul kiritish zayavkasini tekshirib to'lovni tasdiqlang! \nURL: $url",
                        "type" => "Pul kiritish uchun zayavka keldi!"
                    ]);
                }

                if ($payment->status == 'paid') {
                    $payment->status = 'paid';
                    $payment->save();
                    $payment->refresh();
                    return Redirect::to('/cabinet')->with('success', "Hisobingizga pul to'ldirilgan.");
                }

                // dd($payment);
                // if ($payment->status == 'unpaid') {
                //     return Redirect::back()->with('alert', "Hisobingizga pul to'ldirilmagan va bekor qilingan.");
                // }



            }

        }else{
            abort(404, 'Ushbu sahifa topilmadi!');
        }


        // $url = FreeKassa::getPayUrl($amount, $payment->id);


        // $url = URL::to('/') . "/payment/" . $payment->public_id . "?status=waiting";
        // dd($url);
        // $redirect = FreeKassa::redirectToPayUrl($amount, $payment->id);
        return view('account.check_payment_manually', compact('payment'))->with('success', 'Pul kiritish uchun zayavka qabul qilindi.');
    }


    public function free() // type of forecasts page.
    {
        $forecasts = Forecast::where('type', 'free')
                             ->where('status', 'published')
                             ->where('is_ended', 'no')
                             ->orderBy('updated_at', 'desc')->simplePaginate(env('MIN_FORECAST_ITEMS'));
        // ddd($forecasts);
        $widgets = new Collection();
        $widgets->content = Widget::where('type', 'content')->where('page', 'forecast')->where('status', 'active')->get();

        // CollectionPaginator::paginate();
        $timerForecasts = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('forecasts.free', compact('forecasts', 'timerForecasts', 'widgets'));
    }

    public function history() // type of forecasts page.
    {
        $forecasts = Forecast::where('status', 'published')
                             ->where('is_ended', 'yes')
                             ->orderBy('updated_at', 'desc')->paginate(env('MIN_FORECAST_ITEMS'));
        // ddd($forecasts);
        $widgets = new Collection();
        $widgets->content = Widget::where('type', 'content')->where('page', 'forecast')->where('status', 'active')->get();

        // CollectionPaginator::paginate();

        return view('forecasts.history', compact('forecasts', 'widgets'));
    }


    public function paid() // type of forecasts page in cabinet.
    {

        if (Auth::check()) {

            $forecasts = Forecast::where('type', 'paid')
                                ->where('status', 'published')
                                ->where('is_ended', 'no')
                                ->orderBy('updated_at', 'desc')->get();
                                // simplePaginate(2)
                                // ddd($forecasts);
            if ($forecasts->count() > 1) {
                $items = $forecasts;

                $forecasts = CollectionPaginator::paginate($items, 5);
                // ddd($forecasts);
            }elseif($forecasts->count() !== 1){

                $forecasts = null;
            }
        } else {
            return redirect('/login')->with('error', 'Oldin tizimga kiring!');
        }

        $timerForecasts = Forecast::where('type', 'free')
                            ->where('is_ended', 'no')
                            ->where('status', 'published')
                            ->orderBy('id', 'DESC')
                            ->get();
        // dd($forecasts);
        return view('forecasts.paid', compact('forecasts', 'timerForecasts'));
    }
    public function purchased() // purchased forecasts page.
    {

        if (Auth::user()) {


            // $ids = explode('|', Auth::user()->account->purchased_forecasts);

            $items = purchasedItem::with('forecast')->where('user_id', Auth::id())->get();
            // dd($items);
            // unset($ids[0]); // bump off whitespace from ids

            // ddd($ids[1]);
            $forecasts = new Collection();
            $items->each(function ($item) use ($forecasts)
            {
                $forecasts[] = $item->forecast;
            });

            // foreach ($ids as $id) {
            // }
            // ddd($forecasts);

            // ddd($forecasts);
            // echo 'ok'; die;
            return view('account.purchased', compact('forecasts'));
        }else{
            abort('403', "Akkauntga kirib keyin urinib ko'ring");
            // return redirect('404')
        }


    }


    public function storeMessage(StoreContactMessage $request){
        // $this->validate($request, [
        //     'name' => ['required', 'string'],
        //     'email' => ['required', 'email'],
        //     'phone' => ['required', new PhoneNumber],
        //     'message' => ['required', 'string'],
        // ]);
        $validated = $request->validated();

        // ddd($validated);
        // ddd($request->all());

        // $message = new Contact();

        // $message->name = $validated['name'];
        // $message->email = $validated['email'];
        // $message->phone = $validated['phone'];
        // $message->message = $validated['message'];
        // $message->save();
        // $message->refresh();
        // MAIL_FROM_ADDRESS
        // Mail::to(env('MAIL_FROM_ADDRESS'))->send(new UserContactMail($message));

        $http = Http::post( env('APP_URL', 'https://infojahon.ru') . '/bot/receive',
        [
            'message' =>  "\n Ismi: " . $validated['name'] .
                          ",\nemail: " . $validated['email'] .
                          ",\nraqam: "  . $validated['phone'] .
                          ",\nxabar: " . $validated['message'],
            "type" => " Saytdan xabar!"
        ]);



        return redirect('/contact')->with('success', "Xabaringizni qabul qildim, siz bilan o'zim aloqaga chiqaman!");
    }


    public function showMessage()
    {
        return 1111;
    }

}

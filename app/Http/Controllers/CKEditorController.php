<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CKEditorController extends Controller
{
    public function upload(Request $request)
    {
        // print_r($request->all());

        if ($request->hasAny('upload')) {
            // echo 111; die;
            // try {
            //     $mime = $request->upload->getMimeType();
            //   } catch (\Exception $e) {
            //     $mime = $request->upload->getClientMimeType();
            // }
            // print_r($mime); die;

            $file_path = Storage::disk('public')->put('uploads', $request->upload);
            // print_r($img_path); die;
            $fileName = explode('/', $file_path)[1];
            $url = asset('storage/uploads/'.$fileName);
            $response = [
                "uploaded" => 1,
                'url' => $url,
                'fileName' => $fileName
            ];
            // dd(file_exists($img2));
            return response()->json($response);

        }else{
            $response = [
                'error' => '1',
                'content' => 'no file submitted'
            ];
            return response()->json($response);
        }
    }
    public function uploadImage(Request $request)
    {
        // print_r($request->all());

        if ($request->hasAny('upload')) {
            // echo 111; die;
            // try {
            //     $mime = $request->upload->getMimeType();
            //   } catch (\Exception $e) {
            //     $mime = $request->upload->getClientMimeType();
            // }
            // print_r($mime); die;

            $file_path = Storage::disk('public')->put('uploads', $request->upload);
            // print_r($img_path); die;
            $fileName = explode('/', $file_path)[1];
            $url = asset('storage/uploads/'.$fileName);
            $response = [
                "uploaded" => 1,
                'url' => $url,
                'fileName' => $fileName
            ];
            // dd(file_exists($img2));
            return response()->json($response);

        }else{
            $response = [
                'error' => '1',
                'content' => 'no file submitted'
            ];
            return response()->json($response);
        }
    }
}

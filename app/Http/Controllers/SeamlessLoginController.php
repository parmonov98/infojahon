<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class SeamlessLoginController extends Controller
{
	//

	public function index(Request $request)
	{


		// ddd($request->all());
		if (Auth::check()) {
			return redirect('/cabinet')->with('Siz saytga kirgansiz oldin!');
		} else {


			if (! function_exists('verifyTelegramLogin')) {
				abort('helper not found');
			}
			$login = verifyTelegramLogin($request->all());
			// ddd($login);

			if ($login['error'] === null) {

				$user = User::where('tg_id', '=', $request->query('id'))->first();


				if ($user === null) {
					// user doesn't exist

                    $data = [
						'email' => '@',
                        'name' => $request->query('first_name') ? $request->query('first_name') : $request->query('last_name'),
                        'username' => $request->has('username') ? $request->query('username') : $request->query('id'),
                        'tg_id' => $request->query('id'),
                        'referer' => Cookie::get('referer'),
						'type' => 'telegram',
						'password' => Hash::make($request->query('id')),
						'email_verified_at' => Carbon::now(),
						'hash' => $request->query('hash'),
						'token' => Str::random(32)
                    ];
                    // dd($data);
					$user =  User::create($data);
                    $profile = new Profile();

                    $profile->create([
                        'user_id' => $user->id,
                        'first_name' => $request->query('first_name') ? $request->query('first_name') : $request->query('last_name'),
                        'last_name' => $request->query('last_name') ? $request->query('last_name') : $request->query('first_name')
                    ]);
					//    ddd($userData);
                    // dd(111);
					if($this->authenticate($user, $request)){
						return redirect('/profile');
					}else{
						return redirect('/login')->with('error', __('dynamics.login_page_redirected') );
						// return redirect('/login')->with('error', "Kirishda xatolik yuz berdi: " . $request->query('username') . " saytda ro'yxatdan o'tkazilgan!");
					}
				} else {

					$this->authenticate($user, $request);

					return redirect('/login')->with('error', "Bu nik nom: " . $request->query('username') . " saytda ro'yxatdan o'tkazilgan!");
				}
			} else {
				return redirect('/login')->with("Qaytadan urinib ko'ring, iltimos!");
			}
		}
		// return view('auth.telegram');
	}

	public function authenticate($user, $request)
	{
		// $request = new Request;
				$credentials = $request->only('id', 'hash');

				// ddd($credentials);
				$user->hash = $credentials['hash'];
				// $user->hash = $credentials['hash'];
				// ddd($user);
			if ($user->save()) {
						// ddd('000');
				return Auth::login($user, true);
			}else {
				return false;
			}
	}
}

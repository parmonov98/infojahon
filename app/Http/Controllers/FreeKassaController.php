<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Maksa988\FreeKassa\Facades\FreeKassa;

class FreeKassaController extends Controller
{
     /**
     * Search the order in your database and return that order
     * to paidOrder, if status of your order is 'paid'
     *
     * @param Request $request
     * @param $order_id
     * @return bool|mixed
     */
    public function searchOrder(Request $request, $order_id)
    {

        $order = Order::where('id', $order_id)->first();


        if($order) {
            $order['_orderSum'] = $order->sum;

            // If your field can be `paid` you can set them like string
            $order['_orderStatus'] = $order['status'];

            // Else your field doesn` has value like 'paid', you can change this value
            $order['_orderStatus'] = ('1' == $order['status']) ? 'paid' : false;

            return $order;
        }

        return false;
    }

    /**
     * When paymnet is check, you can paid your order
     *
     * @param Request $request
     * @param $order
     * @return bool
     */
    public function paidOrder(Request $request)
    {


        $payment = Payment::firstWhere('id', $request->query('MERCHANT_ORDER_ID'));
        // dd($payment);

        // ddd($payment, $payment->status);

		if($payment !== null){

			if($payment->status === 'paid' && $payment->order_id == $request->query('intid')){
				$user = $payment->user;

				$referer = User::firstWhere('username', $user->referer);

				// ddd($referer);

				if ($referer !== null) {
					// ddd($url, $redirect);
					if (Payment::firstWhere('purchase_id', $payment->purchase_id)->where('type', 'affiliate')->first() === null) {
						$fillup = env('AFFILIATE_PERCENT', 10) * $payment->sum / 100;
						// ddd($referer->payment);
						$referer->account->balance = $referer->account->balance + $fillup;
						$referer->account->save();
						$payment2 = new Payment();
						$payment2->user_id = $referer->id;
						$payment2->purchase_id = $payment->purchase_id;
						$payment2->type = 'affiliate';
						$payment2->rate = 0;
						$payment2->status = 'paid';
						$payment2->sum = $fillup;
						$payment2->hint = $fillup . " so'm ". $referer->username ." hisobiga ". $user->username ." to'lov amalga oshirgan summadan " . env('AFFILIATE_PERCENT', 10) . "% i qabul qilindi .";

						$payment2->save();
					}else if($payment->type == 'account'){

						$payment->user->account->balance = $payment->sum;
						$payment->user->account->save();

						$fillup = env('AFFILIATE_PERCENT', 10) * $payment->sum / 100;
						// ddd($referer->payment);
						$referer->account->balance = $referer->account->balance + $fillup;
						$referer->account->save();

						$payment2 = new Payment();
						$payment2->user_id = $referer->id;
						$payment2->purchase_id = 0;
						$payment2->type = 'affiliate';
						$payment2->rate = 0;
						$payment2->status = 'paid';
						$payment2->sum = $fillup;
						$payment2->hint = $fillup . " so'm ". $referer->username ." hisobiga ". $user->username ." to'lov amalga oshirgan summadan " . env('AFFILIATE_PERCENT', 10) . "% i qabul qilindi .";

						$payment2->save();
					}


				}
				return Redirect::to('/cabinet')->with('success', "To'lov qabul qilindi. ");


            }else{
					return redirect(route("checkpayment", $payment->id))->with('alert', "To'lov amalga oshirilmagan!");
			}

        }else{
            return abort(response("To'lov amalga oshirilmagan!", 404));
        }

        // ddd($order);

    }
    public function unpaidOrder(Request $request)
    {

        // $order = Payment::findOrFail($request->get('MERCHANT_ORDER_ID'));


        // $order->status = 'unpaid';
        // $order->save();
        // ddd($order);

        return Redirect::to('/cabinet')->with('error', "To'lov amalga oshmadi, qaytadan urinib ko'ring.");
    }

    /**
     * Start handle process from route
     *
     * @param Request $request
     * @return mixed
     */

    public function handlePayment(Request $request)
    {
		// ddd($request->all());

        file_put_contents('response-payment-success.txt', json_encode($request->toArray()));
        if (FreeKassa::handle($request) == 'YES') {
        }else{
			file_put_contents('response-payment-fail.txt', json_encode($request->toArray()));
		}

		$payment = Payment::firstWhere('id', $request->query('MERCHANT_ORDER_ID'));

		$payment->order_id = $request->query('intid');
		$payment->status = 'paid';
		$payment->save();

		// var_dump($payment->user);
		if($payment->type === 'account'){
			$payment->user->account->balance = $payment->user->account->balance + $payment->sum;
			$payment->user->account->save();
		}
		if($payment->type === 'purchase'){
            if ($payment->purchase->expire_date !== null) {
                $unixtime = strtotime($payment->user->account->expire_date) + (time() - strtotime($payment->purchase->expire_date));
                $payment->user->account->expire_date = Carbon::createFromTimestamp($unixtime)->toDateTimeString();
            }else{
                $payment->user->account->quantity = $payment->user->account->quantity + $payment->purchase->quantity;
            }
			$payment->user->account->save();
		}

		// print_r($payment->attributes);
		// print_r($payment);
		echo 'ok';
    }
}

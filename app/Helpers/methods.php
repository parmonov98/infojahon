<?php

function ru2lat($str){
    $tr = array(
    "А"=>"a", "Б"=>"b", "В"=>"v", "Г"=>"g", "Д"=>"d",
    "Е"=>"e", "Ё"=>"yo", "Ж"=>"zh", "З"=>"z", "И"=>"i",
    "Й"=>"j", "К"=>"k", "Л"=>"l", "М"=>"m", "Н"=>"n",
    "О"=>"o", "П"=>"p", "Р"=>"r", "С"=>"s", "Т"=>"t",
    "У"=>"u", "Ф"=>"f", "Х"=>"kh", "Ц"=>"ts", "Ч"=>"ch",
    "Ш"=>"sh", "Щ"=>"sch", "Ъ"=>"", "Ы"=>"y", "Ь"=>"",
    "Э"=>"e", "Ю"=>"yu", "Я"=>"ya", "а"=>"a", "б"=>"b",
    "в"=>"v", "г"=>"g", "д"=>"d", "е"=>"e", "ё"=>"yo",
    "ж"=>"zh", "з"=>"z", "и"=>"i", "й"=>"j", "к"=>"k",
    "л"=>"l", "м"=>"m", "н"=>"n", "о"=>"o", "п"=>"p",
    "р"=>"r", "с"=>"s", "т"=>"t", "у"=>"u", "ф"=>"f",
    "х"=>"kh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", "щ"=>"sch",
    "ъ"=>"", "ы"=>"y", "ь"=>"", "э"=>"e", "ю"=>"yu",
    "я"=>"ya", " "=>"-", "."=>"", ","=>"", "/"=>"-",
    ":"=>"", ";"=>"","—"=>"", "–"=>"-"
    );

    return preg_replace('/[^a-z0-9]+/', '-', strtolower(strtr($str,$tr)));
}

function sortMonths ( $a, $b ) {
    $months = array( 'januari', 'februari', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december' );
    if ( array_search( $a, $months) == array_search( $b, $months) ) return 0;
    return array_search( $a, $months) > array_search( $b, $months) ? 1 : -1;
}


function verifyTelegramLogin($data)
{
    $token = "1360040119:AAFa0m4cQlsi9b2WpiEBRITE40uebkD8Ztg";
    $check_hash = $data['hash'];
    unset($data['hash']);
    $data_check_arr = [];
    foreach ($data as $key => $value) {
        $data_check_arr[] = $key . '=' . $value;
    }
    sort($data_check_arr);
    $data_check_string = implode("\n", $data_check_arr);
    $secret_key = hash('sha256', $token, true);
    $hash = hash_hmac('sha256', $data_check_string, $secret_key);
    // dd($hash, $check_hash);
    if (strcmp($hash, $check_hash) !== 0) {
        unset($data);
        $data['error'] = "Data is not from Telegram";
        return $data;
    }
    if ((time() - $data['auth_date']) > 86400) {
        unset($data);
        $data['error'] = "Data is Outdated";
        return $data;
    }
    $data['error'] = null;
    return $data;

}




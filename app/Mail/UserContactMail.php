<?php

namespace App\Mail;

use App\Models\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        // ddd($contact);
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // env('APP_NAME')
        // ->from([
        //     'address' => $this->contact->email,
        //     'name' => $this->contact->name
        // ])
        return $this->markdown('emails.users.contact')
                        ->subject("Saytdan yangi xabar!")
                        ->replyTo($this->contact->email)
                        ->with( [
                            'name' => $this->contact->name,
                            'email' => $this->contact->email,
                            'phone' => $this->contact->phone,
                            'text' => $this->contact->message,
                            'created_at' => $this->contact->created_at
                        ]);
    }
}

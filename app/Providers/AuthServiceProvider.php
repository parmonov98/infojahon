<?php

namespace App\Providers;

use App\Models\Forecast;
use App\Models\Widget;
use App\Policies\ForecastPolicy;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Request;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Forecast::class => ForecastPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();



        $forecasts = new Collection();
        $forecasts->free = Forecast::has('result')->where('type', 'free')->where('is_ended', 'yes')->get();
        $forecasts->paid = Forecast::has('result')->where('type', 'paid')->where('is_ended', 'yes')->get();
        // ddd($forecasts->free);

        $sidebarDetails['free']['all'] = 0;
        $sidebarDetails['free']['wins'] = 0;
        $sidebarDetails['free']['losses'] = 0;
        $sidebarDetails['free']['ties'] = 0;
        $sidebarDetails['free']['average_kf'] = 0;


        if ($forecasts->free->count() > 0) {
            # code...
            foreach ($forecasts->free as $key => $item) {
                // echo($item->kf);

                $sidebarDetails['free']['all'] = $sidebarDetails['free']['all'] + 1;
                $sidebarDetails['free']['average_kf'] = $sidebarDetails['free']['average_kf'] + $item->kf;
                if ($item->result && $item->result->result === 'win') {
                    $sidebarDetails['free']['wins'] = $sidebarDetails['free']['wins'] + 1;
                }elseif($item->result && $item->result->result === 'tie'){
                    $sidebarDetails['free']['ties'] = $sidebarDetails['free']['ties'] + 1;
                }else{
                    $sidebarDetails['free']['losses'] = $sidebarDetails['free']['losses'] + 1;
                }
            }
            $sidebarDetails['free']['average_kf'] = $sidebarDetails['free']['average_kf'] / ($forecasts->free->count() != 0 ? $forecasts->free->count() : 1);
            // dd($sidebarDetails['free']['average_kf']);

            if ($sidebarDetails['free']['losses'] !== 0) {
                $sidebarDetails['free']['losses_percentage'] = (100 / $sidebarDetails['free']['all']) *  $sidebarDetails['free']['losses'];
            }
            if ($sidebarDetails['free']['wins'] !== 0) {
                $sidebarDetails['free']['wins_percentage'] = (100 / $sidebarDetails['free']['all']) * $sidebarDetails['free']['wins'];
            }
            if ($sidebarDetails['free']['ties'] !== 0) {
                $sidebarDetails['free']['ties_percentage'] = (100 / $sidebarDetails['free']['all']) * $sidebarDetails['free']['ties'];
            }
        }

        // dd($forecasts->paid);

        // dd($sidebarDetails['free']);

        $sidebarDetails['paid']['all'] = 0;
        $sidebarDetails['paid']['wins'] = 0;
        $sidebarDetails['paid']['losses'] = 0;
        $sidebarDetails['paid']['ties'] = 0;
        $sidebarDetails['paid']['average_kf'] = 0;
        $sidebarDetails['paid']['dates'] = new Collection();
		// dd($forecasts->paid);
        if ($forecasts->paid->count() > 0) {
            # code...
            foreach ($forecasts->paid as $key => $item) {
                $sidebarDetails['paid']['all'] = $sidebarDetails['paid']['all'] + 1;
                $sidebarDetails['paid']['average_kf'] = $sidebarDetails['paid']['average_kf'] + $item->kf;
                if ($item->result && $item->result->result === 'win') {
                    $sidebarDetails['paid']['wins'] = $sidebarDetails['paid']['wins'] + 1;
                }elseif($item->result && $item->result->result === 'tie'){
                    $sidebarDetails['paid']['ties'] = $sidebarDetails['paid']['ties'] + 1;
                }else{
                    $sidebarDetails['paid']['losses'] = $sidebarDetails['paid']['losses'] + 1;
                }
            }

            $sidebarDetails['paid']['average_kf'] = $sidebarDetails['paid']['average_kf'] / ($forecasts->paid->count() != 0 ? $forecasts->paid->count() : 1);

            if ($sidebarDetails['paid']['losses'] !== 0) {
                $sidebarDetails['paid']['losses_percentage'] = $sidebarDetails['paid']['all'] * (100 / $sidebarDetails['paid']['losses']);
            }
            if ($sidebarDetails['paid']['wins'] !== 0) {
                $sidebarDetails['paid']['wins_percentage'] = $sidebarDetails['paid']['all'] * (100 / $sidebarDetails['paid']['wins']);
            }
            if ($sidebarDetails['paid']['ties'] !== 0) {
                $sidebarDetails['paid']['ties_percentage'] = $sidebarDetails['paid']['all'] * (100 / $sidebarDetails['paid']['ties']);
            }
        }


        // ddd($sidebarDetails['paid']);
        // ddd($sidebarDetails['paid']['average_kf']);

        View::composer('*', function($view) use ($sidebarDetails){

            $request = new Request;
            // $request-
            $sidebar = new Collection();
            // dd(\Route::getCurrentRoute()->getName());
            $rate = [];
            if (file_exists('storage/currency/' . date("Y-m-d", time()) . '.json')) {
                $rate = file_get_contents( 'storage/currency/' . date("Y-m-d", time()) . '.json');
                // ddd($rate);
                $rate = json_decode($rate, 1);
                // file_put_contents( 'currency/' . date("Y-m-d", time()) . '.json', json_encode($rate));
            }else{

                try {
                    $url = 'https://cbu.uz/uz/arkhiv-kursov-valyut/json/RUB/'. date("Y-m-d", time()) .'/';
                    // $rate = Http::timeout(20)->get($url)->json();
                    $rate = json_decode(file_get_contents($url), 1);
                    // dd(json_decode($rate, 1));
                    // dd($rate);
                    $rate = $rate[0];
                    $rate['status'] = 'online';

                    file_put_contents( 'storage/currency/' . date("Y-m-d", time()) . '.json', json_encode($rate));
                    // ddd($rate);
                } catch (ConnectionException $e) {
                    // ddd($e->getMessage());
                    $rate = json_decode('[{"id":1,"Code":"643","status":"offline","Ccy":"RUB","CcyNm_RU":"\u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0438\u0439 \u0440\u0443\u0431\u043b\u044c","CcyNm_UZ":"Rossiya rubli","CcyNm_UZC":"\u0420\u043e\u0441\u0441\u0438\u044f \u0440\u0443\u0431\u043b\u0438","CcyNm_EN":"Russian Ruble","Nominal":"1","Rate":"135.03","Diff":"2.76","Date":"13.10.2020"}]', 1)[0];
                    $rate['Rate'] = env('EXCHANGE_RATE', 142.60);
                    file_put_contents( 'storage/currency/' . date("Y-m-d", time()) . '.json', $rate);
                }

            }
            // ddd($sidebar);
            if (\Route::getCurrentRoute()) {

                switch (\Route::getCurrentRoute()->getName()) {
                    case 'home':
                        $sidebar = Widget::where('page', 'home')->where('type', 'sidebar')->where('status', 'active')->get();
                        break;
                    case 'stats':
                        $sidebar = Widget::where('page', 'stats')->where('type', 'sidebar')->where('status', 'active')->get();
                        break;
                    case 'tariffs':
                        $sidebar = Widget::where('page', 'tariffs')->where('type', 'sidebar')->where('status', 'active')->get();
                        break;
                    case 'theories.index':
                        $sidebar = Widget::where('page', 'theories')->where('type', 'sidebar')->where('status', 'active')->get();
                        break;
                    case 'forecasts.free':
                        $sidebar = Widget::where('page', 'forecast')->where('type', 'sidebar')->where('status', 'active')->get();
                        break;

                    default:
                        # code...
                        break;
                }
            }else{
                $sidebar = null;
            }

            // dd($sidebar);
            // ddd($sidebarDetails);
			if(Auth::check()){
				$currentUser = Auth::user();
				// ddd($currentUser);
				// ddd($currentUser->profile->image);
                // ddd(Auth::user()->getProfilePhoto() );

				if ( Auth::user()->getProfilePhoto() === null) {
					$currentUser->image = '/storage/imgs/default-avatar.jpg';
				}elseif(!Str::contains($currentUser->profile->image, 'https')){
					$currentUser->image =  '/storage/' .  $currentUser->getProfilePhoto();
				}

				// ddd($currentUser->profile);
                // ddd($rate);
                $view->with('currentUser', $currentUser)
                        ->with('sidebarDetails', $sidebarDetails)
                        ->with('rate', $rate)
                        ->with('sidebar', $sidebar);
			}else{
                // ddd(111);
				$view->with('sidebarDetails', $sidebarDetails)->with('rate', $rate)->with('sidebar', $sidebar);
			}


        });
        //
    }
}

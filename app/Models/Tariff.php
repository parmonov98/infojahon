<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    use HasFactory;


    public function setTariffLabel(){

        // ddd($this->attributes['quantity']);
        // ddd($this->attributes['type']);
        if ($this->attributes['type'] == "piece") {
            $this->tariffLabel =  $this->quantity . ' dona prognoz';
        }else{
            // ddd($this->type, $this->attributes);
            $this->tariffLabel =  $this->term . ' kunlik paket';
        }
    }

}

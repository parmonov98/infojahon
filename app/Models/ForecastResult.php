<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForecastResult extends Model
{
    use HasFactory;

    public $fillable = ['forecast_id', 'result', 'screenshot'];

    public function forecast()
    {
        return $this->belongsTo(Forecast::class);
    }
}

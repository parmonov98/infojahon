<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forecast extends Model
{
    use HasFactory;

    public function user()
    {
        $this->hasOne(User::class);
    }

    public function setResult()
    {
        $this->result = ForecastResult::firstWhere('forecast_id', $this->id);
    }

    public function result()
    {
        return $this->hasOne(ForecastResult::class, 'forecast_id');
    }

}

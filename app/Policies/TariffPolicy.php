<?php

namespace App\Policies;

use App\Models\Tariff;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TariffPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Tariff  $tariff
     * @return mixed
     */
    public function view(User $user, Tariff $tariff)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->type === 'developer' || $user->type === 'admin';
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Tariff  $tariff
     * @return mixed
     */
    public function update(User $user, Tariff $tariff)
    {
        return $user->type === 'developer' || $user->type === 'admin';
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Tariff  $tariff
     * @return mixed
     */
    public function delete(User $user, Tariff $tariff)
    {
        return $user->type === 'developer' || $user->type === 'admin';
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Tariff  $tariff
     * @return mixed
     */
    public function restore(User $user, Tariff $tariff)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Tariff  $tariff
     * @return mixed
     */
    public function forceDelete(User $user, Tariff $tariff)
    {
        //
    }
}

<?php

namespace App\Policies;

use App\Models\Forecast;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Carbon;

class ForecastPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
        ddd(1111);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forecast  $forecast
     * @return mixed
     */
    public function view(User $user, Forecast $forecast)
    {
        echo 111; die;
        // ddd($user->account->forecasts);

        if ($user->account->forecasts > 0 || $user->account->expire_date > Carbon::now()) {
            return true;
        }else{
            return false;
        }

    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->type == 'admin' || $user->type == 'developer';
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forecast  $forecast
     * @return mixed
     */
    public function update(User $user, Forecast $forecast)
    {

        return $user->type == 'admin' || $user->type == 'developer'
                ? Response::allow()
                : Response::deny("Admin bo'lishiz kerak bu amalni bajarish uchun.");
        // return $user->id == $forecast->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forecast  $forecast
     * @return mixed
     */
    public function delete(User $user, Forecast $forecast)
    {
        return $user->type == 'admin' || $user->type == 'developer'
                ? Response::allow()
                : Response::deny("Admin bo'lishiz kerak bu amalni bajarish uchun.");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forecast  $forecast
     * @return mixed
     */
    public function restore(User $user, Forecast $forecast)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Forecast  $forecast
     * @return mixed
     */
    public function forceDelete(User $user, Forecast $forecast)
    {
        //
    }
}

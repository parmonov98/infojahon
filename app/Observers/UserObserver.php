<?php

namespace App\Observers;

use App\Mail\UserWelcomeMail;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $request = Request::all();
        // ddd($user->profile);
        if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
            # code...
            $user->profile()->create(
                ['first_name' => $user->username]
            );
            $user->account()->create(
                ['balance' => 0.0]
            );
            // Mail::to($user->email)->send(new UserWelcomeMail());
        }else{

            $user->profile()->create(
                [
                    'first_name' => isset($request['first_name']) ? $request['first_name'] : $request['last_name'],
                    'last_name' => isset($request['last_name']) ? $request['last_name'] : $request['first_name'],
                    'image' => isset($request['photo_url'])  ? $request['photo_url'] : null,
                ]
            );

            $user->account()->create(
                ['balance' => 0.0]
            );
			// Auth::login($user, true);

        }

    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}

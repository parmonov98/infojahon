<?php

namespace App\Console\Commands;

use App\Models\Forecast;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class DailyCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:forecasts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking all forecasts for publishing.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        file_put_contents('cron-time.log', date("H:i"));

        // https://github.com/irazasyed/telegram-bot-sdk

        // publishing created forecasts
        $forecasts = Forecast::where('status', 'created')->get();

        if ($forecasts->count() > 0) {
            // file_put_contents( 'forecast-exists.json', 2);
            $forecasts->each(function ($item)
            {
                // file_put_contents( 'forecast-' . $item->id . '.json', $item->toJson());
                if (strtotime($item->publishing_time) < time()) {
                    $item->status = 'published';
                    // ['status' =>'published']
                    $item->save();

                    $url = env('APP_URL', 'https://infojahon.ru') . '/forecasts/' . $item->link;

                    $http = Http::post( env('APP_URL', 'https://infojahon.ru') . '/bot/receive',
                    [
                        'message' =>  $item->title . " chiqarildi! \nURL: $url",
                        "type" => "Yangi prognoz chiqdi"
                    ]);

                }

            });
        }


        $forecasts = Forecast::where('is_ended', 'no')->get();

        if ($forecasts->count() > 0) {
            // file_put_contents( 'forecast-exists.json', 2);
            $forecasts->each(function ($item)
            {
                // file_put_contents( 'forecast-' . $item->id . '.json', $item->toJson());
                if (strtotime($item->end) < time()) {
                    $item->is_ended = 'yes';
                    $item->save();

                    $url = env('APP_URL', 'https://infojahon.ru') . '/admin/forecastResult/' . $item->id;

                    $http = Http::post( env('APP_URL', 'https://infojahon.ru') . '/bot/receive',
                    [
                        'message' =>  $item->title . " uchun natija kiriting! \nURL: $url",
                        "type" => "Prognoz stavka vaqti tugadi!"
                    ]);
                }

            });
        }

        // file_put_contents('forecasts.json', json_encode($data));
        $this->info('Successfully run check:forecasts command.');
        // return 0;
    }



}

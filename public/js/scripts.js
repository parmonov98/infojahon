document.addEventListener('DOMContentLoaded', function (e) {

    if (document.querySelector('.hamburger')) {
        const hamburger = document.querySelector('.hamburger');
        hamburger.addEventListener('click', function (e) {
            const navbarMenu = document.getElementById('navbar-menu');
            navbarMenu.classList.toggle('active');
        });
    }

    if (document.querySelector('.main-slider')) {

        var slider = tns({
            container: '.main-slider',
            items: 1,
            slideBy: 'page',
            autoplay: false,
            controlsContainer: "#custom-main-slider-controls",
            nav: true
        });
    }
    if (document.querySelector('.close_zoom_modal')) {

        document.querySelector('.close_zoom_modal').addEventListener('click', function (e) {
            e.target.parentElement.classList.remove('shown_modal');
        })

    }

});

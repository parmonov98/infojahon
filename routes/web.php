<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BotController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\CustomLoginController;
use App\Http\Controllers\ForecastsController;
use App\Http\Controllers\FreeKassaController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SeamlessLoginController;
use App\Http\Controllers\TariffController;
use App\Http\Controllers\TheoriesController;
use App\Http\Middleware\AdminMiddleware;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/admin', [AdminController::class, 'index']);

// Route::get('login', [LoginController::class, 'authenticate'])->name('authenticate');
// Route::post('login', [LoginController::class, 'authenticate'])->name('authenticate');

Route::get('test', function () {
    echo phpinfo();
});

// Route::any('/bot/receive', [BotController::class, 'receiveMessage'])->name('receiveMessage');
Route::any('/bot/receive', [BotController::class, 'sendMessage'])->name('sendMessage');


Route::get('/', [App\Http\Controllers\PagesController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth', 'admin']], function () {



    Route::post('ckeditor/image-upload', [CKEditorController::class, 'uploadImage'])->name('ckeditor.image-upload');
    Route::post('ckeditor/upload', [CKEditorController::class, 'upload'])->name('ckeditor.upload');

    Route::get('/admin/theory/create', [TheoriesController::class, 'createTheory'])->name('addtheory');
    Route::post('/admin/theory/create', [TheoriesController::class, 'storeTheory'])->name('storeTheory');
    Route::get('/admin/theory/all', [TheoriesController::class, 'allTheories'])->name('alltheories');
    Route::get('/admin/theory/edit/{theory}', [TheoriesController::class, 'editTheory'])->name('editTheory');
    Route::post('/admin/theory/update/{theory}', [TheoriesController::class, 'updateTheory'])->name('updatetheory');
    Route::post('/admin/theory/{theory}', [TheoriesController::class, 'deleteTheory'])->name('deleteTheory');
    Route::post('/admin/ajax/user/{user}', [AdminController::class, 'deleteUser'])->name('deleteUser');

    Route::get('admin', [AdminController::class, 'index'])->name('admin');
    Route::get('/admin/users', [AdminController::class, 'users'])->name('users');
    Route::get('/admin/messages', [AdminController::class, 'allMessages'])->name('messages');
    Route::get('/admin/widgets', [AdminController::class, 'allWidgets'])->name('widgets');

    Route::get('/admin/payments/{payment}', [AccountController::class, 'editPayment'])->name('editPayment');
    Route::post('/admin/payments/{payment}', [AccountController::class, 'updatePayment'])->name('updatePayment');

    Route::get('/admin/forecast/add', [ForecastsController::class, 'addForecast'])->name('addforecast');
    Route::post('/admin/forecast/add', [ForecastsController::class, 'storeForecast'])->name('addforecast');
    Route::get('/admin/forecast/all', [ForecastsController::class, 'allForecasts'])->name('allforecasts');
    Route::get('/admin/forecast/delete/{forecast}', [ForecastsController::class, 'deleteForecast'])->name('deleteForecast');
    Route::post('/admin/forecast/update/{forecast}', [ForecastsController::class, 'updateForecast'])->name('updateForecast');

    Route::get('/admin/forecast/edit/{forecast}', [ForecastsController::class, 'editForecast'])->name('editForecast');
    Route::get('/admin/forecastResult/{forecast}', [ForecastsController::class, 'editForecastResult'])->name('editForecastResult');



    Route::get('/admin/forecastResults', [ForecastsController::class, 'allForecastResult'])->name('allForecastResult');
    Route::get('/admin/forecastResult/{forecast}', [ForecastsController::class, 'addForecastResult'])->name('addForecastResult');
    Route::post('/admin/forecastResult/{forecast}/create', [ForecastsController::class, 'storeForecastResult'])->name('storeForecastResult');



    Route::get('/admin/tariff/create', [TariffController::class, 'createTariff'])->name('addtariff');
    Route::post('/admin/tariff/create', [TariffController::class, 'storeTariff'])->name('addtariff');
    Route::get('/admin/tariff/all', [TariffController::class, 'allTariffs'])->name('alltariffs');
    Route::get('/admin/tariff/edit/{tariff}', [TariffController::class, 'editTariff'])->name('edittariff');
    Route::post('/admin/tariff/update/{tariff}', [TariffController::class, 'updateTariff'])->name('updatetariff');
    Route::get('/admin/tariff/delete/{tariff}', [TariffController::class, 'deleteTariff'])->name('deleteTariff');

    Route::get('/admin/withdrawals', [AccountController::class, 'withdrawals'])->name('allWithdrawals');
    Route::get('/admin/payments', [AccountController::class, 'allPayments'])->name('allPayments');

    Route::post('/admin/ajax/update_payment/{payment}', [AccountController::class, 'updatePayment'])->name('updatePayment');
    Route::post('/admin/ajax/update_withdrawal/{withdrawal}', [AccountController::class, 'updateWithdrawal'])->name('updateWithdrawal');
    Route::post('/admin/ajax/update_widget/{widget}', [AdminController::class, 'updateWidget'])->name('updateWidget');

    Route::get('/admin/{controller}', [TariffController::class, 'index']);

});
Route::group(['middleware' => ['auth']], function () {
    // profile vs user operations
    Route::get('/affiliate', [App\Http\Controllers\ProfileController::class, 'affiliate'])->name('affiliate');
    Route::post('/cabinet', [App\Http\Controllers\ProfileController::class, 'store'])->name('store'); // saving submitted data
    Route::get('cabinet', [App\Http\Controllers\ProfileController::class, 'edit'])->name('cabinet'); // editing cabinet form
    Route::get('/notifications', [App\Http\Controllers\ProfileController::class, 'notifications'])->name('notifications');

    Route::get('/referals', [App\Http\Controllers\ProfileController::class, 'referals'])->name('referals');
    Route::get('/withdrawal', [App\Http\Controllers\AccountController::class, 'withdrawal'])->name('withdrawal');
    Route::post('/withdrawal', [App\Http\Controllers\AccountController::class, 'withdrawalStore'])->name('withdrawalStore');


    // account controller operations
    Route::post('/addup', [App\Http\Controllers\AccountController::class, 'storePayment'])->name('storePayment'); // accepting payment storing
    Route::get('/payment/{public_id}', [App\Http\Controllers\PagesController::class, 'checkPayment'])->name('checkPayment');
    Route::get('/payments', [App\Http\Controllers\AccountController::class, 'payments'])->name('payments');
    Route::get('/addup', [App\Http\Controllers\AccountController::class, 'addup'])->name('addup'); // accepting payment form

    Route::post('/addup/{payment}', [App\Http\Controllers\AccountController::class, 'fillup'])->name('fillup'); // accepting payment for purchase

});



Route::get('/history', [PagesController::class, 'history'])->name('history');
Route::get('/stats', [PagesController::class, 'stats'])->name('stats');

Route::get('/contact', [PagesController::class, 'contact']);
Route::post('/contact', [PagesController::class, 'storeMessage']);

Route::get('forecasts/free', [PagesController::class, 'free'])->name('forecasts.free');
Route::get('forecasts/paid', [PagesController::class, 'paid']);
Route::get('purchased', [PagesController::class, 'purchased'])->name('purchased');

Route::get('tariffs', [PagesController::class, 'tariffs'])->name('tariffs');

Route::get('tariffs/{id}', [PagesController::class, 'makePayment'])->name('makePayment');


Route::post('/paybill', [App\Http\Controllers\AccountController::class, 'payBill'])->name('paybill'); // accepting payment from forecasts/paid/{id}
Route::get('/checkpayment/{purchase}', [App\Http\Controllers\AccountController::class, 'checkPayment'])->name('checkpayment'); // accepting payment from forecasts/paid/{id}

Route::get('/payment/pay/{payment}', [App\Http\Controllers\AccountController::class, 'payPayment'])->name('paypayment'); // accepting payment from forecasts/paid/{id}



Route::resource('forecasts', ForecastsController::class);
Route::get('/theories/free', [TheoriesController::class, 'free'])->name('theories.free');
Route::get('/theories/paid', [TheoriesController::class, 'paid'])->name('theories.paid');
Route::resource('theories', TheoriesController::class);
Route::get('login/{type}', [CustomLoginController::class, 'index']);


Route::get('auth', [SeamlessLoginController::class, 'index']);

Auth::routes(['verify' => true]);
Auth::routes();

Route::get('/freekassa/result', [ FreeKassaController::class, 'handlePayment']);
Route::get('/freekassa/success', [ FreeKassaController::class, 'paidOrder']);
Route::get('/freekassa/fail', [ FreeKassaController::class, 'unpaidOrder']);



Route::get('/clear', function() {

    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');

    return "Cleared!";

 });


